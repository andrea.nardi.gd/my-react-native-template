module.exports = function (api) {
  api.cache(true);
  const baseConfig = {
    presets: ['babel-preset-expo'],
    plugins: [
      'inline-dotenv',
      [
        'babel-plugin-root-import',
        {
          rootPathPrefix: 'src',
          rootPathSuffix: './src',
        },
      ],
      'react-native-reanimated/plugin',
    ],
  };

  if (process.env.REACT_APP_ENV_TYPE === 'dev') {
    console.debug('#ENV TYPE => ', process.env.REACT_APP_ENV_TYPE);
    return baseConfig
  }

  baseConfig.presets.push('module:metro-react-native-babel-preset')
  const devPlugins = ['transform-remove-console', ...baseConfig.plugins]

  baseConfig.plugins = devPlugins

  return baseConfig
}
import 'dotenv/config';
import production from './app.production.json';
import dev from './app.dev.json';

const returnEnvironment = (envType) => {
  switch (envType) {
    default:
    case 'dev':
      return dev;
    case 'prod':
      return production;
  }
};

export default ({ config }) => {
  const envType = process.env.APP_ENVIRONMENT
    ? process.env.APP_ENVIRONMENT
    : process.env.REACT_APP_ENV_TYPE;

  const envConfig = returnEnvironment(envType);
  return {
    ...config,
    ...envConfig,
  };
};

import 'intl-pluralrules';
import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import en from './assets/locales/translations/en/translation.json';
import * as Localization from 'expo-localization';

const resources = {
  en: {
    translation: {
      ...en,
    },
  },
};

const systemLocale = Localization.locale.split('-')[0];

i18n.use(initReactI18next).init({
  resources,
  lng: resources[systemLocale] ? systemLocale.toString() : 'en',

  interpolation: {
    escapeValue: false,
  },
});

export default i18n;

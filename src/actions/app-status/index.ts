import { AppStateStatus } from 'react-native';
import { AppXOR } from '../../types/types/utils';

export const SET_APP_THREAD_LOADING = 'SET_APP_THREAD_LOADING';
export const SET_THREAD_LOADING_PROGRESS = 'SET_THREAD_LOADING_PROGRESS';
export const SET_SHOULD_SKIP_INTRO = 'SET_SHOULD_SKIP_INTRO';
export const SET_APP_INITIALIZED = 'SET_APP_INITIALIZED';
export const SET_INIZIALIZATION_FAILED = 'SET_INIZIALIZATION_FAILED';
export const UNSET_APP_INITIALIZED = 'UNSET_APP_INITIALIZED';
export const UNSET_INITIALIZATION_FAILED = 'UNSET_INITIALIZATION_FAILED';
export const SET_APP_STATE = 'SET_APP_STATE';

type FirstUsageAction = {
  type: typeof SET_SHOULD_SKIP_INTRO | typeof SET_APP_THREAD_LOADING;
  payload: boolean;
};

type ThreadLoadingProgressAction = {
  type: typeof SET_THREAD_LOADING_PROGRESS;
  payload: number;
};

type SetAppStateAction = {
  type: typeof SET_APP_STATE;
  payload: AppStateStatus;
};

type StatusAction = {
  type:
    | typeof SET_INIZIALIZATION_FAILED
    | typeof SET_APP_INITIALIZED
    | typeof UNSET_APP_INITIALIZED
    | typeof UNSET_INITIALIZATION_FAILED;
};

export type AppStatusAction =
  | AppXOR<FirstUsageAction, StatusAction>
  | SetAppStateAction
  | ThreadLoadingProgressAction;

export function setActivityState(payload: AppStateStatus): AppStatusAction {
  return {
    type: SET_APP_STATE,
    payload,
  };
}

export function setAppThreadLoading(payload: boolean): AppStatusAction {
  return {
    type: SET_APP_THREAD_LOADING,
    payload,
  };
}

export function setAppThreadLoadingProgress(payload: number): AppStatusAction {
  return {
    type: SET_THREAD_LOADING_PROGRESS,
    payload,
  };
}

export function setIsFirstUsage(payload: boolean): AppStatusAction {
  return {
    type: SET_SHOULD_SKIP_INTRO,
    payload,
  };
}

export function setInizialitazionFailed(): AppStatusAction {
  return {
    type: SET_INIZIALIZATION_FAILED,
  };
}

export function unsetInitializationFailed(): AppStatusAction {
  return {
    type: UNSET_INITIALIZATION_FAILED,
  };
}

export function setAppInitialized(): AppStatusAction {
  return {
    type: SET_APP_INITIALIZED,
  };
}

export function unsetAppInitialized(): AppStatusAction {
  return {
    type: UNSET_APP_INITIALIZED,
  };
}

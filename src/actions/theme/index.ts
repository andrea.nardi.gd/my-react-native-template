import { AppColors } from '../../config/styles/GlobalStyles';

export const SET_ACTIVE_COLOR_SCHEME = 'SET_ACTIVE_COLOR_SCHEME';
export const SET_FONT_SIZE_MULTIPLIER = 'SET_FONT_SIZE_MULTIPLIER';

type SetColorSchemeAction = {
  type: typeof SET_ACTIVE_COLOR_SCHEME;
  payload: typeof AppColors;
};

type SetFontSizeAction = {
  type: typeof SET_FONT_SIZE_MULTIPLIER;
  payload: number;
};

export type SetThemeAction = SetColorSchemeAction | SetFontSizeAction;

export const setColorSchemeAction = (payload: typeof AppColors): SetThemeAction => {
  return {
    type: SET_ACTIVE_COLOR_SCHEME,
    payload,
  };
};

export const SetFontSizeAction = (payload: number): SetThemeAction => {
  return {
    type: SET_FONT_SIZE_MULTIPLIER,
    payload,
  };
};

import { Navigation, NavigationModal } from '../../types/types/config/navigation';

export const SET_ACTIVE_SCREEN = 'SET_ACTIVE_SCREEN';
export const SET_ACTIVE_MODAL = 'SET_ACTIVE_MODAL';
export const UNSET_ACTIVE_MODAL = 'UNSET_ACTIVE_MODAL';

type SetNavigationAction = {
  type: typeof SET_ACTIVE_SCREEN;
  payload: Navigation;
};

type SetActiveModalAction = {
  type: typeof SET_ACTIVE_MODAL;
  payload: NavigationModal;
};

type UnSetActiveModalAction = {
  type: typeof UNSET_ACTIVE_MODAL;
};

export type NavigationAction = SetNavigationAction | SetActiveModalAction | UnSetActiveModalAction;

export const setActiveScreenAction = (payload: Navigation): NavigationAction => {
  return {
    type: SET_ACTIVE_SCREEN,
    payload,
  };
};

export const setActiveModalAction = (payload: NavigationModal): NavigationAction => {
  return {
    type: SET_ACTIVE_MODAL,
    payload,
  };
};

export const unsetActiveModalAction = (): NavigationAction => {
  return {
    type: UNSET_ACTIVE_MODAL,
  };
};

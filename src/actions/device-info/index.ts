import { AppDeviceOS, AppDeviceType } from '../../types/types/device';

export const SET_DEVICE_TYPE = 'SET_DEVICE_TYPE';
export const SET_DEVICE_TYPE_AND_OS = 'SET_DEVICE_TYPE_AND_OS';
export const SET_DEVICE_OS = 'SET_DEVICE_OS';
export const SET_IS_ROOTED = 'SET_IS_ROOTED';

type SetDeviceOS = {
  type: typeof SET_DEVICE_OS;
  payload: AppDeviceOS;
};

type SetDeviceType = {
  type: typeof SET_DEVICE_TYPE;
  payload: AppDeviceType;
};

type SetDeviceRooted = {
  type: typeof SET_IS_ROOTED;
  payload: boolean;
};

type DeviceTypeAndOs = { type: AppDeviceType; os: AppDeviceOS };

type SetDeviceTypeAndOs = {
  type: typeof SET_DEVICE_TYPE_AND_OS;
  payload: DeviceTypeAndOs;
};

export type SetDeviceInfoAction =
  | SetDeviceOS
  | SetDeviceRooted
  | SetDeviceType
  | SetDeviceTypeAndOs;

export function setDeviceType(data: AppDeviceType): SetDeviceType {
  return {
    type: SET_DEVICE_TYPE,
    payload: data,
  };
}

export function setIsRooted(data: boolean): SetDeviceRooted {
  return {
    type: SET_IS_ROOTED,
    payload: data,
  };
}

export function setDeviceTypeAndOs(payload: DeviceTypeAndOs): SetDeviceTypeAndOs {
  return {
    type: SET_DEVICE_TYPE_AND_OS,
    payload,
  };
}

export function setDeviceOs(data: AppDeviceOS): SetDeviceOS {
  return {
    type: SET_DEVICE_OS,
    payload: data,
  };
}

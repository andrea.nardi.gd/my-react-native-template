import { AppXOR } from '../../types/types/utils';

export const SET_IS_ONLINE = 'SET_IS_ONLINE';
export const SET_FLIGHT_MODE = 'SET_FLIGHT_MODE';
export const SET_DISCONNECTED = 'SET_DISCONNECTED';
export const SET_RECONNECTED = 'SET_RECONNECTED';
export const SET_ENSTABLISHED_CONNECTION = 'SET_ENSTABLISHED_CONNECTION';

type SetConnectionStatusAction = {
  type: typeof SET_FLIGHT_MODE | typeof SET_IS_ONLINE;
  payload: boolean;
};

type SetConnectionHistoryAction = {
  type: typeof SET_DISCONNECTED | typeof SET_ENSTABLISHED_CONNECTION | typeof SET_RECONNECTED;
};

export type SetConnectionInfoAction = AppXOR<SetConnectionHistoryAction, SetConnectionStatusAction>;

export function setEnstablishedConnection(): SetConnectionHistoryAction {
  return {
    type: SET_ENSTABLISHED_CONNECTION,
  };
}

export function setDisconnected(): SetConnectionHistoryAction {
  return {
    type: SET_DISCONNECTED,
  };
}

export function setReconnected(): SetConnectionHistoryAction {
  return {
    type: SET_RECONNECTED,
  };
}

export function setOnlineStatus(data: boolean): SetConnectionStatusAction {
  return {
    type: SET_IS_ONLINE,
    payload: data,
  };
}

//Unused for now
export function setFlightMode(data: boolean): SetConnectionStatusAction {
  return {
    type: SET_FLIGHT_MODE,
    payload: data,
  };
}

export const SET_LOCAL_AUTH_ENABLED = 'SET_LOCAL_AUTH_ENABLED';
export const SET_LOCAL_AUTH_POSSIBLE = 'SET_LOCAL_AUTH_POSSIBLE';
export const SET_LOCAL_AUTH_ENROLLED = 'SET_LOCAL_AUTH_ENROLLED';
export const SET_IS_PIN_SET = 'SET_IS_PIN_SET';
export const SET_IS_USER_AUTHORIZED = 'SET_IS_USER_AUTHORIZED';

export type SetLocalAuthAction = {
  type:
    | typeof SET_LOCAL_AUTH_ENABLED
    | typeof SET_LOCAL_AUTH_POSSIBLE
    | typeof SET_LOCAL_AUTH_ENROLLED
    | typeof SET_IS_PIN_SET
    | typeof SET_IS_USER_AUTHORIZED;
  payload: boolean;
};

export function setIsPinSet(payload: boolean): SetLocalAuthAction {
  return {
    type: SET_IS_PIN_SET,
    payload,
  };
}

export function setIsUserAuthorized(payload: boolean): SetLocalAuthAction {
  return {
    type: SET_IS_USER_AUTHORIZED,
    payload,
  };
}

export function setLocalAuthEnabled(payload: boolean): SetLocalAuthAction {
  return {
    type: SET_LOCAL_AUTH_ENABLED,
    payload,
  };
}

export function setLocalAuthEnrolled(payload: boolean): SetLocalAuthAction {
  return {
    type: SET_LOCAL_AUTH_ENROLLED,
    payload,
  };
}

export function setLocalAuthPossibile(payload: boolean): SetLocalAuthAction {
  return {
    type: SET_LOCAL_AUTH_POSSIBLE,
    payload,
  };
}

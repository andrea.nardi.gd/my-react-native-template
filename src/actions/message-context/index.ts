import { SnackBarMessage } from '../../types/types/messages';
import { AppXOR } from '../../types/types/utils';

export const SET_ACTIVE_MESSAGE = 'SET_ACTIVE_MESSAGE';
export const DELETE_ACTIVE_MESSAGE = 'DELETE_ACTIVE_MESSAGE';
export const SHOW_QUIT_MESSAGE = 'SHOW_QUIT_MESSAGE';
export const HIDE_QUIT_MESSAGE = 'HIDE_QUIT_MESSAGE';

type SetMessageContextAction = {
  type: typeof SET_ACTIVE_MESSAGE | typeof SHOW_QUIT_MESSAGE;
  isNotification: boolean;
  payload: SnackBarMessage;
};

type UnsetMessageContextAction = {
  type: typeof DELETE_ACTIVE_MESSAGE | typeof HIDE_QUIT_MESSAGE;
  isNotification: boolean;
};

export type MessageContextAction = AppXOR<SetMessageContextAction, UnsetMessageContextAction>;
export function setActiveSnackBar(payload: SnackBarMessage): MessageContextAction {
  return {
    type: SET_ACTIVE_MESSAGE,
    isNotification: true,
    payload,
  };
}

export function unsetActiveSnackBar(): MessageContextAction {
  return {
    type: DELETE_ACTIVE_MESSAGE,
    isNotification: true,
  };
}

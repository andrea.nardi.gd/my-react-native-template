import { LocalizationLang } from '../../types/types/localization';

export const SET_LANG = 'SET_LANG';

type SetLanguageAction = {
  type: typeof SET_LANG;
  payload: LocalizationLang;
};

export type LocalizationAction = SetLanguageAction;

export function setAppLanguage(data: LocalizationLang): SetLanguageAction {
  return {
    type: SET_LANG,
    payload: data,
  };
}

import { AppConfig } from '../../types/types/config/AppConfigs';

export const SET_CONFIG_DATA = 'SET_CONFIG_DATA';
export const SET_CONFIG_LOADING = 'SET_CONFIG_LOADING';
export const SET_CONFIG_ERROR = 'SET_CONFIG_ERROR';

type SetConfigDataAction = {
  type: typeof SET_CONFIG_DATA;
  payload: AppConfig;
};

type SetConfigLoadingAction = {
  type: typeof SET_CONFIG_LOADING;
};

type SetConfigErrorAction = {
  type: typeof SET_CONFIG_ERROR;
  error: unknown;
};

export type SetConfigAction = SetConfigDataAction | SetConfigLoadingAction | SetConfigErrorAction;

export function setAppConfig(payload: AppConfig): SetConfigDataAction {
  return {
    type: SET_CONFIG_DATA,
    payload,
  };
}

export function setConfigLoading(): SetConfigLoadingAction {
  return {
    type: SET_CONFIG_LOADING,
  };
}

export function setConfigError(error: unknown): SetConfigErrorAction {
  return {
    type: SET_CONFIG_ERROR,
    error,
  };
}

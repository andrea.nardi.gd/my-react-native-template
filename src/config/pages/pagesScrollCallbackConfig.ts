import { AppThunk } from '../../types/types/action/Action';
import {
  PageScrollCallbackResolverFunction,
  PageScrollCallbackResolverProps,
  PAGES_CALLBACK_PATHS,
} from '../../types/types/pages/PagesCallback';

const wait = (timeout: number) => {
  return new Promise((resolve) => setTimeout(resolve, timeout));
};

const VoidScrollFunction = <P, T>({
  //Just a function that shows a scroll loader for 2 seconds
  loaderStateSetter,
}: PageScrollCallbackResolverProps<P, T>): AppThunk => {
  return async (_dispatch) => {
    loaderStateSetter(true);
    wait(2000).then(() => loaderStateSetter(false));
  };
};

export const pagesScrollCallbackConfigResolver: Partial<
  Record<PAGES_CALLBACK_PATHS, PageScrollCallbackResolverFunction>
> = {
  DEFAULT: VoidScrollFunction,
};

import { AppThunk } from '../../types/types/action/Action';
import {
  PagesCallbackResolverFunction,
  PAGES_CALLBACK_PATHS,
} from '../../types/types/pages/PagesCallback';

const VoidTestFunction = (): AppThunk => {
  return async (_dispatch) => {
    void 0;
  };
};

const VoidPageCallbackFunction = async (): Promise<void> => void 0;

export const pagesCallbackConfigResolver: Partial<
  Record<PAGES_CALLBACK_PATHS, PagesCallbackResolverFunction>
> = {
  TEST_PAGE_CALLBACK: VoidTestFunction,
  DEFAULT: VoidPageCallbackFunction as unknown as PagesCallbackResolverFunction, //HACK just a function that does nothing
};

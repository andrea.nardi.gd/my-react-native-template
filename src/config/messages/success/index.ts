import { SnackBarMessage } from '../../../types/types/messages';

export enum SuccessMessagesKeys {
  DEFAULT = 'success',
}

export const SuccessMessagesDict: Record<SuccessMessagesKeys, SnackBarMessage> = {
  [SuccessMessagesKeys.DEFAULT]: { type: 'success', message: SuccessMessagesKeys.DEFAULT },
};

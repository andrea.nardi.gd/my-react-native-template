import { SnackBarMessage } from '../../../types/types/messages';

export enum QuitMessageKeys {
  DEFAULT = 'quit',
}

export const QuitMessagesDict: Record<QuitMessageKeys, SnackBarMessage> = {
  [QuitMessageKeys.DEFAULT]: { message: QuitMessageKeys.DEFAULT, type: 'quit' },
};

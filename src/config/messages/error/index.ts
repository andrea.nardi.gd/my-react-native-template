import { SnackBarMessage } from '../../../types/types/messages';

export enum ErrorMessagesKeys {
  DEFAULT = 'error',
}

export const ErrorMessagesDict: Record<ErrorMessagesKeys, SnackBarMessage> = {
  [ErrorMessagesKeys.DEFAULT]: {
    message: ErrorMessagesKeys.DEFAULT,
    type: 'error',
  },
};

import { SnackBarMessage } from '../../../types/types/messages';

export enum WarningMessagesKeys {
  DEFAULT = 'warning',
}

export const WarningMessagesDict: Record<WarningMessagesKeys, SnackBarMessage> = {
  [WarningMessagesKeys.DEFAULT]: { message: WarningMessagesKeys.DEFAULT, type: 'warning' },
};

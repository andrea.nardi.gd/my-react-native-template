import { SnackBarMessage } from '../../../types/types/messages';

export enum InfoMessagesKeys {
  DEFAULT = 'info',
}

export const InfoMessagesDict: Record<InfoMessagesKeys, SnackBarMessage> = {
  [InfoMessagesKeys.DEFAULT]: { message: InfoMessagesKeys.DEFAULT, type: 'info' },
};

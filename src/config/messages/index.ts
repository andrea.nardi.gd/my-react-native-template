import { MessageTypes, SnackBarMessage } from '../../types/types/messages';
import { ErrorMessagesDict, ErrorMessagesKeys } from './error';
import { InfoMessagesDict, InfoMessagesKeys } from './info';
import { QuitMessageKeys, QuitMessagesDict } from './quit';
import { SuccessMessagesDict, SuccessMessagesKeys } from './success';
import { WarningMessagesDict, WarningMessagesKeys } from './warning';

export type AggregateMessagesKeys =
  | SuccessMessagesKeys
  | InfoMessagesKeys
  | QuitMessageKeys
  | ErrorMessagesKeys
  | WarningMessagesKeys;

const MessagesDictionary: Partial<
  Record<MessageTypes, Partial<Record<AggregateMessagesKeys, SnackBarMessage>>>
> = {
  success: { ...SuccessMessagesDict },
  error: { ...ErrorMessagesDict },
  info: { ...InfoMessagesDict },
  quit: { ...QuitMessagesDict },
  warning: { ...WarningMessagesDict },
};

export default MessagesDictionary;

import { PAGES_LIST } from '../../navs/NavsConfig';

export enum WarningEntryKeys {
  TEST_WARNING_ENTRY_KEY = 'TEST_WARNING_ENTRY_KEY',
}

export type WarningEntry = {
  textKey: string;
  textTitle?: string;
  confirmRedirect?: PAGES_LIST;
};

export const WarningDict: Partial<Record<WarningEntryKeys, WarningEntry>> = {
  TEST_WARNING_ENTRY_KEY: {
    textKey: 'TestWarningTitle',
    confirmRedirect: PAGES_LIST.TEST_AUTH_STACK,
  },
};

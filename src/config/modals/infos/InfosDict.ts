export enum InfosEntryKeys {
  default = 'default',
}

export type InfosEntry = {
  textKey: string;
  textTitle?: string;
  //confirmRedirect?: string;
};

export type InfoEntry = {
  textKey: string;
  textTitle?: string;
};

export const InfosDict: Record<InfosEntryKeys, InfosEntry> = {
  default: {
    textTitle: 'missingTitle',
    textKey: 'missingDescription',
  },
};

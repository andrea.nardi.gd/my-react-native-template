import { AppBaseRequest } from '../../types/interfaces/api/request';

enum ApiMapKeys {}

const ApiMap: Record<ApiMapKeys, AppBaseRequest> = {};

export default ApiMap;

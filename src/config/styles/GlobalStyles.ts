import { Colors } from './ColorStyles';
import { ShadowsStyles } from './ShadowStyles';

export const AppColors = Colors;

export const globalStyles = {
  IOS_MEASURES: {
    statusBar: 20,
    failInsetConstant: 0.33,
  },
  MODALS: {
    backgroundColor: 'rgba(52, 52, 52, 0.4)',
    backgroundColorLighter: 'rgba(52, 52, 52, 0.2)',
    backgroundColorDarker: 'rgba(52, 52, 52, 0.8)',
  },
  SHADOWS: { ...ShadowsStyles },
  COLOR_CONFIG: {
    primary: AppColors.primary500,
    darkPrimary: AppColors.primary500,
    secondary: AppColors.primary500,
    appDefaultBackgroundColor: AppColors.primary500,
  },
  ICONS_SETTINGS: {
    DEFAULT_SETTINGS_BUTTON_SIZE: 36,
  },
  UNSAFE_AREA_TOP_COLOR: AppColors.secondary100,
  APP_HEADER_BAR: {
    height: 60,
    backGroundColor: AppColors.secondary100,
  },
  APP_TAB_BAR: {
    icons: {
      size: 36,
      activeColor: AppColors.primary600,
      inactiveColor: AppColors.ink900,
    },
  },
};

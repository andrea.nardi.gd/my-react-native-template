export const Colors = {
  // Primary shades
  primary50: '#FAE4E9',
  primary100: '#F3BAC9',
  primary200: '#EB8FA5',
  primary300: '#E26583',
  primary400: '#DB496B',
  primary500: '#D43454',
  primary600: '#C43051',
  primary700: '#AF2B4D',
  primary800: '#9A2749',
  primary900: '#752041',

  //WhiteShades
  secondary100: '#FAFAFA',
  secondary900: '#FFFFFF',

  // Blues
  blue200: '#9FCEEB',
  blue800: '#3E6FA6',

  // Warning Shades
  warning100: '#ffecb3',
  warning400: '#ffb300',

  // Success Shades
  success100: '#e1f6c3',
  success500: '#8ee000',

  // Error Shades
  error100: '#ffccd1',
  error400: '#fc4848',

  // Ink Shades
  ink50: '#f9f9fb',
  ink200: '#f2f2f2',
  ink300: '#e4e4e4',
  ink400: '#c6c6c6',
  ink500: '#A8A8A8',
  ink800: '#4a4a4a',
  ink900: '#000000',
};

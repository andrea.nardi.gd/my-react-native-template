import { Colors } from './ColorStyles';

export const ShadowsStyles = {
  bottom: {
    high: {},
    medium: {},
    soft: {},
  },
  top: {
    high: {
      shadowColor: Colors.ink900,
      shadowOffset: {
        width: 0,
        height: -2,
      },
      shadowOpacity: 0.25,
      shadowRadius: 3.84,
      elevation: 5,
    },
    medium: {},
    soft: {},
  },
};

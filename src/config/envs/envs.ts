import Constants from 'expo-constants';
import { dev } from './dev';
import { prod } from './prod';

const ENV = {
  dev: { ...dev },
  prod: { ...prod },
};

type ConfigTypes = typeof ENV.dev | typeof ENV.prod;

function ChooseEnv(env: string | undefined): ConfigTypes {
  switch (env) {
    case 'dev':
    default:
      return ENV.dev;
    case 'prod':
      return ENV.prod;
  }
}

export default function getEnvVars(): ConfigTypes {
  if (__DEV__) {
    const env = process.env.REACT_APP_ENV_TYPE;
    console.debug({
      args: ['Using DEV MODE. Current env is? => ', env],
      prefixes: ['CONFIG', 'ENV'],
    });
    return ChooseEnv(env);
  } else {
    const channel = Constants.manifest?.releaseChannel || process.env.APP_ENVIRONMENT;
    console.debug({
      args: ['Using Build. Current channel is? => ', channel],
      prefixes: ['CONFIG', 'CHANNEL'],
    });
    return ChooseEnv(channel);
  }
}

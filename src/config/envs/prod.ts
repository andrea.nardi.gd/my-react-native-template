import fallback from './config-fallbacks/config-prod.json';

const prod = {
  EXPO_SETTINGS: {
    developer: '',
    projectSlug: '',
  },
  NETWORK_SETTINGS: {
    AXIOS_BASE_URL: '',
    ERROR_RESPONSE_FIELD_NAME: 'message',
    ENABLE_PUSH_NOTIFICATIONS: false,
    ENABLE_DEEP_LINKING: false,
  },
  SECURITY_SETTINGS: {
    ENABLE_USER_AUTHENTICATION: false,
    PIN_LENGTH: 4,
    BIOMETRIC_AUTH: true,
    LOCK_APP_AFTER_TOO_MANY_ERRORS: true,
    MAX_SECURITY_ERRORS: 3,
    ASK_FOR_PIN_ON_SUSPENSION: false,
  },
  STORAGE_SETTINGS: {
    ALLOW_UNSAFE_STORAGE: true,
    FORCE_CLEAR: true,
  },
  MEDIA_SETTINGS: {
    ACCESS: {
      MEDIA_LIBRARY_AUDIO: true,
      MEDIA_LIBRARY_PHOTO: true,
    },
    COMPRESSION: {
      IMAGE: {
        MEDIA_LIBRARY: {
          USE_COMPRESSION: true,
        },
      },
    },
  },
  LOGS_SETTINGS: {
    ENABLE_LOGS: true,
    ENABLE_SENTRY: false,
    LOG_LEVEL: 'debug',
  },
  CONFIG_INFO: {
    CONFIG_ENV_TYPE: 'prod',
    ALLOW_FALLBACK: true,
    FALLBACK_CONFIG: { ...fallback },
  },
};

export { prod };

import { AppFonts } from '../../../types/types/styles/fonts';

export const robotoFonts = Object.freeze({
  italic: {
    '100': AppFonts.robotoThin100Italic,
    '300': AppFonts.robotoLight300Italic,
    '400': AppFonts.robotoItalic,
    '500': AppFonts.robotoMedium500Italic,
    '700': AppFonts.robotoBold700Italic,
    '900': AppFonts.robotoBlack900Italic,
    normal: AppFonts.robotoItalic,
    bold: AppFonts.robotoBold700Italic,
  },
  normal: {
    '100': AppFonts.robotoThin100,
    '300': AppFonts.robotoLight300,
    '400': AppFonts.roboto,
    '500': AppFonts.robotoMedium500,
    '700': AppFonts.robotoBold700,
    '900': AppFonts.robotoBlack900,
    normal: AppFonts.roboto,
    bold: AppFonts.robotoBold700,
  },
  default: AppFonts.roboto,
});

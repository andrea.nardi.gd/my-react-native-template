const ImagesFilesDict = {
  SplashScreen: require('../../../assets/images/splash/splash.png'),
};

type Filedict = keyof typeof ImagesFilesDict;

export const ImagesFileNames = Object.keys(ImagesFilesDict) as Filedict[];

export default ImagesFilesDict;

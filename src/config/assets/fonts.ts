import { AppFonts } from '../../types/types/styles/fonts';
import { robotoFonts } from './roboto';

const AppFontsDict = {
  [AppFonts.robotoThin100]: require('../../../assets/fonts/roboto/Roboto-Thin.ttf'),
  [AppFonts.robotoThin100Italic]: require('../../../assets/fonts/roboto/Roboto-ThinItalic.ttf'),
  [AppFonts.robotoLight300]: require('../../../assets/fonts/roboto/Roboto-Light.ttf'),
  [AppFonts.robotoLight300Italic]: require('../../../assets/fonts/roboto/Roboto-LightItalic.ttf'),
  [AppFonts.roboto]: require('../../../assets/fonts/roboto/Roboto-Regular.ttf'),
  [AppFonts.robotoItalic]: require('../../../assets/fonts/roboto/Roboto-Italic.ttf'),
  [AppFonts.robotoMedium500]: require('../../../assets/fonts/roboto/Roboto-Medium.ttf'),
  [AppFonts.robotoMedium500Italic]: require('../../../assets/fonts/roboto/Roboto-MediumItalic.ttf'),
  [AppFonts.robotoBold700]: require('../../../assets/fonts/roboto/Roboto-Bold.ttf'),
  [AppFonts.robotoBold700Italic]: require('../../../assets/fonts/roboto/Roboto-BoldItalic.ttf'),
  [AppFonts.robotoBlack900]: require('../../../assets/fonts/roboto/Roboto-Black.ttf'),
  [AppFonts.robotoBlack900Italic]: require('../../../assets/fonts/roboto/Roboto-BlackItalic.ttf'),
};

export const AppFontsDictionary = Object.freeze({
  default: AppFonts.roboto,
  [AppFonts.roboto]: { ...robotoFonts },
});

export default Object.freeze(AppFontsDict);

import { DeepLinkingResolverFunction, LINKING_PATHS } from '../../types/types/linking/DeepLinking';
import { PAGES_LIST } from './NavsConfig';

export const deepLinkingConfig = {
  config: {
    screens: {
      [PAGES_LIST.TEST_UNAUTH_STACK]: LINKING_PATHS.TEST_UNAUTH,
      [PAGES_LIST.TEST_MIDDLEWARE_STACK]: LINKING_PATHS.TEST_MIDDLEWARE,
      [PAGES_LIST.TEST_AUTH_STACK]: LINKING_PATHS.TEST_AUTH,
    },
  },
};

const VoidDeepLinkFunction = async (): Promise<void> => void 0;

export const deepLinkingActionResolver: Partial<
  Record<LINKING_PATHS, DeepLinkingResolverFunction>
> = {
  DEFAULT: VoidDeepLinkFunction as unknown as DeepLinkingResolverFunction, //HACK just a function that does nothing
};

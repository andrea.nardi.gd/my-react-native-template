import { NavigationModal } from '../../../types/types/config/navigation';
import { MODALS_LIST } from '../NavsConfig';

export const authenticatedModalsStack: Partial<Record<MODALS_LIST, NavigationModal>> = {
  TEST_MODAL_AUTH: {
    name: MODALS_LIST.TEST_MODAL,
    blockGoBack: true,
    title: '',
    component: MODALS_LIST.TEST_MODAL,
    enabled: true,
    isModal: true,
    modalTransition: 'slide',
  },
};

export const middlewareModalStack: Partial<Record<MODALS_LIST, NavigationModal>> = {
  TEST_MODAL_AUTH: {
    name: MODALS_LIST.TEST_MODAL,
    blockGoBack: true,
    title: '',
    component: MODALS_LIST.TEST_MODAL,
    enabled: true,
    isModal: true,
    modalTransition: 'slide',
  },
};

export const unAuthenticatedModalStack: Partial<Record<MODALS_LIST, NavigationModal>> = {
  TEST_MODAL_AUTH: {
    name: MODALS_LIST.TEST_MODAL,
    blockGoBack: true,
    title: '',
    component: MODALS_LIST.TEST_MODAL,
    enabled: true,
    isModal: true,
    modalTransition: 'slide',
  },
};

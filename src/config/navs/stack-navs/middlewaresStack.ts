import { FlowAlteringConditionsKeys } from '../../../types/types/config/FlowAlteringConditions';
import { NavigationWithConditions } from '../../../types/types/config/navigation';
import { AppColors } from '../../styles/GlobalStyles';
import { PAGES_LIST } from '../NavsConfig';

const middlewaresStack: Partial<Record<PAGES_LIST, NavigationWithConditions>> = {
  TEST_MIDDLEWARE_STACK: {
    name: PAGES_LIST.TEST_MIDDLEWARE_STACK,
    title: 'pinPage',
    showTitle: false,
    
    showBackButton: true,
    backButtonDestination: PAGES_LIST.TEST_MIDDLEWARE_STACK,
    topNavbarVisible: false,
    component: PAGES_LIST.TEST_MIDDLEWARE_STACK,
    enabled: true,
    pageBackgroundColor: AppColors.primary500,
    noMargin: true,
    showingConditions: [
      {
        name: FlowAlteringConditionsKeys.AUTHORIZED,
        type: 'boolean',
        condition: false,
      },
    ],
  },
};

export default middlewaresStack;

import { Navigation } from '../../../types/types/config/navigation';
import { AppColors } from '../../styles/GlobalStyles';
import { PAGES_LIST, SECTIONS_LIST } from '../NavsConfig';

const unAuthenticatedStack: Partial<Record<PAGES_LIST, Navigation>> = {
  TEST_UNAUTH_STACK: {
    name: PAGES_LIST.TEST_UNAUTH_STACK,
    title: 'login',
    showTitle: false,
    
    section: SECTIONS_LIST.HOME,
    topNavbarVisible: false,
    component: PAGES_LIST.TEST_UNAUTH_STACK,
    enabled: true,
    noMargin: true,
    pageBackgroundColor: AppColors.primary500,
  },
};

export default unAuthenticatedStack;

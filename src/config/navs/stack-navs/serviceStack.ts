import { Navigation } from '../../../types/types/config/navigation';
import { AppColors } from '../../styles/GlobalStyles';
import { PAGES_LIST, SECTIONS_LIST } from '../NavsConfig';

const serviceStack: Partial<Record<PAGES_LIST, Navigation>> = {
  TEST_SERVICE_STACK: {
    name: PAGES_LIST.TEST_SERVICE_STACK,
    blockGoBack: true,
    title: '',
    showTitle: false,
    
    showBackButton: true,
    backButtonDestination: PAGES_LIST.TEST_SERVICE_STACK,
    section: SECTIONS_LIST.SETTINGS,
    topNavbarVisible: false,
    component: PAGES_LIST.TEST_SERVICE_STACK,
    enabled: true,
    isServiceNav: true,
    pageBackgroundColor: AppColors.primary500,
    noMargin: true,
  },
  FULL_IMAGE_PAGE_PRIMARY: {
    name: PAGES_LIST.FULL_IMAGE_PAGE_PRIMARY,
    blockGoBack: true,
    title: '',
    showTitle: false,
    
    showBackButton: true,
    backButtonDestination: PAGES_LIST.FULL_IMAGE_PAGE_PRIMARY,
    section: SECTIONS_LIST.SETTINGS,
    topNavbarVisible: false,
    component: PAGES_LIST.FULL_IMAGE_PAGE_PRIMARY,
    pageBackgroundColor: AppColors.primary500,
    enabled: true,
    isServiceNav: true,
  },
  FULL_IMAGE_PAGE_SECONDARY: {
    name: PAGES_LIST.FULL_IMAGE_PAGE_SECONDARY,
    blockGoBack: true,
    title: '',
    showTitle: false,
    
    showBackButton: true,
    backButtonDestination: PAGES_LIST.FULL_IMAGE_PAGE_SECONDARY,
    section: SECTIONS_LIST.SETTINGS,
    pageBackgroundColor: AppColors.secondary100,
    topNavbarVisible: false,
    component: PAGES_LIST.FULL_IMAGE_PAGE_SECONDARY,
    enabled: true,
    isServiceNav: true,
  },
};

export default serviceStack;

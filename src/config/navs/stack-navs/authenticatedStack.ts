import { Navigation } from '../../../types/types/config/navigation';
import { AppColors } from '../../styles/GlobalStyles';
import { PAGES_LIST, SECTIONS_LIST } from '../NavsConfig';

const authenticatedStack: Partial<Record<PAGES_LIST, Navigation>> = {
  TEST_AUTH_STACK: {
    name: PAGES_LIST.TEST_AUTH_STACK,
    title: 'settings',
    showTitle: true,

    showLogo: true,
    showBackButton: false,
    backButtonDestination: PAGES_LIST.TEST_AUTH_STACK,
    section: SECTIONS_LIST.SETTINGS,
    topNavbarVisible: true,
    component: PAGES_LIST.TEST_AUTH_STACK,
    enabled: true,
    drawerNavigator: true,
  },
  TEST_AUTH_LIST_INFO: {
    name: PAGES_LIST.TEST_AUTH_LIST_INFO,
    title: 'settings',
    showTitle: true,
    noScroll: true,
    showLogo: true,
    showBackButton: false,
    backButtonDestination: PAGES_LIST.TEST_AUTH_LIST_INFO,
    section: SECTIONS_LIST.SETTINGS,
    topNavbarVisible: true,
    component: PAGES_LIST.TEST_AUTH_LIST_INFO,
    enabled: true,
    drawerNavigator: true,
    pageBackgroundColor: AppColors.primary300,
  },
};

export default authenticatedStack;

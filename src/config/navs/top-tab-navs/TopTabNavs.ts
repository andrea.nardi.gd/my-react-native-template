import { PAGES_LIST, SECTIONS_LIST, TOP_TAB_PAGES } from '../NavsConfig';
import { Navigation } from '../../../types/types/config/navigation';

export const unAuthenticatedTopTabNavs: Record<string, Navigation> = {};

export const topTabNavs: Partial<Record<TOP_TAB_PAGES, Navigation[]>> = {
  TEST_TOP_TAB: [
    {
      name: 'TAB_1',
      title: 'test1',
      showTitle: true,
      showLogo: true,
      
      showBackButton: false,
      backButtonlabel: 'back',
      section: SECTIONS_LIST.HOME,
      topNavbarVisible: true,
      onlineNeeded: true,
      pageBackgroundColor: 'green',
      component: PAGES_LIST.TEST_AUTH_STACK,
      icon: 'LIST',
      enabled: true,
    },
    {
      name: 'TAB_2',
      title: 'test2',
      showTitle: true,
      showLogo: true,
      
      section: SECTIONS_LIST.HOME,
      topNavbarVisible: true,
      component: PAGES_LIST.TEST_AUTH_STACK,
      pageBackgroundColor: 'blue',
      icon: 'cards',
      enabled: true,
    },
    {
      name: 'TAB_3',
      title: 'test2',
      showTitle: true,
      showLogo: true,
      
      showBackButton: false,
      backButtonlabel: 'back',
      section: SECTIONS_LIST.HOME,
      topNavbarVisible: true,
      onlineNeeded: true,
      component: PAGES_LIST.TEST_AUTH_STACK,
      pageBackgroundColor: 'yellow',
      icon: 'home',
      enabled: true,
    },
  ],
};

import { TopTabOptions } from '../../../types/types/navigation/navigators/topTab';
import { AppColors } from '../../styles/GlobalStyles';
import { TOP_TAB_PAGES } from '../NavsConfig';

export const topTabOptions: Partial<Record<TOP_TAB_PAGES, TopTabOptions>> = {
  DEFAULT: {
    titleBackgroundColor: AppColors.secondary900,
    titleTextColor: AppColors.ink900,
    showIcon: true,
    showText: true,
    activeIndicatorHeight: 3,
    activeTabUnderlineColor: AppColors.primary500,
  },
  TEST_TOP_TAB: {
    titleBackgroundColor: AppColors.secondary900,
    titleTextColor: AppColors.ink900,
    showIcon: true,
    showText: true,
    activeIndicatorHeight: 3,
    activeTabUnderlineColor: AppColors.primary500,
    firstScreen: 1,
    clearStackOnLeave: true,
  },
};

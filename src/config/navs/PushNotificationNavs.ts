import { PushNotificationTypes } from '../../types/types/notification';
import { ImagePagesDict } from './ImagePages';
import { PAGES_LIST } from './NavsConfig';

type NotificationRedirectInfo = {
  //NOT USED, but could become useful if push notification are implemented someday
  triggers: string[];
  destination: PAGES_LIST;
  touchDestination?: string;
  touchDestinationAdditionalParams?: Record<string, unknown>;
  additionalParams?: Record<string, unknown>;
};

type NotificationRedirectNavs = Record<PushNotificationTypes, NotificationRedirectInfo>;

const notificationRedirectNavs: NotificationRedirectNavs = {
  [PushNotificationTypes.TEST_PUSH_NOTIFICATION]: {
    triggers: [PAGES_LIST.TEST_AUTH_STACK],
    destination: PAGES_LIST.FULL_IMAGE_PAGE_PRIMARY,
    touchDestination: PAGES_LIST.FULL_IMAGE_PAGE_PRIMARY,
    additionalParams: { ...ImagePagesDict.TEST_IMAGE_PAGE },
  },
};

export default notificationRedirectNavs;

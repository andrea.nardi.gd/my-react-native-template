import { DefaultTheme } from '@react-navigation/native';
import { globalStyles } from '../../styles/GlobalStyles';

export const NavigationTheme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    background: globalStyles.COLOR_CONFIG.appDefaultBackgroundColor,
  },
};

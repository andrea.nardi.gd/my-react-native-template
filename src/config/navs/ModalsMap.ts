import AlertModal from '../../components/modals/AlertModal';

const ModalsMap = {
  ALERT_MODAL_PATH: {
    name: 'ALERT_MODAL',
    component: AlertModal,
    enabled: true,
  },
};

export default ModalsMap;

import React from 'react';
import MockFieldExplanationPage from '../../../pages/test-pages/mock-field-explanation-page/MockFieldExplanationPage';
import TestTopTabPage from '../../../pages/test-pages/TestTopTabPage';
import { PAGES_LIST } from '../NavsConfig';

const AuthorizedNavComponents: Partial<Record<PAGES_LIST, React.ReactNode>> = {
  [PAGES_LIST.TEST_AUTH_LIST_INFO]: MockFieldExplanationPage,
  [PAGES_LIST.TEST_TOP_TAB_PAGE]: TestTopTabPage,
};

export default AuthorizedNavComponents;

import React from 'react';
import TestSettingModalScreen from '../../../pages/service-modals/TestSettingsModal';
import { MODALS_LIST } from '../NavsConfig';

const ModalsNavsComponents: Partial<Record<MODALS_LIST, React.ReactNode>> = {
  TEST_MODAL: TestSettingModalScreen,
};

export default ModalsNavsComponents;

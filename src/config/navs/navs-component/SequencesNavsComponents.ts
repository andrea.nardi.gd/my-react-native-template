import React from 'react';
import { PAGES_LIST } from '../NavsConfig';

const SequencesNavComponents: Partial<Record<PAGES_LIST, React.ReactNode>> = {};

export default SequencesNavComponents;

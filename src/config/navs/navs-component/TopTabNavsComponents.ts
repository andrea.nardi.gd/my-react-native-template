import { PAGES_LIST } from '../NavsConfig';

const TopTabNavsComponents: Partial<Record<PAGES_LIST, React.ReactNode>> = {};

export default TopTabNavsComponents;

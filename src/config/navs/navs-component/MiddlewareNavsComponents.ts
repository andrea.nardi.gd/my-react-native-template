import React from 'react';
import { PAGES_LIST } from '../NavsConfig';

const MiddlewareNavsComponents: Partial<Record<PAGES_LIST, React.ReactNode>> = {};

export default MiddlewareNavsComponents;

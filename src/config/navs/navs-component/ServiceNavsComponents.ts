import React from 'react';
import MockExplanationPage from '../../../pages/test-pages/mock-explanation-page/ExplanationPage';
import { PAGES_LIST } from '../NavsConfig';

const ServiceNavsComponents: Partial<Record<PAGES_LIST | 'default', React.ReactNode>> = {
  default: MockExplanationPage,
};

export default ServiceNavsComponents;

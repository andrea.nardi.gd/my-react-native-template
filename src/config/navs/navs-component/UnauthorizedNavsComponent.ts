import React from 'react';
import { PAGES_LIST } from '../NavsConfig';

const UnauthorizedNavComponents: Partial<Record<PAGES_LIST, React.ReactNode>> = {};

export default UnauthorizedNavComponents;

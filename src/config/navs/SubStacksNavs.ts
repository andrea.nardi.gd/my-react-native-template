import { PAGES_LIST } from './NavsConfig';
import { Navigation, NavigationWithConditions } from '../../types/types/config/navigation';
import { AppColors } from '../styles/GlobalStyles';
import { FlowAlteringConditionsKeys } from '../../types/types/config/FlowAlteringConditions';
import authenticatedStack from './stack-navs/authenticatedStack';
import serviceStack from './stack-navs/serviceStack';
import middlewaresStack from './stack-navs/middlewaresStack';
import unAuthenticatedStack from './stack-navs/unAuthenticatedStack';

export const unAuthenticatedSubStackNavs: Partial<Record<PAGES_LIST, NavigationWithConditions>> = {
  ...unAuthenticatedStack,
};

export const authorizationScreens: Partial<Record<PAGES_LIST, NavigationWithConditions>> = {
  INSERT_PIN_PAGE: {
    name: PAGES_LIST.INSERT_PIN_PAGE,
    title: 'Insert Pin',
    showTitle: false,

    topNavbarVisible: false,
    component: PAGES_LIST.INSERT_PIN_PAGE,
    pageBackgroundColor: AppColors.primary500,
    enabled: true,
    drawerNavigator: true,
    showingConditions: [
      {
        name: FlowAlteringConditionsKeys.AUTHORIZED,
        type: 'boolean',
        condition: false,
      },
    ],
  },
};

export const middlewareScreens: Record<string, NavigationWithConditions> = {
  ...middlewaresStack,
};

export const serviceNavs: Record<string, Navigation> = {
  ...serviceStack,
};

export const authenticatedSubStackNavs: Record<string, Navigation> = {
  ...authenticatedStack,
};

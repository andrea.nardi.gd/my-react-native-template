import { NavigationModal, NavigationWithConditions } from '../../types/types/config/navigation';
import { authenticatedBottomTabNavs, unAuthenticatedBottomTabNavs } from './BottomTabNavs';
import {
  authenticatedModalsStack,
  middlewareModalStack,
  unAuthenticatedModalStack,
} from './modals/modalsStack';
import {
  AuthenticatedSequenceNavs,
  MiddlewareSequenceNavs,
  SequenceNavs,
  UnAuthenticatedSequenceNavs,
} from './SequenceNavs';
import {
  authenticatedSubStackNavs,
  authorizationScreens,
  middlewareScreens,
  serviceNavs,
  unAuthenticatedSubStackNavs,
} from './SubStacksNavs';

type NavigationMap = {
  modals: Record<string, NavigationModal>;
  standard: Record<string, NavigationWithConditions>;
  sequences: SequenceNavs;
};

export const unAuthenticatedNavigationMap: NavigationMap = {
  standard: {
    ...unAuthenticatedBottomTabNavs,
    ...unAuthenticatedSubStackNavs,
    ...serviceNavs,
  },
  sequences: {
    ...UnAuthenticatedSequenceNavs,
  },
  modals: {
    ...unAuthenticatedModalStack,
  },
};

export const middlewareScreensMap: NavigationMap = {
  standard: {
    ...authorizationScreens,
    ...middlewareScreens,
  },
  sequences: {
    ...MiddlewareSequenceNavs,
  },
  modals: {
    ...middlewareModalStack,
  },
};

export const authenticatedNavigationMap: NavigationMap = {
  standard: {
    ...authenticatedBottomTabNavs,
    ...authenticatedSubStackNavs,
    ...serviceNavs,
  },
  sequences: {
    ...AuthenticatedSequenceNavs,
  },
  modals: {
    ...authenticatedModalsStack,
  },
};

export const modalsNavigationMap: Record<string, NavigationModal> = {};

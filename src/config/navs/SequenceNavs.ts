import { SequenceStep } from '../../types/types/navigation/sequence/sequence';
import { SEQUENCES_LIST } from './NavsConfig';
import { TestSequence } from './sequences/authenticated/TestSequence';

export type SequenceNavs = Partial<Record<SEQUENCES_LIST, SequenceStep[]>>;

export const AuthenticatedSequenceNavs: SequenceNavs = {
  TEST_SEQUENCE: TestSequence,
};

export const UnAuthenticatedSequenceNavs: SequenceNavs = {};

export const MiddlewareSequenceNavs: SequenceNavs = {};

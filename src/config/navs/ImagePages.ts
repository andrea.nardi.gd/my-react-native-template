import React from 'react';
import MockEmptyPage from '../../pages/test-pages/MockPagePlaceHolder';
import { ColorVariants } from '../../types/types/styles/themes';
import ImagesFilesDict from '../assets/images';
import { IMAGE_PAGES } from './NavsConfig';

export type Redirect = {
  buttonType?: 'confirm' | 'cancel';
  description: string;
  page: string;
  optionalParams?: object;
};

export type ExternalRedirect = {
  buttonType?: 'confirm' | 'cancel';
  description: string;
  redirectType: 'url' | 'phone' | 'email';
  link: string;
  optionalParams?: object;
};

export type descriptionsType =
  | {
      text: string;
      params?: unknown;
    }
  | string;

export type ImagePageProps = {
  image?: IMAGE_PAGES | null;
  background: ColorVariants;
  imageType?: 'svg' | 'png';
  title: string;
  titleParams?: string[];
  subtitles?: string;
  descriptions?: string[] | descriptionsType[];
  redirects?: Redirect[];
  externalRedirects?: ExternalRedirect[];
};

export const ImagePagesPng: Partial<Record<IMAGE_PAGES, string>> = {
  TEST_IMAGE_PAGE: ImagesFilesDict.SplashScreen,
};

export const ImagePagesImageDict: Partial<Record<IMAGE_PAGES, React.ReactNode>> = {
  TEST_IMAGE_PAGE: MockEmptyPage, //ADD HERE SVG CONVERTED COMPONENTS TO WRAP THEM IN CONTAINER
};

export const ImagePagesDict: Partial<Record<IMAGE_PAGES, ImagePageProps>> = {
  TEST_IMAGE_PAGE: {
    image: IMAGE_PAGES.TEST_IMAGE_PAGE,
    background: ColorVariants.PRIMARY,
    title: 'testTitle',
    descriptions: ['testDescription'],
  },
};

import { PAGES_LIST, SECTIONS_LIST, TOP_TAB_PAGES } from './NavsConfig';
import { Navigation } from '../../types/types/config/navigation';
import { AppColors } from '../styles/GlobalStyles';
import { PAGES_CALLBACK_PATHS } from '../../types/types/pages/PagesCallback';

export const unAuthenticatedBottomTabNavs: Partial<Record<PAGES_LIST, Navigation>> = {};

export const authenticatedBottomTabNavs: Partial<Record<PAGES_LIST, Navigation>> = {
  TEST_BOTTOM_TAB_PAGE_2: {
    name: PAGES_LIST.TEST_BOTTOM_TAB_PAGE_2,
    title: 'home',
    showTitle: true,
    showLogo: true,

    showBackButton: false,
    backButtonlabel: 'back',
    section: SECTIONS_LIST.HOME,
    topNavbarVisible: true,
    onlineNeeded: true,
    component: PAGES_LIST.TEST_BOTTOM_TAB_PAGE_2,
    icon: 'events',
    enabled: true,
    askConfirmationOnChange: true,
    onScrollCallback: {
      method: PAGES_CALLBACK_PATHS.DEFAULT,
    },
    //noScroll: true,
    //nestedTopNavigatorName: TOP_TAB_PAGES.TEST_TOP_TAB,
    pageBackgroundColor: 'yellow',
    //noMargin: true,
  },
  TEST_BOTTOM_TAB_PAGE_1: {
    name: PAGES_LIST.TEST_BOTTOM_TAB_PAGE_1,
    title: 'home',
    showTitle: true,
    showLogo: true,

    showBackButton: false,
    backButtonlabel: 'back',
    section: SECTIONS_LIST.SETTINGS,
    topNavbarVisible: true,
    onlineNeeded: true,
    component: PAGES_LIST.TEST_TOP_TAB_PAGE,
    icon: 'events',
    enabled: true,
    noScroll: true,
    nestedTopNavigatorName: TOP_TAB_PAGES.TEST_TOP_TAB,
    pageBackgroundColor: AppColors.secondary100,
    drawerNavigator: true,
    onLeaveCallBack: {
      method: PAGES_CALLBACK_PATHS.TEST_PAGE_CALLBACK,
    },
    //noMargin: true,
  },
};

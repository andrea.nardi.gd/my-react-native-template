import { SequenceStep, StepsBehavior } from '../../../../types/types/navigation/sequence/sequence';
import { PAGES_LIST, SECTIONS_LIST } from '../../NavsConfig';

export const TestSequence: SequenceStep[] = [
  {
    name: PAGES_LIST.TEST_SEQUENCE_PAGE_1,
    showBackButton: true,
    backButtonlabel: 'back',
    backButtonDestination: PAGES_LIST.TEST_AUTH_STACK,
    title: 'informations',
    label: 'informations',
    showTitle: true,
    
    component: PAGES_LIST.TEST_SEQUENCE_PAGE_1,
    section: SECTIONS_LIST.HOME,
    topNavbarVisible: true,
    enabled: true,
    askConfirmationOnBack: true,
    askConfirmationOnChange: true,
    orderInSequence: 1,
    goBack: {
      behavior: StepsBehavior.REDIRECT,
      redirect: PAGES_LIST.TEST_AUTH_STACK,
      cleanData: false,
    },
    error: {
      behavior: StepsBehavior.STAY,
    },
    goForward: {
      behavior: StepsBehavior.GO_FORWARD,
      redirect: PAGES_LIST.TEST_SEQUENCE_PAGE_2,
    },
  },
  {
    name: PAGES_LIST.TEST_SEQUENCE_PAGE_2,
    showBackButton: true,
    backButtonlabel: 'back',
    backButtonDestination: PAGES_LIST.TEST_AUTH_STACK,
    title: 'informations',
    label: 'informations',
    showTitle: true,
    
    component: PAGES_LIST.TEST_SEQUENCE_PAGE_2,
    section: SECTIONS_LIST.HOME,
    topNavbarVisible: true,
    enabled: true,
    askConfirmationOnBack: true,
    askConfirmationOnChange: true,
    orderInSequence: 1,
    goBack: {
      behavior: StepsBehavior.REDIRECT,
      redirect: PAGES_LIST.TEST_SEQUENCE_PAGE_1,
      cleanData: false,
    },
    error: {
      behavior: StepsBehavior.STAY,
    },
    goForward: {
      behavior: StepsBehavior.GO_FORWARD,
      redirect: PAGES_LIST.TEST_AUTH_STACK,
    },
  },
  {
    name: PAGES_LIST.TEST_SEQUENCE_PAGE_2 + 1,
    showBackButton: true,
    backButtonlabel: 'back',
    backButtonDestination: PAGES_LIST.TEST_AUTH_STACK,
    title: 'informations',
    label: 'informations',
    showTitle: true,
    
    component: PAGES_LIST.TEST_SEQUENCE_PAGE_2,
    section: SECTIONS_LIST.HOME,
    topNavbarVisible: true,
    enabled: true,
    askConfirmationOnBack: true,
    askConfirmationOnChange: true,
    orderInSequence: 1,
    goBack: {
      behavior: StepsBehavior.REDIRECT,
      redirect: PAGES_LIST.TEST_SEQUENCE_PAGE_1,
      cleanData: false,
    },
    error: {
      behavior: StepsBehavior.STAY,
    },
    goForward: {
      behavior: StepsBehavior.GO_FORWARD,
      redirect: PAGES_LIST.TEST_AUTH_STACK,
    },
  },
  {
    name: PAGES_LIST.TEST_SEQUENCE_PAGE_2 + 2,
    showBackButton: true,
    backButtonlabel: 'back',
    backButtonDestination: PAGES_LIST.TEST_AUTH_STACK,
    title: 'informations',
    label: 'informations',
    showTitle: true,
    
    component: PAGES_LIST.TEST_SEQUENCE_PAGE_2,
    section: SECTIONS_LIST.HOME,
    topNavbarVisible: true,
    enabled: true,
    askConfirmationOnBack: true,
    askConfirmationOnChange: true,
    orderInSequence: 1,
    goBack: {
      behavior: StepsBehavior.REDIRECT,
      redirect: PAGES_LIST.TEST_SEQUENCE_PAGE_1,
      cleanData: false,
    },
    error: {
      behavior: StepsBehavior.STAY,
    },
    goForward: {
      behavior: StepsBehavior.GO_FORWARD,
      redirect: PAGES_LIST.TEST_AUTH_STACK,
    },
  },
];

import ApiMap from './api/ApiMap';
import getEnvVars from './envs/envs';
import MessagesDictionary from './messages';
import PreferencesStorageKeys, { PreferencesStorageKeysList } from './storage/preferences';
import SecureStorageKeys, { SecureStorageKeysList } from './storage/secure';
import defaultLocalization from './locale/defaultTranslation.json';

const mergeConfig = () => {
  return getEnvVars();
};

const baseConfig = {
  API_MAP: { ...ApiMap },
  STORAGE_KEYS: { ...SecureStorageKeys, ...PreferencesStorageKeys },
  STORAGE_KEYS_LIST: [...SecureStorageKeysList, ...PreferencesStorageKeysList],
  DEFAULT_LOCALIZATION: { ...defaultLocalization },
  MESSAGES_DICTIONARY: { ...MessagesDictionary },
  LAYOUT: {
    ENABLE_SIDE_DRAWER_NAVIGATION: true,
    ENABLE_BOTTOM_TAB_NAVIGATION: true,
    USE_IOS_BAR_HACK: false,
    USING_DRAWER_NAVIGATION: true,
    HEADER_ABOVE_DRAWER: false, //IMPORTANT: keep this always true for android
  },
};

export default Object.freeze({ ...mergeConfig(), ...baseConfig });

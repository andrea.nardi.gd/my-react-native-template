const SecureStorageKeys = {
  pin: 'pin',
};

export const boolKeys = [''];

export const SecureStorageKeysList = Object.keys(SecureStorageKeys).map(
  (key: string) => SecureStorageKeys[key as keyof typeof SecureStorageKeys],
);

export default SecureStorageKeys;

const PreferencesStorageKeys = {};

export const boolKeys = [];

export const PreferencesStorageKeysList = Object.keys(PreferencesStorageKeys).map(
  (key: string) => PreferencesStorageKeys[key as keyof typeof PreferencesStorageKeys],
);

export default PreferencesStorageKeys;

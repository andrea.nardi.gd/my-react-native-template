export const LogLevels = {
  debug: 0,
  info: 1,
  warning: 2,
  error: 3,
};

export const LogLevelsMap = {
  debug: 'debug',
  info: 'info',
  warning: 'warning',
  error: 'error',
};

import {
  configLoggerType,
  consoleTransport,
  fileAsyncTransport,
  transportFunctionType,
} from 'react-native-logs';
import * as FileSystem from 'expo-file-system';
import loggerColors from './loggerColors';
import { InteractionManager } from 'react-native';
import config from '../index';
import { LogLevels, LogLevelsMap } from './loggerMap';

export const loggerPrefixesDelimiters = {
  square: ['[', ']'],
  graphs: ['{', '}'],
  round: ['(', ')'],
};

const buildTransportArray = (): transportFunctionType[] => {
  const transports: transportFunctionType[] = [];

  if (config.LOGS_SETTINGS.ENABLE_LOGS) {
    if (__DEV__) {
      transports.push(consoleTransport);
    } else {
      transports.push(fileAsyncTransport);
    }
  }

  return transports;
};

const localLoggerConfig: configLoggerType = {
  levels: LogLevels,
  transport: buildTransportArray(),
  severity: __DEV__
    ? LogLevelsMap.debug
    : LogLevelsMap[config.LOGS_SETTINGS.LOG_LEVEL as keyof typeof LogLevelsMap],
  transportOptions: {
    FS: FileSystem,
    colors: {
      debug: loggerColors.blue,
      info: loggerColors.green,
      warning: loggerColors.magenta,
      error: loggerColors.red,
    },
  },
  async: true,
  asyncFunc: InteractionManager.runAfterInteractions,
  dateFormat: 'time',
  printLevel: true,
  printDate: true,
  enabled: true,
};

export default localLoggerConfig;

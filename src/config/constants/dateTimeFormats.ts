const defaultAppDateStringFormat = 'dd/MM/yyyy';
const defaultFullMonthNameStringFormat = 'dd MMMM yyyy';
const defaultAppTimeStringFormat = 'HH:mm';

export default {
  input: {
    standardDate: defaultAppDateStringFormat,
    standardTime: defaultAppTimeStringFormat,
  },
  output: {
    dateCompact: 'dd MMM yyyy',
    dateFullMonthName: defaultFullMonthNameStringFormat,
    fullDateWithTime: `${defaultFullMonthNameStringFormat}, ${defaultAppTimeStringFormat}`,
    monthOnlyFull: 'MMMM',
  },
};

import { Instruction } from '../../types/types/config/instructions';

export enum InstructionKeys {
  TEST_INSTRUCTION_SET = 'TEST_INSTRUCTION_SET',
}

export const InstructionsDict: Record<InstructionKeys, Instruction> = {
  TEST_INSTRUCTION_SET: {
    title: 'testInstructionTitle',
    subtitle: 'testInstructionDescription',
    steps: [
      {
        image: 'testInstructionImage1',
        description: 'testInstructionTimeline1',
      },
      {
        image: 'testInstructionImage2',
        description: 'testInstructionTimeline2',
      },
      {
        image: 'testInstructionImage3',
        description: 'testInstructionTimeline3',
      },
    ],
  },
};

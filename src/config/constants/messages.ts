const SnackBarConstants = {
  durationInSeconds: 9,
  type: {
    warning: 'warning',
    success: 'sucess',
    error: 'error',
    danger: 'danger',
  },
  position: 'top',
  size: 'full',
};

export default SnackBarConstants;

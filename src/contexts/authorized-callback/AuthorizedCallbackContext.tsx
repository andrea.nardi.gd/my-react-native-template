import React, { useCallback, useState } from 'react';
import AuthorizationModal from '../../components/layout/modals/AuthorizationModal';
import { useAppSelector } from '../../store/hooks';
import { AppXOR } from '../../types/types/utils';

interface ConnectedWalletContextProps {
  children: React.ReactNode | React.ReactNode[];
}

interface AuthorizedCallbackT<T> {
  authenticatedCallBackWrapper: (callback: T) => void;
}

export const AuthorizedCallback = React.createContext<Partial<AuthorizedCallbackT<unknown>>>({});

const AuthorizedCallBackProvider = ({ children }: ConnectedWalletContextProps): JSX.Element => {
  const [callback, setCallback] = useState<AppXOR<() => unknown, null>>(null);
  const [authorizationModalVisible, setAuthorizationModalVisible] = useState(false);
  const isAuthorized = useAppSelector((store) => store.localAuth.isUserAuthorized);

  const clearCallback = useCallback(() => {
    setCallback(null);
    setAuthorizationModalVisible(false);
  }, [callback, authorizationModalVisible]);

  const executeCallback = useCallback(() => {
    if (callback) {
      callback();
    }
    clearCallback();
  }, [callback, clearCallback]);

  const authenticatedCallBackWrapper = useCallback(
    (callback: unknown) => {
      if (isAuthorized) {
        setCallback(() => callback);
        setAuthorizationModalVisible(true);
      }
    },
    [isAuthorized],
  );

  return (
    <AuthorizedCallback.Provider value={{ authenticatedCallBackWrapper }}>
      {children}
      <AuthorizationModal
        toggleModal={clearCallback}
        isVisible={authorizationModalVisible}
        authorizedCallback={executeCallback}
      />
    </AuthorizedCallback.Provider>
  );
};

export default AuthorizedCallBackProvider;

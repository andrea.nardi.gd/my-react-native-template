import { configureStore, Store } from '@reduxjs/toolkit';
import rootReducer, { RootState } from '../reducers';

function setupStore(): Store<RootState> {
  return configureStore({
    reducer: rootReducer,
  });
}

const store = setupStore();

export default store;

export type AppDispatch = typeof store.dispatch;

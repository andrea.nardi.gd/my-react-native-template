import { AppStateStatus } from 'react-native';
import {
  AppStatusAction,
  SET_APP_INITIALIZED,
  UNSET_APP_INITIALIZED,
  SET_INIZIALIZATION_FAILED,
  UNSET_INITIALIZATION_FAILED,
  SET_APP_STATE,
  SET_APP_THREAD_LOADING,
  SET_THREAD_LOADING_PROGRESS,
} from '../../actions/app-status';

interface InitialState {
  activityState: AppStateStatus;
  appInitializationFailed: boolean;
  appInitialized: boolean;
  appThreadLoading: boolean;
  threadLoadingProgress: number;
}

export const initialState: InitialState = {
  activityState: 'unknown',
  appInitializationFailed: false,
  appInitialized: false,
  appThreadLoading: false,
  threadLoadingProgress: 0,
};

export function reducer(
  state: InitialState = initialState,
  action: AppStatusAction,
): typeof initialState {
  switch (action.type) {
    case SET_APP_STATE:
      return {
        ...state,
        activityState: action.payload,
      };
    case SET_APP_THREAD_LOADING: {
      return {
        ...state,
        ...(action.payload === false && { threadLoadingProgress: 0 }),
        appThreadLoading: action.payload,
      };
    }
    case SET_THREAD_LOADING_PROGRESS: {
      return {
        ...state,
        threadLoadingProgress: action.payload,
      };
    }
    case SET_INIZIALIZATION_FAILED:
      return {
        ...state,
        appInitializationFailed: true,
      };
    case SET_APP_INITIALIZED:
      return {
        ...state,
        appInitialized: true,
      };

    case UNSET_APP_INITIALIZED:
      return {
        ...state,
        appInitialized: false,
      };

    case UNSET_INITIALIZATION_FAILED:
      return {
        ...state,
        appInitializationFailed: false,
        //appInitialized: true,
      };

    default:
      return state;
  }
}

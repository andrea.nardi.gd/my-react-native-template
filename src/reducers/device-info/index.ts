import {
  SetDeviceInfoAction,
  SET_DEVICE_OS,
  SET_IS_ROOTED,
  SET_DEVICE_TYPE,
  SET_DEVICE_TYPE_AND_OS,
} from '../../actions/device-info';
import { AppDeviceOS, AppDeviceType } from '../../types/types/device';

type InitialState = {
  deviceType: AppDeviceType;
  deviceOS: AppDeviceOS;
  isRooted: boolean;
};
export const initialState: InitialState = {
  deviceType: 'PHONE',
  deviceOS: 'UNKNOWN',
  isRooted: false,
};

export function reducer(
  state: InitialState = initialState,
  action: SetDeviceInfoAction,
): InitialState {
  switch (action.type) {
    case SET_DEVICE_TYPE_AND_OS:
      return {
        ...state,
        deviceOS: action.payload.os,
        deviceType: action.payload.type,
      };
    case SET_DEVICE_TYPE:
      return {
        ...state,
        deviceType: action.payload,
      };
    case SET_DEVICE_OS:
      return {
        ...state,
        deviceOS: action.payload,
      };
    case SET_IS_ROOTED:
      return {
        ...state,
        isRooted: action.payload,
      };
    default:
      return state;
  }
}

import {
  SetConnectionInfoAction,
  SET_FLIGHT_MODE,
  SET_IS_ONLINE,
} from '../../actions/connection-info';

export const initialState = {
  isOnline: false,
  flightMode: false,
};

export function reducer(
  state: typeof initialState = initialState,
  action: SetConnectionInfoAction,
): typeof initialState {
  switch (action.type) {
    case SET_IS_ONLINE:
      return {
        ...state,
        isOnline: action.payload,
      };
    case SET_FLIGHT_MODE:
      return {
        ...state,
        isOnline: !action.payload,
        flightMode: action.payload,
      };
    default:
      return state;
  }
}

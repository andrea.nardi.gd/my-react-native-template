import {
  SetThemeAction,
  SET_ACTIVE_COLOR_SCHEME,
  SET_FONT_SIZE_MULTIPLIER,
} from '../../actions/theme';
import { AppColors } from '../../config/styles/GlobalStyles';

interface InitialState {
  AppColors: typeof AppColors;
  fontSizeMultiplier: number;
}

export const initialState: InitialState = {
  AppColors: AppColors,
  fontSizeMultiplier: 1,
};

export function reducer(state: InitialState = initialState, action: SetThemeAction): InitialState {
  switch (action.type) {
    case SET_ACTIVE_COLOR_SCHEME:
      return {
        ...state,
        AppColors: action.payload,
      };
    case SET_FONT_SIZE_MULTIPLIER:
      return {
        ...state,
        fontSizeMultiplier: action.payload,
      };
    default:
      return state;
  }
}

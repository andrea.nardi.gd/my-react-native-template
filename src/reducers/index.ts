import { AnyAction, combineReducers } from '@reduxjs/toolkit';

import * as deviceInfo from './device-info';
import * as localization from './localization';
import * as localAuth from './local-auth';
import * as config from './config';
import * as appStatus from './app-status';
import * as connectionInfo from './connection-info';
import * as messageContext from './message-context';
import * as navigationContext from './navigation';
import * as theme from './theme';

export const initialState = {
  deviceInfo: deviceInfo.initialState,
  localization: localization.initialState,
  localAuth: localAuth.initialState,
  config: config.initialState,
  appStatus: appStatus.initialState,
  connectionInfo: connectionInfo.initialState,
  messageContext: messageContext.initialState,
  navigationContext: navigationContext.initialState,
  theme: theme.initialState,
};

const appReducer = combineReducers({
  deviceInfo: deviceInfo.reducer,
  localization: localization.reducer,
  localAuth: localAuth.reducer,
  config: config.reducer,
  appStatus: appStatus.reducer,
  connectionInfo: connectionInfo.reducer,
  messageContext: messageContext.reducer,
  navigationContext: navigationContext.reducer,
  theme: theme.reducer,
});

const rootReducer = (
  state: RootState,
  action: AnyAction = { type: '' },
): ReturnType<typeof appReducer> => {
  if (action?.type === 'LOGOUT') {
    const { deviceInfo, config, appStatus, connectionInfo, messageContext } = state; // preserve system related field
    state = { ...initialState, deviceInfo, config, appStatus, connectionInfo, messageContext };
  }

  return appReducer(state, action);
};

export default rootReducer as typeof appReducer;

export type RootState = typeof initialState;

import {
  DELETE_ACTIVE_MESSAGE,
  HIDE_QUIT_MESSAGE,
  MessageContextAction,
  SET_ACTIVE_MESSAGE,
  SHOW_QUIT_MESSAGE,
} from '../../actions/message-context';
import { SnackBarMessage } from '../../types/types/messages';
import { AppXOR } from '../../types/types/utils';

interface InitialState {
  activeSnackBar: AppXOR<SnackBarMessage, null>;
  activeQuitMessage: AppXOR<SnackBarMessage, null>;
}

export const initialState: InitialState = {
  activeSnackBar: null,
  activeQuitMessage: null,
};

export function reducer(
  state: InitialState = initialState,
  action: MessageContextAction,
): InitialState {
  switch (action.type) {
    case SET_ACTIVE_MESSAGE: {
      return {
        ...state,
        activeSnackBar: action.payload,
      };
    }
    case DELETE_ACTIVE_MESSAGE: {
      return {
        ...state,
        activeSnackBar: null,
      };
    }
    case SHOW_QUIT_MESSAGE: {
      return {
        ...state,
        activeQuitMessage: action.payload,
      };
    }
    case HIDE_QUIT_MESSAGE: {
      return {
        ...state,
        activeQuitMessage: null,
      };
    }
    default:
      return state;
  }
}

import { LocalizationAction, SET_LANG } from '../../actions/localization';
import { LocalizationLang } from '../../types/types/localization';

interface InitialState {
  lang: LocalizationLang;
}

export const initialState: InitialState = {
  lang: LocalizationLang.EN,
};

export function reducer(
  state: InitialState = initialState,
  action: LocalizationAction,
): typeof initialState {
  switch (action.type) {
    case SET_LANG:
      return {
        ...state,
        lang: action.payload,
      };
    default:
      return state;
  }
}

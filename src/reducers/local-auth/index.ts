import {
  SetLocalAuthAction,
  SET_LOCAL_AUTH_ENROLLED,
  SET_LOCAL_AUTH_POSSIBLE,
  SET_LOCAL_AUTH_ENABLED,
  SET_IS_PIN_SET,
  SET_IS_USER_AUTHORIZED,
} from '../../actions/local-auth';

export const initialState = {
  biometricAuthEnrolled: false,
  biometricAuthPossible: false,
  biometricAuthEnabled: false,
  isPinSet: false,
  isUserAuthorized: true,
};

export function reducer(
  state: typeof initialState = initialState,
  action: SetLocalAuthAction,
): typeof initialState {
  switch (action.type) {
    case SET_LOCAL_AUTH_ENABLED:
      return {
        ...state,
        biometricAuthEnabled: action.payload,
      };
    case SET_IS_USER_AUTHORIZED:
      return {
        ...state,
        isUserAuthorized: action.payload,
      };
    case SET_LOCAL_AUTH_POSSIBLE:
      return {
        ...state,
        biometricAuthPossible: action.payload,
      };
    case SET_LOCAL_AUTH_ENROLLED:
      return {
        ...state,
        biometricAuthEnrolled: action.payload,
      };
    case SET_IS_PIN_SET:
      return {
        ...state,
        isPinSet: action.payload,
      };
    default:
      return state;
  }
}

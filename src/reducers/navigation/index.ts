import {
  NavigationAction,
  SET_ACTIVE_MODAL,
  SET_ACTIVE_SCREEN,
  UNSET_ACTIVE_MODAL,
} from '../../actions/navigation';
import { Navigation, NavigationModal } from '../../types/types/config/navigation';
import { AppXOR } from '../../types/types/utils';

interface InitialState {
  activeScreen: AppXOR<null, Navigation>;
  activeModal: AppXOR<null, NavigationModal>;
}

export const initialState: InitialState = {
  activeScreen: null,
  activeModal: null,
};

export function reducer(
  state: InitialState = initialState,
  action: NavigationAction,
): InitialState {
  switch (action.type) {
    case SET_ACTIVE_SCREEN: {
      return {
        ...state,
        activeScreen: action.payload as Navigation,
      };
    }
    case SET_ACTIVE_MODAL: {
      return {
        ...state,
        activeModal: action.payload,
      };
    }
    case UNSET_ACTIVE_MODAL: {
      return {
        ...state,
        activeModal: null,
      };
    }
    default:
      return state;
  }
}

import {
  SetConfigAction,
  SET_CONFIG_DATA,
  SET_CONFIG_ERROR,
  SET_CONFIG_LOADING,
} from '../../actions/config';
import { AppConfig } from '../../types/types/config/AppConfigs';
import { AppXOR } from '../../types/types/utils';

interface InitialState {
  data: AppXOR<null, AppConfig>;
  loading: boolean;
  error: null | unknown;
}

export const initialState: InitialState = {
  data: null,
  loading: false,
  error: null,
};

export function reducer(state: InitialState = initialState, action: SetConfigAction): InitialState {
  switch (action.type) {
    case SET_CONFIG_DATA:
      return {
        ...state,
        data: action.payload,
        loading: false,
      };
    case SET_CONFIG_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
      };
    case SET_CONFIG_LOADING:
      return {
        ...state,
        error: null,
        loading: true,
      };
    default:
      return state;
  }
}

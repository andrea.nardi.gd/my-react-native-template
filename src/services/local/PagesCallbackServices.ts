import { DefaultStackPages } from '../../config/navs/NavsConfig';
import { pagesCallbackConfigResolver } from '../../config/pages/pagesCallbackConfig';
import { pagesScrollCallbackConfigResolver } from '../../config/pages/pagesScrollCallbackConfig';
import { AppThunk } from '../../types/types/action/Action';
import {
  ExecuteCallBackActionProps,
  ExecuteScrollCallBackActionProps,
  PageBackButtonHandlerProps,
} from '../../types/types/pages/PagesCallback';

export default class PagesCallbackServices {
  static ExecutePageScrollCallback<T, P>({
    params,
    checkParams,
    callbackInfo,
    loaderStateSetter,
  }: ExecuteScrollCallBackActionProps<T, P>): AppThunk {
    return async (dispatch) => {
      const callback = pagesScrollCallbackConfigResolver[callbackInfo?.method]
        ? pagesScrollCallbackConfigResolver[callbackInfo?.method]
        : pagesScrollCallbackConfigResolver.DEFAULT;
      if (callback) {
        dispatch(callback({ checkParams, params, loaderStateSetter }));
      }
    };
  }

  static PageBackButtonHandler({
    navigation,
    navigationInfo,
    sequenceInfo,
    backButtonCallback,
    backHandler,
  }: PageBackButtonHandlerProps): boolean {
    if (navigation?.canGoBack()) {
      if (navigationInfo?.blockGoBack) {
        navigation?.pop();
        navigation?.navigate(DefaultStackPages.AUTHENTICATED_STACK as never);

        return true;
      }
      if (
        backButtonCallback &&
        (navigationInfo?.askConfirmationOnBack || sequenceInfo?.config.askConfirmationOnBack)
      ) {
        backButtonCallback();
        return true;
      } else {
        navigation?.goBack();
        return true;
      }
    } else {
      backHandler.exitApp();
      return true;
    }
  }

  static ExecutePagesCallback<T, P>({
    params,
    checkParams,
    callbackInfo,
    navigation,
  }: ExecuteCallBackActionProps<T, P>): AppThunk {
    return async (dispatch) => {
      const callback = pagesCallbackConfigResolver[callbackInfo?.method]
        ? pagesCallbackConfigResolver[callbackInfo?.method]
        : pagesCallbackConfigResolver.DEFAULT;
      if (callback) {
        setTimeout(
          () => dispatch(callback({ navigation, checkParams, params })),
          callbackInfo?.delay || 0,
        );
      }
    };
  }
}

import * as Font from 'expo-font';
import AppFontsDict from '../../config/assets/fonts';
import ImagesFilesDict, { ImagesFileNames } from '../../config/assets/images';
import { Asset } from 'expo-asset';
import Constants from 'expo-constants';

export default class AppServices {
  static async loadAppFonts(): Promise<void> {
    await Promise.resolve(
      await Font.loadAsync({
        ...AppFontsDict,
      }),
    );
  }

  static async cacheImages(): Promise<void> {
    const cacheImages = ImagesFileNames.map((image) => {
      return ImagesFilesDict[image] as string;
    });

    await Asset.loadAsync([...cacheImages]);
  }

  static IsRunningOnExpoGo(): boolean {
    return Constants.appOwnership === 'expo';
  }
}

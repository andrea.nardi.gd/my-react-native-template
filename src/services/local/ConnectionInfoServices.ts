import { AppThunk } from '../../types/types/action/Action';
import { NetInfoState } from '@react-native-community/netinfo';
import { setOnlineStatus } from '../../actions/connection-info';

export default class ConnectionInfoService {
  //TODO add connection info && flight mode (connection info for downloading cover)
  static setNetworkInformation(state: NetInfoState): AppThunk {
    return async (dispatch) => {
      dispatch(setOnlineStatus(state.isConnected as boolean));
    };
  }
}

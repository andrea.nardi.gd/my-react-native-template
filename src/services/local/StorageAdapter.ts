import { Platform } from 'react-native';
import {
  AppStorage,
  BaseAppStorage,
  OperationSafetyType,
} from '../../types/interfaces/services/Storage';
import AsyncStorageServices from './storage/AsyncStorageServices';
import SecureStorageService from './storage/SecureStorageServices';

export class StorageAdapter implements AppStorage {
  storage: BaseAppStorage;
  enclaveStorage: BaseAppStorage;

  constructor() {
    const platformType = Platform.OS;
    this.storage = new AsyncStorageServices();
    if (platformType === 'android' || platformType === 'ios') {
      this.enclaveStorage = new SecureStorageService();
    } else {
      this.enclaveStorage = new AsyncStorageServices();
    }
  }

  async get(key: string, safety: OperationSafetyType = 'unsafe'): Promise<string | null> {
    try {
      const storage = safety === 'unsafe' ? this.storage : this.enclaveStorage;
      const value = await storage.get(key);
      return value ? value : null;
    } catch (e) {
      return null;
    }
  }

  async set<T>(key: string, value: T, safety: OperationSafetyType = 'unsafe'): Promise<void> {
    const storage = safety === 'unsafe' ? this.storage : this.enclaveStorage;
    await storage.set(key, value);
  }

  async delete(key: string, safety: OperationSafetyType = 'unsafe'): Promise<void> {
    const storage = safety === 'unsafe' ? this.storage : this.enclaveStorage;
    await storage.delete(key);
  }

  async clear(safety: OperationSafetyType = 'unsafe'): Promise<void> {
    const storage = safety === 'unsafe' ? this.storage : this.enclaveStorage;
    await storage.clear();
  }

  async clearWithExceptions(
    preserveKeys?: string[],
    safety: OperationSafetyType = 'unsafe',
  ): Promise<void> {
    const storage = safety === 'unsafe' ? this.storage : this.enclaveStorage;
    await storage.clearWithExceptions(preserveKeys || []);
  }
}

import * as SecureStore from 'expo-secure-store';
import config from '../../../config';
import { boolKeys } from '../../../config/storage/secure';
import { BaseAppStorage } from '../../../types/interfaces/services/Storage';

export default class SecureStorageService implements BaseAppStorage {
  async set<T>(key: string, value: T): Promise<void> {
    try {
      await SecureStore.setItemAsync(key, JSON.stringify(value));
    } catch (e) {
      console.log('SECURE SET =>', e);
    }
  }

  async get(key: string): Promise<string | null> {
    try {
      const result = await SecureStore.getItemAsync(key);
      if (result) {
        if (boolKeys.includes(key)) {
          return result;
        }
        return JSON.parse(result);
      }

      return null;
    } catch (e) {
      console.error('SECURE GET ERROR=> ', e);
      return null;
    }
  }

  async delete(key: string): Promise<void> {
    try {
      //console.log('SECURE DELETE => ', key);

      return await SecureStore.deleteItemAsync(key);
    } catch (e) {
      console.log('SECURE DELETE => ', e);
    }
  }

  async checkSafety(): Promise<boolean> {
    try {
      return await SecureStore.isAvailableAsync();
    } catch (e) {
      console.log('SECURE CLEAR', e);
      return false;
    }
  }

  async clear(): Promise<void> {
    try {
      config.STORAGE_KEYS_LIST.forEach(async (key: string) => {
        await this.set(key, '');
        await this.delete(key);
      });
    } catch (e) {
      console.log('SECURE CLEAR', e);
    }
  }

  async clearWithExceptions(preserveKeys?: string[]): Promise<void> {
    try {
      config.STORAGE_KEYS_LIST.forEach(async (key: string) => {
        if (!preserveKeys?.includes(key)) {
          await this.set(key, '');
          await this.delete(key);
        }
      });
    } catch (e) {
      console.log('SECURE CLEAR WITH EXCEPTION', e);
    }
  }
}

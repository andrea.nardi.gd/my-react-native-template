import AsyncStorage from '@react-native-async-storage/async-storage';
import config from '../../../config';
import { BaseAppStorage } from '../../../types/interfaces/services/Storage';

export default class AsyncStorageServices implements BaseAppStorage {
  async set<T>(key: string, value: T): Promise<void> {
    try {
      //console.log('Websafe SET => ', key);
      await AsyncStorage.setItem(key, JSON.stringify(value));
    } catch (e) {
      console.log('Websafe SET => ', e);
    }
  }

  async get(key: string): Promise<string | null> {
    try {
      //console.log('Websafe GET => ', key);
      return await AsyncStorage.getItem(key);
    } catch (e) {
      console.log('Websafe GET => ', e);
      return null;
    }
  }

  async delete(key: string): Promise<void> {
    try {
      //console.log('Websafe DELETE => ', key);
      return await AsyncStorage.removeItem(key);
    } catch (e) {
      console.log('Websafe DELETE => ', e);
    }
  }

  async clear(): Promise<void> {
    try {
      //console.log('Websafe CLEAR');
      await AsyncStorage.clear();
    } catch (e) {
      console.log('Websafe CLEAR', e);
    }
  }

  async clearWithExceptions(preserveKeys?: string[]): Promise<void> {
    try {
      config.STORAGE_KEYS_LIST.forEach(async (key: string) => {
        if (!preserveKeys?.includes(key)) {
          //await this.set(key, '');
          await this.delete(key);
        }
      });
    } catch (e) {
      console.log('WEBSAFE CLEAR WITH EXCEPTION', e);
    }
  }
}

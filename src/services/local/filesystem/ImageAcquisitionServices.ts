import * as ImagePicker from 'expo-image-picker';
import config from '../../../config';
import { AppPermissionError } from '../../../types/types/errors';
import { AppXOR } from '../../../types/types/utils';

export default class ImageAcquisitionServices {
  mediaPermissionsDenied: boolean;

  constructor() {
    this.mediaPermissionsDenied = false;
  }

  private applyCompression(compression = 10): number {
    const useCompression = config.MEDIA_SETTINGS.COMPRESSION.IMAGE.MEDIA_LIBRARY.USE_COMPRESSION;

    if (useCompression) {
      return compression / 10;
    }
    return 1;
  }

  private async _fetchMediaLibraryPermission() {
    const permission = await ImagePicker.getMediaLibraryPermissionsAsync();

    if (!permission.granted && !permission.canAskAgain) {
      this.mediaPermissionsDenied = true;
    }

    return permission;
  }

  private async _askMediaLibraryPermission() {
    const permission = await ImagePicker.requestMediaLibraryPermissionsAsync();

    if (!permission.granted && !permission.canAskAgain) {
      this.mediaPermissionsDenied = true;
    }

    return permission;
  }

  async fetchAllPermissions(): Promise<boolean> {
    const _askMediaLibraryPermissionResolver = await this._fetchMediaLibraryPermission();
    return this.mediaPermissionsDenied;
  }

  async getFromLibrary(
    compression?: number,
  ): Promise<AppXOR<ImagePicker.ImagePickerResult, AppPermissionError>> {
    const libPermission = await this._askMediaLibraryPermission();
    if (libPermission.granted) {
      return await ImagePicker.launchImageLibraryAsync({
        quality: this.applyCompression(compression),
        mediaTypes: ImagePicker.MediaTypeOptions.Images,
      });
    } else {
      return { denied: true, permanently: !libPermission.canAskAgain };
    }
  }
}

import { ParsedURL } from 'expo-linking';
import { deepLinkingActionResolver } from '../../config/navs/deepLinkingConfig';
import { AppThunk } from '../../types/types/action/Action';
import { ExecuteDeepLinkActionProps, LINKING_PATHS } from '../../types/types/linking/DeepLinking';

export default class DeepLinkingServices {
  private static extractScheme(url: ParsedURL): LINKING_PATHS {
    const urlParams = url?.scheme || LINKING_PATHS.DEFAULT;

    return urlParams as LINKING_PATHS;
  }

  static ExecuteDeepLinkAction<T>({
    url,
    checkParams,
    navigation,
  }: ExecuteDeepLinkActionProps<T>): AppThunk {
    return async (dispatch) => {
      const scheme = this.extractScheme(url);
      const callback = deepLinkingActionResolver[scheme]
        ? deepLinkingActionResolver[scheme]
        : deepLinkingActionResolver.DEFAULT;
      if (callback) {
        dispatch(
          callback({ navigation: navigation, checkParams: checkParams, params: url?.queryParams }),
        );
      }
    };
  }
}

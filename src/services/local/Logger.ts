import { isArray } from 'lodash';
import { logger } from 'react-native-logs';
import config from '../../config';
import localLoggerConfig from '../../config/logger';
import { loggerPrefixesDelimiters } from '../../config/logger';
import { LogLevels } from '../../config/logger/loggerMap';

const loggerInstance = logger.createLogger(localLoggerConfig);
const loggerEnabled = config.LOGS_SETTINGS.ENABLE_LOGS;
const logLevel = __DEV__ ? -1 : LogLevels[config.LOGS_SETTINGS.LOG_LEVEL as keyof typeof LogLevels];

interface LoggerProps {
  args: unknown[] | unknown;
  prefixes?: string | string[];
  shouldSaveLogLocally?: boolean;
}

interface EmitLogProps {
  args: unknown[];
  logType: 'info' | 'warning' | 'error' | 'debug';
  shouldSaveLogLocally?: boolean;
}

export default class Logger {
  private static prefixesBuilder(prefixes: string[] | string): unknown[] {
    const appPrefix = loggerPrefixesDelimiters.square;
    if (isArray(prefixes)) {
      return prefixes.map((prefix) => `${appPrefix[0]}${prefix}${appPrefix[1]}`);
    }
    return [`${appPrefix[0]}${prefixes}${appPrefix[1]}`];
  }

  private static argParser(args: unknown[] | unknown, prefixes?: string[] | string): unknown[] {
    const argsPrefixes = prefixes ? this.prefixesBuilder(prefixes) : [];
    if (isArray(args)) {
      return [...argsPrefixes, ...args];
    }
    return [...argsPrefixes, args];
  }

  private static async EmitLog({ args, logType }: EmitLogProps) {
    loggerInstance[logType](...args);
  }

  static async LogInfo({ args, prefixes }: LoggerProps): Promise<void> {
    const level = LogLevels.info;
    if (loggerEnabled && level >= logLevel) {
      const logArgs = this.argParser(args, prefixes);
      this.EmitLog({
        logType: 'info',
        args: [...logArgs],
      });
    }
  }
  static async LogDebug({ args, prefixes }: LoggerProps): Promise<void> {
    const level = LogLevels.debug;
    if (loggerEnabled && level >= logLevel) {
      const logArgs = this.argParser(args, prefixes);
      this.EmitLog({
        logType: 'debug',
        args: [...logArgs],
      });
    }
  }
  static async LogWarning({ args, prefixes }: LoggerProps): Promise<void> {
    const level = LogLevels.warning;
    if (loggerEnabled && level >= logLevel) {
      const logArgs = this.argParser(args, prefixes);
      this.EmitLog({
        logType: 'warning',
        args: [...logArgs],
      });
    }
  }
  static async LogError({ args, prefixes }: LoggerProps): Promise<void> {
    const level = LogLevels.error;
    if (loggerEnabled && level >= logLevel) {
      const logArgs = this.argParser(args, prefixes);
      this.EmitLog({
        logType: 'error',
        args: [...logArgs],
      });
    }
  }
}

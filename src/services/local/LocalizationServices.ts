import { setAppLanguage } from '../../actions/localization';
import { AppThunk } from '../../types/types/action/Action';
import { LocalizationLang } from '../../types/types/localization';

export default class LocalizationServices {
  static ChangeLanguage(language: LocalizationLang): AppThunk {
    return async (dispatch) => {
      dispatch(setAppLanguage(language));
    };
  }
}

import { setActiveSnackBar, unsetActiveSnackBar } from '../../actions/message-context';
import { EmitHandledMessage, EmitUnhandledMessage } from '../../types/interfaces/services/Message';
import { AppThunk } from '../../types/types/action/Action';
import { MessageTextType, MessageTypes, SnackBarMessage } from '../../types/types/messages';
import axios, { AxiosError } from 'axios';
import config from '../../config';
import { AggregateMessagesKeys } from '../../config/messages';
import { TranslationKeys } from '../../types/types/localization';
import { ErrorMessagesKeys } from '../../config/messages/error';
import { SuccessMessagesKeys } from '../../config/messages/success';
import { WarningMessagesKeys } from '../../config/messages/warning';
import { InfoMessagesKeys } from '../../config/messages/info';
import { QuitMessageKeys } from '../../config/messages/quit';
import Logger from './Logger';

export default class MessageServices {
  static showSnackBarMessage(message: SnackBarMessage): AppThunk {
    //TODO make private
    return async (dispatch) => {
      dispatch(setActiveSnackBar(message));
    };
  }

  private static errorResponseMessageExtractor(
    e: AxiosError,
    overrideErrorKey?: string,
  ): MessageTextType {
    const errorKey = overrideErrorKey || config.NETWORK_SETTINGS.ERROR_RESPONSE_FIELD_NAME;

    if (e.response?.data && e.response.data[errorKey]) {
      return e.response.data[errorKey];
    }

    return 'error';
  }

  static extractKeyFromError(e: unknown, overrideErrorKey?: string): MessageTextType {
    try {
      let errorMessage = 'error';
      if (e instanceof Error) errorMessage = e?.message || 'error';
      if (axios.isAxiosError(e)) {
        errorMessage = this.errorResponseMessageExtractor(e, overrideErrorKey);
      }
      return errorMessage;
    } catch (ex) {
      return 'error';
    }
  }

  private static async logMessage(message: string, messageType: MessageTypes): Promise<void> {
    const logPayload = { args: message };
    switch (messageType) {
      default:
      case 'success':
        Logger.LogDebug(logPayload);
        break;
      case 'info':
        Logger.LogInfo(logPayload);
        break;
      case 'warning':
        Logger.LogWarning(logPayload);
        break;
      case 'error':
        Logger.LogError(logPayload);
        break;
    }
  }

  private static getDefaultKeyFromType(type: MessageTypes): AggregateMessagesKeys {
    switch (type) {
      default:
      case 'error':
        return ErrorMessagesKeys.DEFAULT;
      case 'success':
        return SuccessMessagesKeys.DEFAULT;
      case 'warning':
        return WarningMessagesKeys.DEFAULT;
      case 'info':
        return InfoMessagesKeys.DEFAULT;
      case 'quit':
        return QuitMessageKeys.DEFAULT;
    }
  }

  private static retrieveValidMessage(
    message: MessageTextType,
    type: MessageTypes,
    allowUnsupportedTranslation = false,
  ): SnackBarMessage {
    try {
      const partialMessageDictionary = config.MESSAGES_DICTIONARY[type];
      if (partialMessageDictionary) {
        if (partialMessageDictionary[message as AggregateMessagesKeys]) {
          return partialMessageDictionary[message as AggregateMessagesKeys] as SnackBarMessage;
        } else {
          if (
            config.DEFAULT_LOCALIZATION &&
            config.DEFAULT_LOCALIZATION[message as TranslationKeys]
          ) {
            return {
              message: config.DEFAULT_LOCALIZATION[message as TranslationKeys] as string,
              type,
            };
          } else {
            if (allowUnsupportedTranslation) {
              return { message, type };
            } else {
              return partialMessageDictionary[this.getDefaultKeyFromType(type)] as SnackBarMessage;
            }
          }
        }
      } else {
        throw Error('No notification dictionary defined, should use direct key');
      }
    } catch (e) {
      return { message: message, type };
    }
  }

  private static addOptionalTranslationParameters(
    snackBarMessage: SnackBarMessage,
    params: Record<string, string> | undefined,
  ): SnackBarMessage {
    if (params) {
      snackBarMessage.optionalMessageParameters = { ...params };
    }

    return snackBarMessage;
  }

  private static overrideMessageOptions(
    snackBarMessage: SnackBarMessage,
    snackBarMessageOptions: Partial<SnackBarMessage> | undefined,
  ): SnackBarMessage {
    if (snackBarMessageOptions) {
      const overriddenMessage = { ...snackBarMessage, ...snackBarMessageOptions };
      return overriddenMessage;
    }
    return snackBarMessage;
  }

  private static buildSnackbarMessage({
    message,
    type = 'error',
    optionalTranslationParams,
    overrideMessageOptions,
    allowUnsupportedTranslation = false,
  }: EmitHandledMessage): SnackBarMessage {
    const messageContent = this.retrieveValidMessage(message, type, allowUnsupportedTranslation);
    const messageWithParameters = this.addOptionalTranslationParameters(
      messageContent,
      optionalTranslationParams,
    );
    const finalMessage = this.overrideMessageOptions(messageWithParameters, overrideMessageOptions);
    return finalMessage;
  }

  static hideSnackBarMessage(): AppThunk {
    return async (dispatch) => {
      dispatch(unsetActiveSnackBar());
    };
  }

  static emitMessage({
    message,
    optionalTranslationParams,
    type = 'error',
    overrideMessageOptions,
    allowUnsupportedTranslation = false,
  }: EmitHandledMessage): AppThunk {
    return async (dispatch) => {
      const testMessage = this.buildSnackbarMessage({
        message,
        optionalTranslationParams,
        type,
        overrideMessageOptions,
        allowUnsupportedTranslation,
      });
      dispatch(this.showSnackBarMessage(testMessage));
    };
  }

  static emitErrorMessage({
    e,
    overrideMessage,
    optionalTranslationParams,
    overrideKeyErrorExtractor,
    fallBackMessage,
    overrideMessageOptions,
    allowUnsupportedTranslation = true,
    showUser = false,
  }: EmitUnhandledMessage): AppThunk {
    return async (dispatch) => {
      let errorKey = overrideMessage || this.extractKeyFromError(e, overrideKeyErrorExtractor);

      if (errorKey === 'error' && fallBackMessage && fallBackMessage !== 'error') {
        errorKey = fallBackMessage;
      }

      if (showUser) {
        dispatch(
          this.emitMessage({
            message: errorKey,
            type: 'error',
            optionalTranslationParams,
            overrideMessageOptions,
            allowUnsupportedTranslation,
          }),
        );
      }
      this.logMessage(errorKey, 'error');
    };
  }
}

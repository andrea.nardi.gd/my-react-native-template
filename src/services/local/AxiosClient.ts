import axios, { AxiosInstance, AxiosPromise, AxiosRequestConfig } from 'axios';
import { AppBaseRequest, AppMethods, AppRequest } from '../../types/interfaces/api/request';
import { StorageAdapter } from './StorageAdapter';
import config from '../../config';

export class AxiosClientServices {
  static buildAxiosClient(config: AxiosRequestConfig): AxiosInstance {
    const client = axios.create(config);

    client.interceptors.response.use(
      function (response) {
        if (response.data.status && response.data.status === 'error') {
          return Promise.reject(response.data);
        }
        return response;
      },
      function (error) {
        return Promise.reject(error);
      },
    );

    return client;
  }
}

class AxiosClient {
  static axiosClient: AxiosInstance;
  static instance: AxiosClient;
  static storage: StorageAdapter;

  constructor() {
    if (!AxiosClient.instance) {
      AxiosClient.axiosClient = AxiosClientServices.buildAxiosClient({
        baseURL: config.NETWORK_SETTINGS.AXIOS_BASE_URL,
      });

      AxiosClient.storage = new StorageAdapter();
      AxiosClient.instance = this;
    }

    return AxiosClient.instance;
  }

  injectAxiosClient(axiosClient: AxiosInstance) {
    AxiosClient.axiosClient = axiosClient;
  }

  private buildAxiosRequest = async <DataT>(
    request: AppBaseRequest,
    data: DataT | DataT[] | null,
  ): Promise<AppRequest<DataT>> => {
    const requestInstance: AppRequest<DataT> = {
      method: request.method,
      url: request.url,
    };

    if (data) {
      const method = request.method;
      if (method === AppMethods.GET || method === AppMethods.DELETE) {
        requestInstance.params = data;
      } else if (method === AppMethods.POST || method === AppMethods.PUT) {
        requestInstance.data = data;
      }
    }
    return requestInstance;
  };

  sendAxiosRequest = async <DataT>(
    request: AppBaseRequest,
    data?: DataT | DataT[] | null,
  ): Promise<AxiosPromise | void> => {
    const requestInstance = (await this.buildAxiosRequest(request, data)) as AxiosRequestConfig;
    return AxiosClient.axiosClient({ ...requestInstance });
  };
}

const AxiosClientInstance = new AxiosClient();

export default AxiosClientInstance;

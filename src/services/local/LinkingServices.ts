import * as Linking from 'expo-linking';

const prefixes = {
  mail: 'mailto: ',
  phone: 'tel:',
};

type LinkingServicesMethodProps = {
  link: string;
  thenableCallback?: () => void;
  catchableCallBack?: (e?: unknown) => void;
};

export default class LinkingServices {
  private static openLink(
    link: LinkingServicesMethodProps['link'],
    thenableCallback?: LinkingServicesMethodProps['thenableCallback'],
    catchableCallBack?: LinkingServicesMethodProps['catchableCallBack'],
  ) {
    Linking.openURL(link)
      .then((response) => {
        if (response && thenableCallback) {
          thenableCallback();
        }
      })
      .catch((e) => {
        if (catchableCallBack) {
          catchableCallBack(e);
        }
      });
  }

  static openPhoneAppLink(
    phone: LinkingServicesMethodProps['link'],
    thenableCallback?: LinkingServicesMethodProps['thenableCallback'],
    catchableCallBack?: LinkingServicesMethodProps['catchableCallBack'],
  ): void {
    this.openLink(`${prefixes.phone}${phone}`, thenableCallback, catchableCallBack);
  }

  static openMailAppLink(
    address: LinkingServicesMethodProps['link'],
    thenableCallback?: LinkingServicesMethodProps['thenableCallback'],
    catchableCallBack?: LinkingServicesMethodProps['catchableCallBack'],
  ): void {
    this.openLink(`${prefixes.mail}${address}`, thenableCallback, catchableCallBack);
  }

  static openBrowserApp(
    url: LinkingServicesMethodProps['link'],
    thenableCallback?: LinkingServicesMethodProps['thenableCallback'],
    catchableCallBack?: LinkingServicesMethodProps['catchableCallBack'],
  ): void {
    this.openLink(url, thenableCallback, catchableCallBack);
  }

  static openSettings(): void {
    Linking.openSettings();
  }
}

import ImageAcquisitionServices from './filesystem/ImageAcquisitionServices';
import * as ImagePicker from 'expo-image-picker';

type PermissionDeniedStructure = {
  imagePickerDenied: boolean;
};

export default class ImagePickerAdapter {
  imageLibraryInstance: ImageAcquisitionServices;

  constructor() {
    this.imageLibraryInstance = new ImageAcquisitionServices();
  }

  async getAllDeniedPermissions(): Promise<PermissionDeniedStructure> {
    const _promiseResolver = await this.imageLibraryInstance.fetchAllPermissions();
    return {
      imagePickerDenied: this.imageLibraryInstance.mediaPermissionsDenied,
    };
  }

  async getImageFromLibrary(
    compression?: number,
  ): Promise<null | ImagePicker.ImagePickerResult | ImagePicker.ImageInfo> {
    if (!this.imageLibraryInstance.mediaPermissionsDenied) {
      const mediaResponse = await this.imageLibraryInstance.getFromLibrary(compression);

      if (!mediaResponse.denied) {
        console.log(mediaResponse);
        return mediaResponse as ImagePicker.ImagePickerResult;
      } else {
        console.log('Image media permission denied. Permanently? => ' + mediaResponse.permanently);
        return null;
      }
    } else {
      console.log('Image media permission denied permanently. Must be regiven from settings');
      return null;
    }
  }
}

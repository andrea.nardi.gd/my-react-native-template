import { setDeviceTypeAndOs, setIsRooted } from '../../actions/device-info';
import { AppThunk } from '../../types/types/action/Action';
import * as Device from 'expo-device';
import { AppDeviceOS, AppDeviceType } from '../../types/types/device';

export default class DeviceInfoServices {
  static getDeviceInfo(): AppThunk {
    return async (dispatch) => {
      const deviceType = Device.DeviceType[await Device.getDeviceTypeAsync()] as AppDeviceType;
      const deviceOs = Device.osName as AppDeviceOS;
      dispatch(setDeviceTypeAndOs({ type: deviceType, os: deviceOs }));
      try {
        const isRooted = await Device.isRootedExperimentalAsync();
        dispatch(setIsRooted(isRooted));
      } catch {
        dispatch(setIsRooted(false));
      }
    };
  }

  static async getDeviceType(): Promise<AppDeviceType> {
    return Device.DeviceType[await Device.getDeviceTypeAsync()] as AppDeviceType;
  }
}

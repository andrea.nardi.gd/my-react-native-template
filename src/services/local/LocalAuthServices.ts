import * as LocalAuthentication from 'expo-local-authentication';
import {
  setIsPinSet,
  setIsUserAuthorized,
  setLocalAuthEnabled,
  setLocalAuthEnrolled,
  setLocalAuthPossibile,
} from '../../actions/local-auth';
import { AppThunk } from '../../types/types/action/Action';
import { StorageAdapter } from './StorageAdapter';
import i18next from 'i18next';
import config from '../../config';

export default class LocalAuthServices {
  private static async canBiometricAuth(): Promise<boolean> {
    try {
      return await LocalAuthentication.hasHardwareAsync();
    } catch (e) {
      console.log(e);
      return false;
    }
  }

  private static async isBiometricAuthEnrolled(): Promise<boolean> {
    try {
      return (await this.canBiometricAuth()) && (await LocalAuthentication.isEnrolledAsync());
    } catch (e) {
      console.log(e);
      return false;
    }
  }

  static checkPasswordEquality(password: string | null, confirmPassword: string | null): boolean {
    return password === confirmPassword;
  }

  static async unlockWithBiometrics(): Promise<boolean> {
    try {
      if (await this.isBiometricAuthEnrolled()) {
        return (
          await LocalAuthentication.authenticateAsync({
            fallbackLabel: 'Use device pin for authentication',
            cancelLabel: i18next.t('cancel'),
            disableDeviceFallback: true,
            promptMessage: i18next.t('biometricAuthDescription'),
          })
        ).success;
      } else {
        return false;
      }
    } catch (e) {
      console.log(e);
      return false;
    }
  }

  static setLocalAuthPossibility(): AppThunk {
    return async (dispatch) => {
      const isPossible = await this.canBiometricAuth();
      dispatch(setLocalAuthPossibile(isPossible));
    };
  }

  static setLocalAuthEnrolled(): AppThunk {
    return async (dispatch) => {
      const isEnrolled = await this.isBiometricAuthEnrolled();
      console.log('isLocalAuthEnrolled? => ', isEnrolled);
      dispatch(setLocalAuthEnrolled(isEnrolled));
    };
  }

  static setLocalAuthEnabled(status: boolean): AppThunk {
    return async (dispatch) => {
      //const storage = new StorageAdapter();
      dispatch(setLocalAuthEnabled(status));
      //storage.set(config.STORAGE_KEYS.biometricAuthEnabled, status);
    };
  }

  static setIsPinEnabled(): AppThunk {
    return async (dispatch) => {
      const storage = new StorageAdapter();
      const pin = await storage.get(config.STORAGE_KEYS.pin);

      if (pin) {
        dispatch(setIsPinSet(true));
      }
    };
  }

  static setAuthorizationStatus(status: boolean): AppThunk {
    return async (dispatch) => {
      //console.log('Called set auth status function');
      dispatch(setIsUserAuthorized(status));
    };
  }
}

import {
  ComplexPermissionValues,
  FunctionPermissionCheck,
  FunctionPermissionCheckSync,
  NumberConditions,
  PermissionConditionsTypes,
  PermissionConditionValues,
} from '../../types/types/config/FlowAlteringConditions';

type Condition<K, V, T> = {
  name: K;
  condition: V;
  type: T;
};

const numberPermissionCheck = (
  currentValue: number,
  targetValue: number,
  numberCondition: NumberConditions,
): boolean => {
  const value = Number(currentValue);
  const target = Number(targetValue);

  switch (numberCondition) {
    default:
    case 'equal':
      return value === target;
    case 'greater':
      return value > target;
    case 'lower':
      return value < target;
  }
};

export default class PermissionsServices {
  private static async checkConditionOnTypeAsync(
    currentValue: PermissionConditionValues,
    requestedValue: PermissionConditionValues,
    type: PermissionConditionsTypes = 'boolean',
    overrideCheckFunction?: FunctionPermissionCheck,
  ): Promise<boolean> {
    switch (type) {
      case 'boolean':
      case 'string':
      default:
        return currentValue === requestedValue;
      case 'number':
        const recastCondition = requestedValue as ComplexPermissionValues;
        const numberCondition = recastCondition.condition;
        return numberPermissionCheck(
          currentValue as number,
          requestedValue as number,
          numberCondition as NumberConditions,
        );
      case 'function':
        return overrideCheckFunction
          ? await overrideCheckFunction(currentValue as never, requestedValue as never)
          : false;
    }
  }

  static async canAsync<K extends string | number | symbol>({
    requisites,
    currentValues,
    overrideCheckFunction,
  }: {
    currentValues: Partial<Record<K, PermissionConditionValues>>;
    requisites: Condition<K, PermissionConditionValues, PermissionConditionsTypes>[];
    overrideCheckFunction?: FunctionPermissionCheck;
  }): Promise<boolean> {
    if (requisites.length === 0 || !requisites) {
      return true;
    }
    let can = true;
    let i = 0;
    const requisitesLength = requisites.length;
    while (can && i < requisitesLength) {
      const currentCondition = requisites[i];
      const currentValue = currentValues[currentCondition.name];
      const currentRequisite = currentCondition.condition;
      const currentType = currentCondition.type;
      can = await this.checkConditionOnTypeAsync(
        currentValue as PermissionConditionValues,
        currentRequisite,
        currentType,
        overrideCheckFunction,
      );
      i++;
    }
    return can;
  }

  private static checkConditionOnTypeSync(
    currentValue: PermissionConditionValues,
    requestedValue: PermissionConditionValues,
    type: PermissionConditionsTypes = 'boolean',
    overrideCheckFunction?: FunctionPermissionCheckSync,
  ): boolean {
    switch (type) {
      case 'boolean':
      case 'string':
      default:
        return currentValue === requestedValue;
      case 'number':
        const recastCondition = requestedValue as ComplexPermissionValues;
        const numberCondition = recastCondition.condition;
        return numberPermissionCheck(
          currentValue as number,
          requestedValue as number,
          numberCondition as NumberConditions,
        );
      case 'function':
        return overrideCheckFunction
          ? overrideCheckFunction(currentValue as never, requestedValue as never)
          : false;
    }
  }

  static canSync<K extends string | number | symbol>({
    requisites,
    currentValues,
    overrideCheckFunction,
  }: {
    currentValues: Partial<Record<K, PermissionConditionValues>>;
    requisites: Condition<K, PermissionConditionValues, PermissionConditionsTypes>[];
    overrideCheckFunction?: FunctionPermissionCheckSync;
  }): boolean {
    if (requisites.length === 0 || !requisites) {
      return true;
    }
    let can = true;
    let i = 0;
    const requisitesLength = requisites.length;

    while (can && i < requisitesLength) {
      const currentCondition = requisites[i];
      const currentValue = currentValues[currentCondition.name];
      const currentRequisite = currentCondition.condition;
      const currentType = currentCondition.type;
      can = this.checkConditionOnTypeSync(
        currentValue as PermissionConditionValues,
        currentRequisite,
        currentType,
        overrideCheckFunction,
      );
      i++;
    }
    return can;
  }
}

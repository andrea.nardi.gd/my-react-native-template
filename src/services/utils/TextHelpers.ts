import { AppFontsDictionary } from '../../config/assets/fonts';
import { SizeVariants } from '../../types/types/styles/sizes';

interface GenerateFontFamily {
  fontFamily?: keyof typeof AppFontsDictionary;
  fontWeight?: keyof typeof AppFontsDictionary[keyof typeof AppFontsDictionary];
  fontStyle?: keyof typeof AppFontsDictionary[keyof typeof AppFontsDictionary];
}

export default class StyleHelpers {
  static generateCompatibleLineHeight = (
    fontSize: number,
    spacing:
      | 'required'
      | SizeVariants.SMALL
      | SizeVariants.MEDIUM
      | SizeVariants.LARGE = 'required',
  ): number => {
    switch (spacing) {
      case 'required':
      default:
        return fontSize + 4;
      case SizeVariants.SMALL:
        return fontSize + 6;
      case SizeVariants.MEDIUM:
        return fontSize + 8;
      case SizeVariants.LARGE:
        return fontSize + 12;
    }
  };

  static generateFontFamily = ({
    fontFamily,
    fontWeight,
    fontStyle,
  }: GenerateFontFamily): string => {
    if (fontFamily && AppFontsDictionary[fontFamily]) {
      if (fontStyle && AppFontsDictionary[fontFamily][fontStyle]) {
        if (fontWeight && AppFontsDictionary[fontFamily][fontStyle][fontWeight]) {
          return AppFontsDictionary[fontFamily][fontStyle][fontWeight];
        } else {
          return AppFontsDictionary[fontFamily][fontStyle][400];
        }
      } else {
        //@ts-ignore
        if (fontWeight && AppFontsDictionary[fontFamily].normal[fontWeight]) {
          //@ts-ignore
          return AppFontsDictionary[fontFamily].normal[fontWeight];
        } else {
          //@ts-ignore
          return AppFontsDictionary[fontFamily].normal[400];
        }
      }
    } else {
      if (fontStyle && AppFontsDictionary[AppFontsDictionary.default][fontStyle]) {
        if (fontWeight && AppFontsDictionary[AppFontsDictionary.default][fontStyle][fontWeight]) {
          return AppFontsDictionary[AppFontsDictionary.default][fontStyle][fontWeight];
        } else {
          return AppFontsDictionary[AppFontsDictionary.default][fontStyle][400];
        }
      } else {
        if (fontWeight && AppFontsDictionary[AppFontsDictionary.default].normal[fontWeight]) {
          return AppFontsDictionary[AppFontsDictionary.default].normal[fontWeight];
        } else {
          return AppFontsDictionary[AppFontsDictionary.default].normal[400];
        }
      }
    }
  };
}

import { NativeStackNavigationProp } from '@react-navigation/native-stack';
import { PAGES_LIST } from '../../config/navs/NavsConfig';
import { NavigatorParamsList } from '../../types/types/config/navigation';

export default class DeferredNavigation {
  //TODO implement logic for this like deep linking one
  static NavigateWithConditions(
    destination: PAGES_LIST,
    navigation: NativeStackNavigationProp<NavigatorParamsList>,
  ): void {
    navigation?.navigate(destination as never);
  }
}

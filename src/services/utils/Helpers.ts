import validator from 'validator';
export default class Helper {
  static isEmailValid(email: string): boolean {
    return validator.isEmail(email.trim());
  }

  private static escapeRegExp(string: string): string {
    return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
  }

  private static replaceAll(str: string, find: string, replace: string): string {
    return str.replace(new RegExp(this.escapeRegExp(find), 'g'), replace);
  }

  static stringReplace(string: string | null, pattern: string, replace: string): string {
    if (string) {
      return this.replaceAll(string, pattern, replace);
    }
    return '';
  }

  static stringCleaner(string: string): string {
    if (string) {
      return string.replace(/\"/g, '');
    }
    return '';
  }
}

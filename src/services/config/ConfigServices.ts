import { setAppInitialized, setInizialitazionFailed } from '../../actions/app-status';
import { setAppConfig, setConfigError, setConfigLoading } from '../../actions/config';
import config from '../../config';
import { AppThunk } from '../../types/types/action/Action';
import { StorageAdapter } from '../local/StorageAdapter';
import LocalAuthServices from '../local/LocalAuthServices';
import { AppConfig } from '../../types/types/config/AppConfigs';
import Logger from '../local/Logger';
import MessageServices from '../local/MessageServices';

const _storage = new StorageAdapter();

export default class ConfigServices {
  private static retrieveFallBackConfig = (): AppConfig => {
    const fallBackConfig = config.CONFIG_INFO.FALLBACK_CONFIG;
    return fallBackConfig;
  };

  private static retrieveUserConfig = async (): Promise<AppConfig | null> => {
    //TODO implement method to retrieve config
    const configData = {} as AppConfig;
    return configData;
  };

  static saveUserConfig = async (): Promise<boolean> => {
    //TODO implement method to save config
    return true;
  };

  static setAppConfig(appConfig: AppConfig | null = null): AppThunk {
    return async (dispatch) => {
      if (appConfig) {
        dispatch(setAppConfig(appConfig));
      } else {
        dispatch(setAppConfig(this.retrieveFallBackConfig()));
      }
    };
  }

  static setupConfig(): AppThunk {
    return async (dispatch) => {
      try {
        dispatch(setConfigLoading());
        const appConfig = await this.retrieveUserConfig();
        dispatch(this.setAppConfig(appConfig));

        if (config.SECURITY_SETTINGS.ENABLE_USER_AUTHENTICATION) {
          dispatch(LocalAuthServices.setIsPinEnabled());
        }
        dispatch(setAppInitialized());
      } catch (e) {
        Logger.LogError({ args: e });
        dispatch(setConfigError(MessageServices.extractKeyFromError(e)));
        dispatch(setInizialitazionFailed());
      }
    };
  }
}

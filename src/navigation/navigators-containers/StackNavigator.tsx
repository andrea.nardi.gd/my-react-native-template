import React, { useMemo } from 'react';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { authenticatedNavigationMap, middlewareScreensMap } from '../../config/navs';
import { PAGES_DICTIONARIES } from '../../types/types/navigation/app-page';
import FlowAlteringConditions from '../../utils/selectors/FlowAlteringConditions';
import { countMiddlewareScreens } from '../builders/BottomTabNavigatorBuilder';
import {
  NavigationModalStackBuilder,
  NavigationScreensSequenceBuilder,
  NavigationScreensStackBuilder,
  NavigatorStackWrapper,
} from '../builders/StackNavigatorBuilders';

const StackNavigator = (): JSX.Element => {
  const navigatorConditionStatus = FlowAlteringConditions();
  const insets = useSafeAreaInsets();

  const MiddlewareScreens = useMemo(() => {
    const standardScreens = NavigationScreensStackBuilder(
      middlewareScreensMap.standard,
      insets,
      PAGES_DICTIONARIES.MIDDLEWARE_PAGES,
      true,
      navigatorConditionStatus,
    );

    const modalScreens = NavigationModalStackBuilder(middlewareScreensMap.modals, insets);

    return [...standardScreens, ...modalScreens];
  }, [insets, navigatorConditionStatus]);

  const AuthScreens = useMemo(() => {
    const standardScreens = NavigationScreensStackBuilder(
      authenticatedNavigationMap.standard,
      insets,
      PAGES_DICTIONARIES.AUTHORIZED_PAGES,
    );

    const sequencesScreens = NavigationScreensSequenceBuilder(
      authenticatedNavigationMap.sequences,
      insets,
      PAGES_DICTIONARIES.SEQUENCE_PAGES,
    );

    const modalScreens = NavigationModalStackBuilder(authenticatedNavigationMap.modals, insets);
    return [...standardScreens, ...sequencesScreens, ...modalScreens];
  }, [insets]);

  const middlewareScreensStackVisible = useMemo(() => {
    return countMiddlewareScreens(middlewareScreensMap.standard, true, navigatorConditionStatus);
  }, [navigatorConditionStatus]);

  const authStackVisible = useMemo(() => {
    return navigatorConditionStatus.AUTHORIZED;
  }, [navigatorConditionStatus.AUTHORIZED]);

  return (
    <NavigatorStackWrapper>
      {middlewareScreensStackVisible && MiddlewareScreens}
      {authStackVisible && AuthScreens}
    </NavigatorStackWrapper>
  );
};

export default StackNavigator;

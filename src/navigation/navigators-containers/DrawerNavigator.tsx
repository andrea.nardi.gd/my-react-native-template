import { createDrawerNavigator } from '@react-navigation/drawer';
import React from 'react';
import { useAppSelector } from '../../store/hooks';

import NavigationDrawer from '../custom-navigation-components/NavigationDrawer';
import BottomTabBarNavigator from './BottomTabBarNavigator';

const Drawer = createDrawerNavigator();

const DrawerNavigator = (): JSX.Element => {
  const activeScreen = useAppSelector((store) => store.navigationContext.activeScreen);
  return (
    <Drawer.Navigator detachInactiveScreens={true} drawerContent={NavigationDrawer}>
      <Drawer.Screen
        name={'Drawer-Screens'}
        component={BottomTabBarNavigator}
        options={{
          swipeEnabled: activeScreen?.drawerNavigator ? true : false,
          headerShown: false,
        }}
      />
    </Drawer.Navigator>
  );
};

export default DrawerNavigator;

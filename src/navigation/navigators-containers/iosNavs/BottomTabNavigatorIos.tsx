import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { BottomTabNavigationHelpers } from '@react-navigation/bottom-tabs/lib/typescript/src/types';
import React, { useMemo } from 'react';
import AppPlayerBar from '../../../components/layout/AppTabBar';
import { middlewareScreensMap } from '../../../config/navs';
import FlowAlteringConditions from '../../../utils/selectors/FlowAlteringConditions';
import { countMiddlewareScreens } from '../../builders/BottomTabNavigatorBuilder';
import IOSAuthStackBuilder from './IOSAuthStack';
import IOSMiddlewareStackBuilder from './IOSMiddlewareStack';

const BottomTab = createBottomTabNavigator();

const BottomTabNavigatorIOS = (): JSX.Element => {
  const navigatorConditionStatus = FlowAlteringConditions();

  const middlewareScreensStackVisible = useMemo(() => {
    return countMiddlewareScreens(middlewareScreensMap.standard, true, navigatorConditionStatus);
  }, [navigatorConditionStatus]);

  const authStackVisible = useMemo(() => {
    return navigatorConditionStatus.AUTHORIZED;
  }, [navigatorConditionStatus.AUTHORIZED]);

  return (
    <BottomTab.Navigator
      detachInactiveScreens={true}
      tabBar={({ navigation }) => (
        <AppPlayerBar navigation={navigation as BottomTabNavigationHelpers} />
      )}
    >
      <BottomTab.Group screenOptions={{ headerShown: false }}>
        {middlewareScreensStackVisible && (
          <BottomTab.Screen name={'middleware-tab-stack'} component={IOSMiddlewareStackBuilder} />
        )}
        {authStackVisible && (
          <BottomTab.Screen name={'auth-tab-stack'} component={IOSAuthStackBuilder} />
        )}
      </BottomTab.Group>
    </BottomTab.Navigator>
  );
};

export default BottomTabNavigatorIOS;

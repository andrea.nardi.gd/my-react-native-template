import { useMemo } from 'react';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { authenticatedNavigationMap } from '../../../config/navs';
import { PAGES_DICTIONARIES } from '../../../types/types/navigation/app-page';
import {
  NavigationModalStackBuilder,
  NavigationScreensSequenceBuilder,
  NavigationScreensStackBuilder,
  NavigatorStackWrapper,
} from '../../builders/StackNavigatorBuilders';

import React from 'react';
import FadeInView from '../../../components/layout/container-animations/FadeInView';

const IOSAuthStackBuilder = (): JSX.Element => {
  const insets = useSafeAreaInsets();

  const AuthScreens = useMemo(() => {
    const standardScreens = NavigationScreensStackBuilder(
      authenticatedNavigationMap.standard,
      insets,
      PAGES_DICTIONARIES.AUTHORIZED_PAGES,
    );

    const sequencesScreens = NavigationScreensSequenceBuilder(
      authenticatedNavigationMap.sequences,
      insets,
      PAGES_DICTIONARIES.SEQUENCE_PAGES,
    );
    const modalScreens = NavigationModalStackBuilder(authenticatedNavigationMap.modals, insets);
    return [...standardScreens, ...sequencesScreens, ...modalScreens];
  }, [insets]);

  return (
    <FadeInView fadeInDuration={500}>
      <NavigatorStackWrapper>{AuthScreens}</NavigatorStackWrapper>
    </FadeInView>
  );
};

export default IOSAuthStackBuilder;

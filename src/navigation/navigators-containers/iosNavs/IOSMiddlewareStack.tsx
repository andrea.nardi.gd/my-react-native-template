import { useMemo } from 'react';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { middlewareScreensMap } from '../../../config/navs';
import { PAGES_DICTIONARIES } from '../../../types/types/navigation/app-page';
import FlowAlteringConditions from '../../../utils/selectors/FlowAlteringConditions';
import {
  NavigationModalStackBuilder,
  NavigationScreensStackBuilder,
  NavigatorStackWrapper,
} from '../../builders/StackNavigatorBuilders';

import React from 'react';
import FadeInView from '../../../components/layout/container-animations/FadeInView';

const IOSMiddlewareStackBuilder = (): JSX.Element => {
  const navigatorConditionStatus = FlowAlteringConditions();
  const insets = useSafeAreaInsets();

  const MiddlewareScreens = useMemo(() => {
    const standardScreens = NavigationScreensStackBuilder(
      middlewareScreensMap.standard,
      insets,
      PAGES_DICTIONARIES.MIDDLEWARE_PAGES,
      true,
      navigatorConditionStatus,
    );

    const modalScreens = NavigationModalStackBuilder(middlewareScreensMap.modals, insets);

    return [...standardScreens, ...modalScreens];
  }, [insets, navigatorConditionStatus]);

  return (
    <FadeInView fadeInDuration={500}>
      <NavigatorStackWrapper>{MiddlewareScreens}</NavigatorStackWrapper>
    </FadeInView>
  );
};

export default IOSMiddlewareStackBuilder;

import React from 'react';
import { Platform } from 'react-native';
import AppHeader from '../../components/layout/header/AppHeader';
import config from '../../config';
import BottomTabBarNavigator from './BottomTabBarNavigator';
import DrawerNavigator from './DrawerNavigator';
import StackNavigator from './StackNavigator';

const GeneralNavigatorBuilder = (): JSX.Element => {
  if (config.LAYOUT.ENABLE_SIDE_DRAWER_NAVIGATION) {
    return (
      <>
        {Platform.OS === 'android' && config.LAYOUT.HEADER_ABOVE_DRAWER && <AppHeader />}
        <DrawerNavigator />
      </>
    );
  }

  if (config.LAYOUT.ENABLE_BOTTOM_TAB_NAVIGATION)
    return (
      <>
        {Platform.OS === 'android' && <AppHeader />}
        <BottomTabBarNavigator />
      </>
    );

  return (
    <>
      {Platform.OS === 'android' && <AppHeader />}
      <StackNavigator />
    </>
  );
};

export default GeneralNavigatorBuilder;

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import React from 'react';
import { Platform } from 'react-native';
import AppPlayerBar from '../../components/layout/AppTabBar';
import config from '../../config';

import StackNavigator from './StackNavigator';
import { BottomTabNavigationHelpers } from '@react-navigation/bottom-tabs/lib/typescript/src/types';
import BottomTabNavigatorIOS from './iosNavs/BottomTabNavigatorIos';

const BottomTab = createBottomTabNavigator();

const BottomTabBarNavigator = (): JSX.Element => {
  //const activeScreen = useAppSelector((store) => store.navigationContext.activeScreen);

  if (config.LAYOUT.ENABLE_BOTTOM_TAB_NAVIGATION) {
    if (Platform.OS === 'ios') {
      return <BottomTabNavigatorIOS />;
    }

    return (
      <BottomTab.Navigator
        detachInactiveScreens={true}
        tabBar={({ navigation }) => (
          <AppPlayerBar navigation={navigation as BottomTabNavigationHelpers} />
        )}
      >
        <BottomTab.Screen
          options={{ headerShown: false }}
          name={'BottomTab-Screens'}
          component={StackNavigator}
        />
      </BottomTab.Navigator>
    );
  }
  return <StackNavigator />;
};

export default BottomTabBarNavigator;

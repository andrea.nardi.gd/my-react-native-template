import {
  createNativeStackNavigator,
  NativeStackNavigationOptions,
} from '@react-navigation/native-stack';
import { TransitionPresets } from '@react-navigation/stack';
import React from 'react';
import { Platform } from 'react-native';
import { EdgeInsets } from 'react-native-safe-area-context';
import AppHeader from '../../components/layout/header/AppHeader';
import StackModal from '../../components/layout/modals/modals-layout/StackModal';
import StackScreen from '../../components/layout/pages/screens-layout/StackScreen';
import { SequenceNavs } from '../../config/navs/SequenceNavs';
import PermissionsServices from '../../services/utils/PermissionsServices';
import {
  FlowAlteringConditionsKeys,
  SimplePermissionValues,
} from '../../types/types/config/FlowAlteringConditions';
import { NavigationModal, NavigationWithConditions } from '../../types/types/config/navigation';
import { PAGES_DICTIONARIES } from '../../types/types/navigation/app-page';
import { SequenceStep } from '../../types/types/navigation/sequence/sequence';

const Stack = createNativeStackNavigator();
const { Screen, Navigator } = Stack;

const TransitionScreenOptions = {
  ...TransitionPresets.SlideFromRightIOS,
};

export const NavigationScreensStackBuilder = (
  navMap: Record<string, NavigationWithConditions>,
  insets: EdgeInsets,
  pageDictionary: PAGES_DICTIONARIES,
  shouldCheckPermissions?: boolean,
  checkConditions?: Partial<Record<FlowAlteringConditionsKeys, SimplePermissionValues>>,
): React.ReactNode[] => {
  const temp: React.ReactNode[] = [];
  const NavMapKeys = Object.keys(navMap);
  NavMapKeys.forEach((nav) => {
    const navProperties = navMap[nav];
    const shouldShow =
      shouldCheckPermissions && navProperties.showingConditions && checkConditions
        ? PermissionsServices.canSync({
            currentValues: checkConditions,
            requisites: navProperties.showingConditions,
          })
        : true;
    if (navProperties.enabled && shouldShow) {
      const screen = (
        <Screen
          key={navProperties.name}
          name={navProperties.name}
          initialParams={{
            insetTop: insets.top,
            insetBottom: insets.bottom,
            navigationInfo: navProperties,
            pageDictionary: navProperties.isServiceNav
              ? PAGES_DICTIONARIES.SERVICE_PAGES
              : pageDictionary,
          }}
          component={StackScreen}
          options={{
            animation: navProperties.animation ? navProperties.animation : 'default',
            headerShown: false,

            title: navProperties.title,
          }}
        />
      );

      temp.push(screen);
    }
  });
  return temp;
};

export const NavigationModalStackBuilder = (
  navMap: Record<string, NavigationModal>,
  insets: EdgeInsets,
): React.ReactNode[] => {
  const temp: React.ReactNode[] = [];
  const NavMapKeys = Object.keys(navMap);
  NavMapKeys.forEach((nav) => {
    const modalProperties = navMap[nav];

    if (modalProperties.enabled) {
      const screen = (
        <Screen
          key={modalProperties.name}
          name={modalProperties.name}
          initialParams={{
            insetTop: insets.top,
            insetBottom: insets.bottom,
            navigationInfo: modalProperties,
          }}
          component={StackModal}
          options={{
            presentation: 'transparentModal',
          }}
        />
      );

      temp.push(screen);
    }
  });
  return temp;
};

export const NavigationScreensSequenceBuilder = (
  sequenceMap: SequenceNavs,
  insets: EdgeInsets,
  pageDictionary: PAGES_DICTIONARIES,
  shouldCheckPermissions?: boolean,
  checkConditions?: Partial<Record<FlowAlteringConditionsKeys, SimplePermissionValues>>,
): React.ReactNode[] => {
  let temp = [];
  const sequencesKeys = Object.keys(sequenceMap);

  temp = sequencesKeys.map((sequence) => {
    const sequenceSteps: React.ReactNode[] = [];
    (sequenceMap[sequence as keyof typeof sequenceMap] || []).forEach(
      (step: SequenceStep, index: number) => {
        const shouldShow =
          shouldCheckPermissions && step.showingConditions && checkConditions
            ? PermissionsServices.canSync({
                currentValues: checkConditions,
                requisites: step.showingConditions,
              })
            : true;
        if (step.enabled && shouldShow) {
          const sequenceInfo = {
            steps: sequenceMap[sequence as keyof typeof sequenceMap],
            currentStep: index,
            config: step,
          };
          sequenceSteps.push(
            <Screen
              key={step.name}
              initialParams={{
                insetTop: insets.top,
                insetBottom: insets.bottom,
                navigationInfo: step,
                pageDictionary: pageDictionary,
                sequenceInfo: sequenceInfo,
              }}
              component={StackScreen}
              name={step.name}
              options={{
                //headerShown: step.topNavbarVisible || false,
                headerShown: false,
                animation: 'slide_from_right',
              }}
            />,
          );
        }
      },
    );
    return [...sequenceSteps];
  });
  return temp;
};

export const NavigatorStackWrapper = ({
  children,
}: {
  children: React.ReactNode | React.ReactNode[];
}): JSX.Element => {
  return (
    <>
      {Platform.OS === 'ios' && <AppHeader />}
      <AppHeader />
      <Navigator
        screenOptions={{
          ...(TransitionScreenOptions as NativeStackNavigationOptions),
          headerShown: false,
        }}
      >
        {children}
      </Navigator>
    </>
  );
};

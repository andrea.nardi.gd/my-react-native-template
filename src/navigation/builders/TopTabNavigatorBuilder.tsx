import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { Platform, useWindowDimensions } from 'react-native';
import { TabView, TabBar } from 'react-native-tab-view';
import { Navigation } from '../../types/types/config/navigation';
import { useTranslation } from 'react-i18next';
import TopTabNavsComponents from '../../config/navs/navs-component/TopTabNavsComponents';
import { TopTabOptions, TopTabRoute } from '../../types/types/navigation/navigators/topTab';
import TopTabBarTitle from '../../components/layout/tab/TopTabBarTitle';
import { useIsFocused } from '@react-navigation/native';
import { PAGES_LIST } from '../../config/navs/NavsConfig';
import ServiceNavsComponents from '../../config/navs/navs-component/ServiceNavsComponents';
import FullPage from '../../components/layout/pages/page-layout/FullPage';
import { useAppSelector } from '../../store/hooks';

const TopTabNavigatorBuilder = ({
  tabs,
  tabOptions,
}: {
  tabs: Navigation[];
  tabOptions: TopTabOptions;
}): JSX.Element => {
  const activeModal = useAppSelector((store) => store.navigationContext.activeModal);
  const { t } = useTranslation();
  const [iosKeyHack, setIosKeyHack] = useState(0);
  const layout = useWindowDimensions();
  const isFocused = useIsFocused();

  const [index, setIndex] = useState(tabOptions.firstScreen || 0);
  const routes = useMemo(() => {
    return tabs.map((tab) => {
      return {
        key: tab.name,
        title: t(tab.title || ''),
        icon: tab.icon,
        component: tab.component,
        navigationInfo: tab,
      } as TopTabRoute;
    });
  }, [tabs]);

  const cachedRenderMap = useMemo(() => {
    const tempRenderMap: Record<string, React.ReactNode> = {};
    routes.forEach((route) => {
      const { navigationInfo } = route;
      const pageComponent =
        TopTabNavsComponents[route?.component as PAGES_LIST] || ServiceNavsComponents.default;
      tempRenderMap[route.key] = (
        <FullPage
          navigationInfo={navigationInfo}
          externalLink={navigationInfo?.externalLink || undefined}
          page={pageComponent}
          sequenceInfo={undefined} //@ No sequences inside top tab, it would be pointless
        />
      );
    });

    return tempRenderMap;
  }, [routes]);

  const customRenderMap = useCallback(
    ({ route }: { route: TopTabRoute }) => {
      return cachedRenderMap[route.key];
    },
    [cachedRenderMap],
  );

  useEffect(() => {
    const resetOnLeave = setTimeout(() => {
      if (!isFocused && tabOptions.clearStackOnLeave && !activeModal)
        //NOTE active modal must be null, otherwise we can't open navigation modals on top tab navs without switching the page if clearStackOnLeave is set
        setIndex(tabOptions.firstScreen || 0);
    }, 300);

    return () => {
      clearTimeout(resetOnLeave);
    };
  }, [isFocused, activeModal]);

  useEffect(() => {
    const iosKeyHackCallback = setTimeout(() => {
      if (Platform.OS === 'ios') {
        const count = iosKeyHack + 1;
        setIosKeyHack(count);
      }
    }, 50);
    return () => {
      clearTimeout(iosKeyHackCallback);
    };
  }, [cachedRenderMap, isFocused, routes, tabs]);

  return (
    <TabView
      key={iosKeyHack + '_' + 'active-toptab'}
      lazy={tabOptions.useLazy || false} //!IMPORTANT: set this to false from tab options settings if the entire navigator is the first screen in its navigation stack
      lazyPreloadDistance={tabOptions.lazyPreloadDistance || 1}
      renderTabBar={(props) => (
        <TabBar
          {...props}
          indicatorStyle={{
            backgroundColor: tabOptions.activeTabUnderlineColor,
            height: tabOptions.activeIndicatorHeight,
          }}
          style={{ backgroundColor: tabOptions.titleBackgroundColor }}
          renderLabel={({ route }) => <TopTabBarTitle activeTab={route} options={tabOptions} />}
        />
      )}
      navigationState={{ index, routes }}
      renderScene={customRenderMap}
      onIndexChange={setIndex}
      initialLayout={{ width: layout.width }}
    />
  );
};

export default TopTabNavigatorBuilder;

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import React from 'react';
import { EdgeInsets } from 'react-native-safe-area-context';
import TabScreen from '../../components/layout/pages/screens-layout/TabScreen';
import { SequenceNavs } from '../../config/navs/SequenceNavs';
import PermissionsServices from '../../services/utils/PermissionsServices';
import {
  FlowAlteringConditionsKeys,
  SimplePermissionValues,
} from '../../types/types/config/FlowAlteringConditions';
import { NavigationWithConditions } from '../../types/types/config/navigation';
import { PAGES_DICTIONARIES } from '../../types/types/navigation/app-page';
import { SequenceStep } from '../../types/types/navigation/sequence/sequence';

const Tab = createBottomTabNavigator();
const { Screen, Navigator } = Tab;

export const NavigationScreensBottomTabBuilder = (
  navMap: Record<string, NavigationWithConditions>,
  insets: EdgeInsets,
  pageDictionary: PAGES_DICTIONARIES,
  shouldCheckPermissions?: boolean,
  checkConditions?: Partial<Record<FlowAlteringConditionsKeys, SimplePermissionValues>>,
): React.ReactNode[] => {
  const temp: React.ReactNode[] = [];
  const NavMapKeys = Object.keys(navMap);
  NavMapKeys.forEach((nav) => {
    const navProperties = navMap[nav];
    const shouldShow =
      shouldCheckPermissions && navProperties.showingConditions && checkConditions
        ? PermissionsServices.canSync({
            currentValues: checkConditions,
            requisites: navProperties.showingConditions,
          })
        : true;
    if (navProperties.enabled && shouldShow) {
      const screen = (
        <Screen
          key={navProperties.name}
          name={navProperties.name}
          initialParams={{
            insetTop: insets.top,
            insetBottom: insets.bottom,
            navigationInfo: navProperties,
            pageDictionary: navProperties.isServiceNav
              ? PAGES_DICTIONARIES.SERVICE_PAGES
              : pageDictionary,
          }}
          component={TabScreen}
          options={{
            //animation: navProperties.animation ? navProperties.animation : 'default',
            headerShown: false,
            title: navProperties.title,
          }}
        />
      );

      temp.push(screen);
    }
  });
  return temp;
};

export const countMiddlewareScreens = (
  navMap: Record<string, NavigationWithConditions>,
  shouldCheckPermissions?: boolean,
  checkConditions?: Partial<Record<FlowAlteringConditionsKeys, SimplePermissionValues>>,
): boolean => {
  let shouldShowMiddlewares = false;
  const NavMapKeys = Object.keys(navMap);
  let i = 0;

  while (!shouldShowMiddlewares && i < NavMapKeys.length) {
    const navProperties = navMap[NavMapKeys[i]];
    const shouldShow =
      shouldCheckPermissions && navProperties.showingConditions && checkConditions
        ? PermissionsServices.canSync({
            currentValues: checkConditions,
            requisites: navProperties.showingConditions,
          })
        : true;
    if (navProperties.enabled && shouldShow) {
      shouldShowMiddlewares = true;
    }
    i++;
  }
  return shouldShowMiddlewares;
};

export const NavigationScreensSequenceBottomTabBuilder = (
  sequenceMap: SequenceNavs,
  insets: EdgeInsets,
  pageDictionary: PAGES_DICTIONARIES,
  shouldCheckPermissions?: boolean,
  checkConditions?: Partial<Record<FlowAlteringConditionsKeys, SimplePermissionValues>>,
): React.ReactNode[] => {
  let temp = [];
  const sequencesKeys = Object.keys(sequenceMap);

  temp = sequencesKeys.map((sequence) => {
    const sequenceSteps: React.ReactNode[] = [];
    (sequenceMap[sequence as keyof typeof sequenceMap] || []).forEach(
      (step: SequenceStep, index: number) => {
        const shouldShow =
          shouldCheckPermissions && step.showingConditions && checkConditions
            ? PermissionsServices.canSync({
                currentValues: checkConditions,
                requisites: step.showingConditions,
              })
            : true;
        if (step.enabled && shouldShow) {
          const sequenceInfo = {
            steps: sequenceMap[sequence as keyof typeof sequenceMap],
            currentStep: index,
            config: step,
          };
          sequenceSteps.push(
            <Screen
              key={step.name}
              initialParams={{
                insetTop: insets.top,
                insetBottom: insets.bottom,
                navigationInfo: step,
                pageDictionary: pageDictionary,
                sequenceInfo: sequenceInfo,
              }}
              component={TabScreen}
              name={step.name}
              options={{
                headerShown: false,
                //animation: 'slide_from_right',
              }}
            />,
          );
        }
      },
    );
    return [...sequenceSteps];
  });
  return temp;
};

export const NavigatorStackWrapper = ({
  children,
}: {
  children: React.ReactNode | React.ReactNode[];
}): JSX.Element => {
  return <Navigator>{children}</Navigator>;
};

import { StyleSheet } from 'react-native';
import { globalStyles } from '../../config/styles/GlobalStyles';

const styles = StyleSheet.create({
  UnsafeArea: {
    flex: 1,
    backgroundColor: globalStyles.UNSAFE_AREA_TOP_COLOR,
  },
});

export default styles;

import { NavigationContainer, useNavigationContainerRef } from '@react-navigation/native';
import React, { useRef } from 'react';
import { useDispatch } from 'react-redux';
import * as Linking from 'expo-linking';
import {
  setActiveModalAction,
  setActiveScreenAction,
  unsetActiveModalAction,
} from '../actions/navigation';
import { Navigation, NavigationModal } from '../types/types/config/navigation';
import { SequenceStep } from '../types/types/navigation/sequence/sequence';

import { deepLinkingConfig } from '../config/navs/deepLinkingConfig';

import config from '../config';
import GeneralNavigatorBuilder from './navigators-containers/GeneralNavigator';
import { NavigationTheme } from '../config/navs/theme/NavigationTheme';
import DeepLinkingMonitor from './linking/DeepLinkingMonitor';
interface RootNavigatorProps {
  parentSetActiveScreen: (screen: Navigation | SequenceStep) => void;
}

const prefix = Linking.createURL('/');
const linking = {
  prefixes: [prefix],
};

const RootNavigator = ({ parentSetActiveScreen }: RootNavigatorProps): JSX.Element => {
  const navigationRef = useNavigationContainerRef();
  const routeNameRef = useRef<string>();

  const dispatch = useDispatch();

  const dispatchSetActiveScreen = async (screen: Navigation | SequenceStep) => {
    dispatch(setActiveScreenAction(screen));
    dispatch(unsetActiveModalAction());
  };

  const internalSetActiveScreen = (screen: Navigation | SequenceStep) => {
    parentSetActiveScreen(screen);
    dispatchSetActiveScreen(screen);
  };

  return (
    <NavigationContainer
      theme={NavigationTheme}
      ref={navigationRef}
      onReady={() => {
        routeNameRef.current = navigationRef?.getCurrentRoute()?.name;
        //@ts-ignore
        internalSetActiveScreen(navigationRef?.getCurrentRoute()?.params?.navigationInfo || {});
      }}
      linking={{ ...linking, ...deepLinkingConfig }}
      onStateChange={async () => {
        const currentRouteName = navigationRef?.getCurrentRoute()?.name;
        //@ts-ignore
        const navigationInfo = navigationRef?.getCurrentRoute()?.params?.navigationInfo as
          | Navigation
          | NavigationModal;
        if (!navigationInfo.isModal) {
          internalSetActiveScreen(navigationInfo || {});
          routeNameRef.current = currentRouteName;
        } else {
          dispatch(setActiveModalAction(navigationInfo as NavigationModal));
        }
      }}
    >
      {config.NETWORK_SETTINGS.ENABLE_DEEP_LINKING && <DeepLinkingMonitor />}
      <GeneralNavigatorBuilder />
    </NavigationContainer>
  );
};

export default RootNavigator;

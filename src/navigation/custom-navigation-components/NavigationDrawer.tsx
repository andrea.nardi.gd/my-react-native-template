import { DrawerNavigationHelpers } from '@react-navigation/drawer/lib/typescript/src/types';
import React from 'react';
import { View } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import AppText from '../../components/layout/text/AppText';
import { useAppSelector } from '../../store/hooks';

const NavigationDrawerComponent = () => {
  return (
    <View style={{ height: 1000 }}>
      <AppText>Mock Navigation Drawer Component Left</AppText>
    </View>
  );
};

const NavigationDrawer = ({ navigation }: { navigation: DrawerNavigationHelpers }): JSX.Element => {
  const _activePage = useAppSelector((store) => store.navigationContext.activeScreen);

  return (
    <ScrollView>
      <NavigationDrawerComponent />
    </ScrollView>
  );
};

export default NavigationDrawer;

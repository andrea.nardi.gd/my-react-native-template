import { useEffect, useState } from 'react';
import * as Linking from 'expo-linking';
import { useDispatch } from 'react-redux';
import { useNavigation } from '@react-navigation/native';
import { AppHooksReturn } from '../../types/types/hooks/AppHooks';
import FlowAlteringConditions from '../../utils/selectors/FlowAlteringConditions';
import { EventType } from 'expo-linking';
import DeepLinkingServices from '../../services/local/DeepLinkingServices';
import { StackNavigationHelpers } from '@react-navigation/stack/lib/typescript/src/types';

const DeepLinkingHandler = (): AppHooksReturn => {
  const [activeUrl, setActiveUrl] = useState<Linking.ParsedURL | undefined>(undefined);

  const dispatch = useDispatch();

  const navigatorConditionStatus = FlowAlteringConditions();
  const navigation = useNavigation();

  const setActiveLink = (e: EventType) => {
    const parsedUrl = Linking.parse(e.url);
    setActiveUrl(parsedUrl);
  };

  const parseInitialUrl = async () => {
    const parsedUrl = await Linking.parseInitialURLAsync();
    setActiveUrl(parsedUrl);
  };

  useEffect(() => {
    if (activeUrl) {
      dispatch(
        DeepLinkingServices.ExecuteDeepLinkAction({
          url: activeUrl,
          checkParams: navigatorConditionStatus,
          navigation: navigation as unknown as StackNavigationHelpers,
        }),
      );
    }
  }, [activeUrl]);

  useEffect(() => {
    Linking.addEventListener('url', setActiveLink);
    if (!activeUrl) {
      parseInitialUrl();
    }

    return () => {
      Linking.removeEventListener('url', setActiveLink);
    };
  }, []);
};

export default DeepLinkingHandler;

import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faMugSaucer } from '@fortawesome/free-solid-svg-icons/faMugSaucer';

export default {
  ICONS: {
    home: (color: string, size = 36): JSX.Element => (
      <FontAwesomeIcon icon={faMugSaucer} color={color} size={size} />
    ),
    events: (color: string, size = 36): JSX.Element => (
      <FontAwesomeIcon icon={faMugSaucer} color={color} size={size} />
    ),
    cards: (color: string, size = 36): JSX.Element => (
      <FontAwesomeIcon icon={faMugSaucer} color={color} size={size} />
    ),
    wallet: (color: string, size = 36): JSX.Element => (
      <FontAwesomeIcon icon={faMugSaucer} color={color} size={size} />
    ),
    shopPointer: (color: string, size = 36): JSX.Element => (
      <FontAwesomeIcon icon={faMugSaucer} color={color} size={size} />
    ),
    shop: (color: string, size = 36): JSX.Element => (
      <FontAwesomeIcon icon={faMugSaucer} color={color} size={size} />
    ),
  },
};

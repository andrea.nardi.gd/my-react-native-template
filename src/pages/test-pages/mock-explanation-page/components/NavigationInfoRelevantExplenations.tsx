import { StyleSheet } from 'react-native';
import React from 'react';
import SettingsGroupWrapper from '../../../../components/ui/settings/SettingsGroupWrapper';
import SettingButton, { CallbackActivator } from '../../../../components/ui/settings/SettingButton';
import { useAppSelector } from '../../../../store/hooks';
import { useNavigation } from '@react-navigation/native';
import { PAGES_LIST } from '../../../../config/navs/NavsConfig';

const NavigationRelevantExplenation = (): JSX.Element => {
  const AppColors = useAppSelector((store) => store.theme.AppColors);
  const navigation = useNavigation();
  return (
    <>
      <SettingsGroupWrapper
        label="Relevant Navigation Info"
        entriesWrapperStyle={[
          styles.mockWrapperSettings,
          {
            backgroundColor: AppColors.secondary100,
            borderColor: AppColors.error100,
          },
        ]}
        labelStyle={{
          marginTop: 10,
          color: AppColors.primary500,
        }}
      >
        <SettingButton
          iconSize={24}
          callback={() => navigation.navigate(PAGES_LIST.TEST_AUTH_LIST_INFO as never)}
          //additionalFieldValue={'Modal'}
          buttonActivator={CallbackActivator.BUTTON_PRESSION}
          title={'List of Navigation Fields'}
          description={'See the list of the relevant fields for page navigation'}
        />
      </SettingsGroupWrapper>
    </>
  );
};

const styles = StyleSheet.create({
  mockWrapperSettings: {
    width: '100%',
    padding: 5,
    borderRadius: 10,
    fontSize: 20,
    borderWidth: 1,
    marginTop: 5,
  },
});

export default NavigationRelevantExplenation;

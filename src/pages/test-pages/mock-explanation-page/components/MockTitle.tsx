import { StyleSheet } from 'react-native';
import React from 'react';
import AppText from '../../../../components/layout/text/AppText';
import { useAppSelector } from '../../../../store/hooks';
import { Navigation } from '../../../../types/types/config/navigation';
import { useTranslation } from 'react-i18next';
import FlexAlignView from '../../../../components/layout/utilities/FlexAlignView';

const MockTitle = ({ navProperties }: { navProperties: Navigation }): JSX.Element => {
  const { t } = useTranslation();
  const AppColors = useAppSelector((store) => store.theme.AppColors);
  return (
    <FlexAlignView direction="column">
      <AppText
        style={[
          styles.title,
          {
            backgroundColor: AppColors.secondary100,
            color: AppColors.primary500,
            borderColor: AppColors.error100,
          },
        ]}
        fontWeight={'bold'}
      >
        {t('This is a test template page')}
        <AppText
          fontStyle="italic"
          style={[
            styles.subtitle,
            {
              color: AppColors.primary500,
              borderColor: AppColors.error100,
            },
          ]}
          fontWeight={'bold'}
        >
          {t('\nsorry for the horrible styling')}
        </AppText>
        <AppText
          fontStyle="italic"
          style={[
            styles.subsubtitle,
            {
              color: AppColors.primary500,
              borderColor: AppColors.error100,
            },
          ]}
          fontWeight={'bold'}
        >
          {t(
            '\n (also this page has been tested only on android. Read note in <AppText /> component to see potential issues related to nested instances of it)',
          )}
        </AppText>
      </AppText>

      <AppText
        style={[
          styles.paragraph,
          {
            backgroundColor: AppColors.secondary100,
            color: AppColors.primary500,
            borderColor: AppColors.error100,
          },
        ]}
      >
        You are seeing this page because you've navigated to a screen that doesn't have a component
        associated to it. You can scroll below (if you see no property noScroll set to true,
        otherwise scroll will be blocked) to check some things about this page.
      </AppText>

      <AppText
        style={[
          styles.paragraph,
          {
            backgroundColor: AppColors.secondary100,
            color: AppColors.primary500,
            borderColor: AppColors.error100,
          },
        ]}
      >
        You can tinker with page settings directly on the page configuration. You can find the root
        with all the screens in{' '}
        <AppText fontStyle="italic" style={{ color: AppColors.blue800 }}>
          src/config/navs/index.ts
        </AppText>{' '}
        in the project folder. From there, just go back on the imports and find the relevant screen
        in the various dictionaries. You can also bind a component to the screen (by binding the
        unique navigation name to a React component in the configuration files inside{' '}
        <AppText fontStyle="italic" style={{ color: AppColors.blue800 }}>
          src/config/navs/navs-component{' '}
        </AppText>
        ), and if you do you'll see that component instead of this page.
      </AppText>

      <AppText
        style={[
          styles.paragraph,
          {
            backgroundColor: AppColors.secondary100,
            color: AppColors.primary500,
            borderColor: AppColors.error100,
          },
        ]}
      >
        <AppText fontWeight="bold">Current page settings are: </AppText>
        <AppText
          style={[
            styles.paragraph,
            {
              backgroundColor: AppColors.secondary100,
              color: AppColors.primary500,
              borderColor: AppColors.error100,
            },
          ]}
        >
          {JSON.stringify(navProperties, null, 2)}
        </AppText>
      </AppText>
    </FlexAlignView>
  );
};

export default MockTitle;

const styles = StyleSheet.create({
  title: {
    textAlign: 'center',
    width: '100%',
    padding: 10,
    borderRadius: 10,
    fontSize: 20,
    borderWidth: 1,
  },

  subtitle: {
    textAlign: 'center',
    width: '100%',
    padding: 5,
    borderRadius: 10,
    fontSize: 12,
    borderWidth: 1,
  },

  subsubtitle: {
    textAlign: 'center',
    width: '100%',
    padding: 5,
    borderRadius: 10,
    fontSize: 10,
    borderWidth: 1,
  },

  paragraph: {
    marginTop: 10,
    textAlign: 'left',
    width: '100%',
    padding: 10,
    borderRadius: 10,
    fontSize: 16,
    borderWidth: 1,
  },
});

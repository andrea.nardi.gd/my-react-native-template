import { Alert, StyleSheet } from 'react-native';
import React, { useCallback, useState } from 'react';
import SettingsGroupWrapper from '../../../../components/ui/settings/SettingsGroupWrapper';
import SettingButton, { CallbackActivator } from '../../../../components/ui/settings/SettingButton';
import UI_Icons from '../../../../components/icons';
import { useNavigation } from '@react-navigation/native';
import { MODALS_LIST } from '../../../../config/navs/NavsConfig';
import { useAppSelector } from '../../../../store/hooks';

const MockSettings = (): JSX.Element => {
  const AppColors = useAppSelector((store) => store.theme.AppColors);

  const navigation = useNavigation();
  const [toggleValue, setToggleValue] = useState(false);

  const toggleValueCallback = useCallback(() => {
    setToggleValue(!toggleValue);
  }, [toggleValue]);

  const toggleSimpleCallback = useCallback(() => {
    Alert.alert('Simple callback called successfully');
  }, []);

  return (
    <SettingsGroupWrapper
      label="Mock Settings"
      entriesWrapperStyle={[
        styles.mockWrapperSettings,
        {
          backgroundColor: AppColors.secondary100,
          borderColor: AppColors.error100,
        },
      ]}
      labelStyle={{
        marginTop: 10,
        color: AppColors.primary500,
      }}
    >
      <SettingButton
        callback={toggleValueCallback}
        iconDictionary={UI_Icons.SETTINGS}
        icon={'EMPTY_CIRCULAR_IMAGE_PLACEHOLDER'}
        buttonActivator={CallbackActivator.TOGGLE_PRESSION}
        title={'Enable button below'}
        toggleValue={toggleValue}
        description={'This enables or disables the button below based on the value of the toggle'}
      />
      <SettingButton
        callback={toggleSimpleCallback}
        iconSize={24}
        enabled={toggleValue}
        buttonActivator={CallbackActivator.BUTTON_PRESSION}
        title={'Simple Callback'}
        description={'If enabled, this should fire an alert when pressed'}
      />
      <SettingButton
        iconSize={24}
        callback={() => navigation?.navigate(MODALS_LIST.TEST_MODAL as never)}
        additionalFieldValue={'Modal'}
        buttonActivator={CallbackActivator.BUTTON_PRESSION}
        title={'Modal'}
        description={'This opens a modal page when pressed'}
      />
    </SettingsGroupWrapper>
  );
};

const styles = StyleSheet.create({
  mockWrapperSettings: {
    width: '100%',
    padding: 5,
    borderRadius: 10,
    fontSize: 20,
    borderWidth: 1,
    marginTop: 5,
  },
});

export default MockSettings;

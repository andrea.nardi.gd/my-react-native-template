import React from 'react';
import { View } from 'react-native';
import { NavigationPageProps } from '../../../types/interfaces/navigation-pages/NavigationPageProps';
import { Navigation } from '../../../types/types/config/navigation';
import MockSettings from './components/MockSettings';
import MockTitle from './components/MockTitle';
import NavigationRelevantExplenation from './components/NavigationInfoRelevantExplenations';

const MockExplanationPage = ({ navProperties }: NavigationPageProps): JSX.Element => {
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: navProperties?.pageBackgroundColor || 'transparent',
      }}
    >
      <MockTitle navProperties={navProperties as Navigation} />
      <NavigationRelevantExplenation />
      <MockSettings />
    </View>
  );
};

export default MockExplanationPage;

import React, { useCallback, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Alert, Dimensions } from 'react-native';
import UI_Icons from '../../components/icons';
import AppText from '../../components/layout/text/AppText';
import FlexAlignView from '../../components/layout/utilities/FlexAlignView';
import SettingButton, { CallbackActivator } from '../../components/ui/settings/SettingButton';
import SettingsGroupWrapper from '../../components/ui/settings/SettingsGroupWrapper';
import { MODALS_LIST } from '../../config/navs/NavsConfig';
import { useAppSelector } from '../../store/hooks';
import { NavigationPageProps } from '../../types/interfaces/navigation-pages/NavigationPageProps';

const MockEmptyPage = ({ navProperties, navigation }: NavigationPageProps): JSX.Element => {
  const AppColors = useAppSelector((store) => store.theme.AppColors);
  const { t } = useTranslation();
  const [toggleValue, setToggleValue] = useState(false);

  const toggleValueCallback = useCallback(() => {
    setToggleValue(!toggleValue);
  }, [toggleValue]);

  const toggleSimpleCallback = useCallback(() => {
    Alert.alert('Simple callback successfully called');
  }, []);

  return (
    <FlexAlignView
      direction="column"
      style={{
        backgroundColor: navProperties?.pageBackgroundColor || 'transparent',
      }}
    >
      <AppText
        style={{
          marginTop: Dimensions.get('screen').height / 3,
          textAlign: 'center',
          width: '100%',
          backgroundColor: AppColors.secondary100,
          color: AppColors.primary500,
          padding: 20,
        }}
        fontWeight={'bold'}
      >
        {t('Test Page for => ')} {navProperties?.name}
      </AppText>
      {navProperties?.drawerNavigator && (
        <AppText
          style={{
            marginTop: Dimensions.get('screen').height / 3,
            textAlign: 'center',
            width: '100%',
            backgroundColor: AppColors.secondary100,
            color: AppColors.primary500,
            padding: 20,
          }}
          fontWeight={'bold'}
        >
          {t(
            'Slide Left to open drawer navigator (but avoid if debug mode is active, it causes problems with chrome debugger tools)',
          )}
        </AppText>
      )}
      <SettingsGroupWrapper label="Group label">
        <SettingButton
          callback={toggleValueCallback}
          iconDictionary={UI_Icons.SETTINGS}
          icon={'EMPTY_CIRCULAR_IMAGE_PLACEHOLDER'}
          buttonActivator={CallbackActivator.TOGGLE_PRESSION}
          title={'Enable button below'}
          toggleValue={toggleValue}
          description={'This enables or disable the button below based on the value of the toggle'}
        />
        <SettingButton
          callback={toggleSimpleCallback}
          iconSize={24}
          enabled={toggleValue}
          //icon={UI_Icons.SETTINGS.EMPTY_CIRCULAR_IMAGE_PLACEHOLDER(AppColors.blue200, 24)}
          buttonActivator={CallbackActivator.BUTTON_PRESSION}
          title={'Simple Callback'}
          description={'If enabled, this should fire an alert'}
        />
        <SettingButton
          iconSize={24}
          //icon={UI_Icons.SETTINGS.EMPTY_CIRCULAR_IMAGE_PLACEHOLDER(AppColors.blue200, 24)}
          callback={() => navigation?.navigate(MODALS_LIST.TEST_MODAL as never)}
          additionalFieldValue={'Modal'}
          buttonActivator={CallbackActivator.BUTTON_PRESSION}
          title={'Modal'}
          description={'This open a modal page'}
        />
      </SettingsGroupWrapper>
    </FlexAlignView>
  );
};

export default MockEmptyPage;

import React from 'react';
import { View } from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
import VerticalSeparator from '../../../components/layout/utilities/VerticalSeparator';
import SettingButton, { CallbackActivator } from '../../../components/ui/settings/SettingButton';
import { useAppSelector } from '../../../store/hooks';
import { SizeVariants } from '../../../types/types/styles/sizes';
import { navigationInfoDataConfig } from './data/navigationInfoDataConfig';

const MockFieldExplanationPage = (): JSX.Element => {
  const AppColors = useAppSelector((store) => store.theme.AppColors);
  return (
    <>
      <FlatList
        stickyHeaderIndices={[0]}
        ListHeaderComponentStyle={{
          backgroundColor: AppColors.secondary900,
          borderBottomColor: AppColors.primary600,
          borderBottomWidth: 1,
        }}
        ListHeaderComponent={() => (
          <>
            <SettingButton
              iconSize={24}
              callback={() => void 0}
              //additionalFieldValue={'Modal'}
              buttonActivator={CallbackActivator.BUTTON_PRESSION}
              title={'List of Navigation Fields'}
              description={'See the list of the relevant fields for page navigation'}
            />
            <VerticalSeparator distance={SizeVariants.SMALL} />
          </>
        )}
        data={navigationInfoDataConfig}
        keyExtractor={(item) => item.key}
        renderItem={({ item, index }) => {
          return (
            <View
              style={{
                backgroundColor: index % 2 === 0 ? AppColors.warning100 : AppColors.secondary100,
              }}
            >
              <SettingButton
                iconSize={24}
                callback={() => void 0}
                //additionalFieldValue={'Modal'}
                buttonActivator={CallbackActivator.BUTTON_PRESSION}
                title={item.key}
                description={item.description}
              />
            </View>
          );
        }}
      />
    </>
  );
};
export default MockFieldExplanationPage;

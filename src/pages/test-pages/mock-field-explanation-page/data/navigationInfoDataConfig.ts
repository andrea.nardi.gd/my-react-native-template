import { NavigationWithConditions } from '../../../../types/types/config/navigation';

type navigationInfoDataDict = {
  key: keyof NavigationWithConditions;
  description: string;
  type?: string;
};

export const navigationInfoDataConfig: navigationInfoDataDict[] = [
  {
    key: 'showTitle',
    description: 'A setting used to decide if you want to show the title on the header bar or not.',
  },
  {
    key: 'showLogo',
    description: 'A setting used to decide if you want to show the logo on the header bar or not.',
  },
  {
    key: 'isModal',
    description:
      'A setting used to mark a screen as a modal. Useful if you do not want to track the current screen as the active screen',
  },
  {
    key: 'onFocusCallBack',
    description:
      'If active, the page executes a callback when entering the navigation stack. You can also set a delay for the execution',
  },
  {
    key: 'onLeaveCallBack',
    description:
      'If active, the page executes a callback when leaving the navigation stack. You can also set a delay for the execution',
  },
  {
    key: 'onScrollCallback',
    description:
      'If active, you can scroll the page from top to execute a callback (like a refresh or something similar)',
  },
  {
    key: 'blockGoBack',
    description:
      'If set, you cannot use the back navigation to go to the previous page on the stack. The back button cleans the stack an navigates to the first screen of the navigation group',
  },
  {
    key: 'showBackButton',
    description:
      'Use it to mark if you want to show a generic back button for the page (eg. on the Header bar)',
  },
  {
    key: 'backButtonlabel',
    description: 'Use it to define a custom text for the back button',
  },
  {
    key: 'backButtonDestination',
    description: 'Use it to define a custom route for the back button press',
  },
  {
    key: 'onlineNeeded',
    description: 'Use it to define if a specific page needs online connection',
  },
  {
    key: 'externalLink',
    description:
      'If a page is a webview, you should define this field and put the url of the webview here',
  },
  {
    key: 'askConfirmationOnBack',
    description:
      'If active, it spawns a modal that asks for a confirmation before going back from the current page',
  },
  {
    key: 'askConfirmationOnChange',
    description:
      'If active, it spawns a modal that asks for a confirmation before changing the current page',
  },
  {
    key: 'hasHidingCondition',
    description:
      'Enable this if the page has some hiding condition in some instances. For example, if you want to remove its icon from the tabBar on certain occasions',
  },
  {
    key: 'section',
    description:
      "The section of which this page is part of. It's mostly used to change the color of the current icon on the navbar or the header",
  },
  {
    key: 'topNavbarVisible',
    description:
      'If enabled, the top bar (header) is visible. Otherwise, it is not (should be implemented on the header component based on the active screen hook)',
  },
  {
    key: 'tabBarHidden',
    description: 'If enabled, the bottom bar (tab bar) is hidden. Otherwise, it is visible',
  },
  {
    key: 'component',
    description:
      'The key that contains the component to bind to this screen. The binding are made inside the dictionaries of components in src/config/navs/navs-component',
  },
  {
    key: 'icon',
    description:
      'A string that matches the key of an icon in a dictionaries of icon. Used to associate an icon to a specific page, for example in the tab screen',
  },
  {
    key: 'enabled',
    description:
      'Mark if the page is enabled or not. Disabled page are not added to their navigation stack',
  },
  {
    key: 'statusBarBgColor',
    description:
      'You can pass a color string here to set the background color of the status bar for a specific page',
  },
  {
    key: 'pageBackgroundColor',
    description: 'You can pass a color string here to set the background color for a specific page',
  },
  {
    key: 'noMargin',
    description: 'If set, the page container has no margins',
  },
  {
    key: 'noScroll',
    description:
      'If set, the page wrapper itself is not scrollable. You need to set this to true if your page contains a vertical flatlist, a nested top tab navigator, or if it is a web view',
  },
  {
    key: 'animation',
    description:
      'Use this to override the default native stack animation transition for a single page. Autocompletes with accepted values',
  },
  {
    key: 'isServiceNav',
    description:
      'Mark the navigation as a serviceNav if its component is binded in the ServiceNavsComponents . Mostly used for screens that are shared between different navigators',
  },
  {
    key: 'drawerNavigator',
    description:
      'If enabled, you can open a side drawer navigator in the current page (by default by pulling it from the left, but you can access an imperative api to trigger it inside the page or the Header if you want)',
  },
  {
    key: 'drawerNavigatorPosition',
    description: 'You can specify the side of the screen used by the drawer navigator',
  },
  {
    key: 'drawerNavigatorComponent',
    description:
      'Just like the page component, a key that binds a custom navigator component to a drawer navigator for a single page',
  },
  {
    key: 'nestedTopNavigatorName',
    description:
      'Just like the page component, a key that binds an internal Top Tab Navigator as the component from the page',
  },
  {
    key: 'showingConditions',
    description:
      'A set of condition to satisfy in order to add this screen to the current navigation stack. This are checked during the stack building phase inside StackNavigatorBuilders.tsx, and by default they check the values stored inside the FlowAlteringConditions selector.',
  },
];

import React, { useMemo } from 'react';
import { topTabNavs } from '../../config/navs/top-tab-navs/TopTabNavs';
import { topTabOptions } from '../../config/navs/top-tab-navs/TopTabOptions';
import TopTabNavigatorBuilder from '../../navigation/builders/TopTabNavigatorBuilder';
import { NavigationPageProps } from '../../types/interfaces/navigation-pages/NavigationPageProps';

const TestTopTabPage = ({ navProperties }: NavigationPageProps): JSX.Element => {
  const [tabs, options] = useMemo(() => {
    const nestednavigatorName = navProperties?.nestedTopNavigatorName;
    if (nestednavigatorName) {
      const tempTabs = topTabNavs[nestednavigatorName];
      const tempTabOptions = topTabOptions[nestednavigatorName] || topTabOptions.DEFAULT;
      return [tempTabs, tempTabOptions];
    }
    return [undefined, undefined];
  }, [navProperties]);

  if (tabs && options) {
    return <TopTabNavigatorBuilder tabs={tabs} tabOptions={options} />;
  }
  return <></>;
};

export default TestTopTabPage;

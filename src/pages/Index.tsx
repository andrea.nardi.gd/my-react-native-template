import React, { useCallback, useEffect, useState } from 'react';
import * as SplashScreen from 'expo-splash-screen';
import { View } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import RootNavigator from '../navigation/RootNavigator';
import ConfigServices from '../services/config/ConfigServices';
import { useDispatch } from 'react-redux';
import { setInizialitazionFailed } from '../actions/app-status';
import { globalStyles } from '../config/styles/GlobalStyles';
import { Navigation } from '../types/types/config/navigation';
import AppStatusBar from '../components/layout/AppStatusBar';
import AppServices from '../services/local/AppServices';
import AppLoader from './service-pages/AppLoader';
import { SequenceStep } from '../types/types/navigation/sequence/sequence';
import Logger from '../services/local/Logger';
import MessageServices from '../services/local/MessageServices';
import StartSystemsMonitor from '../components/utils/SystemMonitor';

const Index = (): JSX.Element => {
  const dispatch = useDispatch();
  const [fetchConfigStart, setFetchConfigStart] = useState(false);
  const [activeScreen, setActiveScreen] = useState<Navigation | SequenceStep | null>(null);
  const _systemMonitors = StartSystemsMonitor();

  const parentSetActiveScreen = useCallback((screen: Navigation | SequenceStep | null) => {
    setActiveScreen(screen);
  }, []);

  const initApp = useCallback(async () => {
    try {
      await SplashScreen.preventAutoHideAsync();
      await Promise.all([
        await Promise.resolve(await AppServices.loadAppFonts()),
        await Promise.resolve(await AppServices.cacheImages()),
        await Promise.resolve(await dispatch(ConfigServices.setupConfig())),
      ])
        .then(() => setFetchConfigStart(true))
        .catch(() => setFetchConfigStart(true));
    } catch (e) {
      Logger.LogError({
        prefixes: 'INIZIALIZATION',
        args: [
          'Initizialization failed',
          `Reason: ${MessageServices.extractKeyFromError(e) as string}`,
        ],
      });
      setFetchConfigStart(true);
      dispatch(setInizialitazionFailed());
    }
  }, [dispatch]);

  const onLayoutRootView = useCallback(async () => {
    if (fetchConfigStart) {
      await SplashScreen.hideAsync();
    }
  }, [fetchConfigStart]);

  useEffect(() => {
    initApp();
  }, []);

  if (!fetchConfigStart) {
    return <AppLoader />;
  }

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: activeScreen?.pageBackgroundColor
          ? activeScreen.pageBackgroundColor
          : globalStyles.COLOR_CONFIG.appDefaultBackgroundColor,
      }}
      onLayout={onLayoutRootView}
    >
      {activeScreen && <AppStatusBar activePage={activeScreen} />}
      <SafeAreaView edges={['left', 'right', 'top']} style={{ flex: 1, height: '100%' }}>
        <RootNavigator parentSetActiveScreen={parentSetActiveScreen} />
      </SafeAreaView>
    </View>
  );
};

export default Index;

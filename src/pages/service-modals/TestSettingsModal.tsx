import React from 'react';
import { View } from 'react-native';
import { useDispatch } from 'react-redux';
import AppText from '../../components/layout/text/AppText';
import FlexAlignView from '../../components/layout/utilities/FlexAlignView';
import AppConfirmButton from '../../components/ui/buttons/AppConfirmButton';
import MessageServices from '../../services/local/MessageServices';
import { useAppSelector } from '../../store/hooks';
import { NavigationPageProps } from '../../types/interfaces/navigation-pages/NavigationPageProps';

function TestSettingModalScreen({ navigation }: NavigationPageProps): JSX.Element {
  const dispatch = useDispatch();
  const AppColors = useAppSelector((store) => store.theme.AppColors);
  return (
    <View
      style={{
        width: '80%',
        height: '30%',
        padding: 20,
        borderRadius: 10,
        backgroundColor: AppColors.secondary100,
      }}
    >
      <AppText style={{ fontSize: 24, color: AppColors.primary500 }}>
        This is a setting modal!
      </AppText>
      <FlexAlignView direction="column" style={{ width: '100%' }}>
        <AppConfirmButton
          confirmFunction={() =>
            dispatch(
              MessageServices.emitMessage({
                allowUnsupportedTranslation: true,
                message:
                  'I will stay on screen even if you close the modal, but I will disappear if you click me or after a set period of time',
                type: 'success',
              }),
            )
          }
          title="Spawn a snackbar message"
        />
        <AppConfirmButton confirmFunction={() => navigation?.goBack()} title="close" />
      </FlexAlignView>
    </View>
  );
}

export default TestSettingModalScreen;

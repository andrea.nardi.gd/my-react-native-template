import React, { useEffect, useState } from 'react';
import { View } from 'react-native';
import styles from './styles';
import { useTranslation } from 'react-i18next';

import SvgWrapper from '../../../components/images/SvgWrapper';
import { CustomParamsRouteKeys } from '../../../types/types/navigation/routes/CustomParamsRoutesList';
import {
  ExternalRedirect,
  ImagePageProps,
  ImagePagesImageDict,
  ImagePagesPng,
  Redirect,
} from '../../../config/navs/ImagePages';
import { CustomRoutePageProps } from '../../../types/interfaces/navigation-pages/NavigationPageProps';
import { AppXOR } from '../../../types/types/utils';
import AppConfirmButton from '../../../components/ui/buttons/AppConfirmButton';
import SmallCancelButton from '../../../components/ui/buttons/SmallCancelButton';
import AppText from '../../../components/layout/text/AppText';
import { ColorVariants } from '../../../types/types/styles/themes';
import { SizeVariants } from '../../../types/types/styles/sizes';
import LinkingServices from '../../../services/local/LinkingServices';
import AppImage from '../../../components/images/AppImage';
import { useAppSelector } from '../../../store/hooks';

type GeneralImagePageProps = AppXOR<
  CustomRoutePageProps<CustomParamsRouteKeys.GENERAL_IMAGE_PAGE>,
  {
    imageProp?: ImagePageProps;
    isOutsideNav?: boolean;
  }
>;

const GeneralImagePage = ({ imageProp, navigation, route }: GeneralImagePageProps): JSX.Element => {
  const AppColors = useAppSelector((store) => store.theme.AppColors);

  const { t } = useTranslation();
  const [pageElements, setPageElements] = useState<ImagePageProps | null>(null);

  useEffect(() => {
    if (imageProp) {
      setPageElements(imageProp);
    } else {
      setPageElements(route?.params || null);
    }
  }, [route, imageProp]);

  const navigationFunction = (redirect: Redirect) => {
    navigation?.pop();
    navigation?.navigate(
      redirect.page as never,
      redirect.optionalParams ? ({ ...redirect.optionalParams } as never) : ({} as never),
    );
  };

  const externalNavigationFunction = (redirect: ExternalRedirect) => {
    const { link, redirectType } = redirect;
    switch (redirectType) {
      default:
      case 'url':
        LinkingServices.openBrowserApp(link);
        break;
      case 'email':
        LinkingServices.openMailAppLink(link);
        break;
      case 'phone':
        LinkingServices.openPhoneAppLink(link);
        break;
    }
  };

  return (
    <View
      style={{
        backgroundColor:
          pageElements?.background === ColorVariants.PRIMARY
            ? AppColors.primary500
            : AppColors.secondary100,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 30,
      }}
    >
      <View
        style={{
          alignItems: 'center',
        }}
      >
        {pageElements?.image &&
          (pageElements?.imageType && pageElements.imageType === 'png' ? (
            <View
              style={{
                alignSelf: 'center',
                height: 250,
                width: 250,
                marginTop: 100,
              }}
            >
              <AppImage
                useAltImage
                resizeMode="contain"
                source={{ uri: ImagePagesPng[pageElements.image] }}
                height={250}
                width={250}
                style={{ height: '100%', width: '100%', resizeMode: 'contain' }}
              />
            </View>
          ) : (
            <SvgWrapper
              svgComponent={ImagePagesImageDict[pageElements.image]}
              containerStyle={{
                alignSelf: 'center',
                height: 250,
                width: 250,
                marginTop: 100,
              }}
            />
          ))}

        {pageElements?.title && (
          <AppText
            fontWeight="bold"
            style={
              pageElements?.background === ColorVariants.PRIMARY ? styles.title : styles.titleAlt
            }
          >
            {t(pageElements.title)}
          </AppText>
        )}
        {pageElements?.subtitles && (
          <AppText
            fontWeight="600"
            style={
              pageElements?.background === ColorVariants.PRIMARY
                ? styles.subTitle
                : styles.subTitleAlt
            }
          >
            {t(pageElements.title)}
          </AppText>
        )}
        {pageElements?.descriptions &&
          pageElements.descriptions.map((description, i) => {
            return (
              <AppText
                fontWeight="300"
                key={(description + '-' + i) as string}
                style={
                  pageElements?.background === ColorVariants.PRIMARY
                    ? styles.description
                    : styles.descriptionAlt
                }
              >
                {t(description as string)}
              </AppText>
            );
          })}
        {pageElements?.redirects &&
          pageElements.redirects.map((redirect) => {
            if (redirect.buttonType === 'cancel') {
              return (
                <SmallCancelButton
                  key={'button_redirect_cancel_' + redirect.description}
                  border={true}
                  variant={
                    pageElements?.background === ColorVariants.PRIMARY
                      ? ColorVariants.SECONDARY
                      : ColorVariants.PRIMARY
                  }
                  title={redirect.description}
                  cancelFunction={() => navigationFunction(redirect)}
                />
              );
            }
            return (
              <AppConfirmButton
                size={SizeVariants.LARGE}
                variant={
                  pageElements?.background === ColorVariants.PRIMARY
                    ? ColorVariants.DEFAULT
                    : ColorVariants.PRIMARY
                }
                key={'button_redirect_' + redirect.description}
                title={redirect.description}
                confirmFunction={() => navigationFunction(redirect)}
              />
            );
          })}
        {pageElements?.externalRedirects &&
          pageElements.externalRedirects.map((redirect) => {
            if (redirect.buttonType === 'cancel') {
              return (
                <SmallCancelButton
                  key={'button_redirect_cancel_' + redirect.description}
                  border={true}
                  variant={
                    pageElements?.background === ColorVariants.PRIMARY
                      ? ColorVariants.SECONDARY
                      : ColorVariants.PRIMARY
                  }
                  title={redirect.description}
                  cancelFunction={() => externalNavigationFunction(redirect)}
                />
              );
            }
            return (
              <AppConfirmButton
                size={SizeVariants.LARGE}
                variant={
                  pageElements?.background === ColorVariants.PRIMARY
                    ? ColorVariants.DEFAULT
                    : ColorVariants.PRIMARY
                }
                key={'button_redirect_' + redirect.description}
                title={redirect.description}
                confirmFunction={() => externalNavigationFunction(redirect)}
              />
            );
          })}
      </View>
    </View>
  );
};

export default GeneralImagePage;

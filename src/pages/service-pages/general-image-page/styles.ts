import { StyleSheet } from 'react-native';
import { AppColors, globalStyles } from '../../../config/styles/GlobalStyles';

const styles = StyleSheet.create({
  safeWrapper: {
    flex: 1,
    backgroundColor: globalStyles.COLOR_CONFIG.primary,
    height: '100%',
    padding: 50,
  },
  title: {
    marginTop: 40,
    marginBottom: 24,
    fontSize: 24,
    textAlign: 'center',
    color: AppColors.secondary100,
  },
  titleAlt: {
    marginTop: 40,
    marginBottom: 24,
    fontSize: 24,
    textAlign: 'center',
    color: AppColors.primary500,
  },
  subTitle: {
    marginTop: 40,
    marginBottom: 24,
    fontSize: 18,
    textAlign: 'center',
    color: AppColors.secondary100,
  },
  subTitleAlt: {
    marginTop: 40,
    marginBottom: 24,
    //fontWeight: 'bold',
    fontSize: 18,
    textAlign: 'center',
    color: AppColors.primary500,
  },
  description: {
    //fontWeight: '300',
    fontSize: 16,
    textAlign: 'center',
    color: AppColors.secondary100,
  },
  descriptionAlt: {
    //fontWeight: '300',
    fontSize: 16,
    textAlign: 'center',
    color: AppColors.ink900,
  },
  outerContainer: {
    justifyContent: 'center',
    alignContent: 'center',
  },
});

export default styles;

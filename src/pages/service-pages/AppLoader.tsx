import React, { useEffect } from 'react';
import { Dimensions, Platform, View } from 'react-native';
import * as SplashScreen from 'expo-splash-screen';
import ImagesFilesDict from '../../config/assets/images';
import AppImage from '../../components/images/AppImage';

const AppLoader = (): JSX.Element => {
  useEffect(() => {
    if (Platform.OS === 'android') {
      SplashScreen.preventAutoHideAsync();
    }

    return () => {
      if (Platform.OS === 'android') {
        SplashScreen.hideAsync();
      }
    };
  }, []);

  return (
    <View
      style={{
        flex: 1,
        height: Dimensions.get('screen').height,
        backgroundColor: '#ffffff',
      }}
    >
      <AppImage
        //forceSafeImage={true} //HACK we need to use a safe wrapper on android because FastImage loads the image too fast and hides the splashscreen too soon
        //onLoadEnd={hideSplashInLoader}
        resizeMethod="scale"
        resizeMode="contain"
        source={ImagesFilesDict.SplashScreen}
        style={{ height: '100%', width: '100%' }}
      />
    </View>
  );
};

export default AppLoader;

export type AppPermissionError = {
  denied: boolean;
  permanently: boolean;
};

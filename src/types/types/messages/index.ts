import { TranslationKeys } from '../localization';
import { SizeVariants } from '../styles/sizes';
import { AppXOR, VerticalPositioning } from '../utils';

export type MessageTypes = 'error' | 'warning' | 'success' | 'info' | 'quit';

export type MessageTextType = AppXOR<string, TranslationKeys>;

export type SnackBarMessage = {
  header?: string;
  message: MessageTextType;
  optionalMessageParameters?: Record<string, string>;
  type: MessageTypes;
  durationInSeconds?: number;
  size?: SizeVariants;
  position?: VerticalPositioning;
};

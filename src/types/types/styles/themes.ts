export enum ColorVariants {
  DEFAULT = 'default',
  PRIMARY = 'primary',
  SECONDARY = 'secondary',
}

export enum ThemeVariants {
  LIGHT = 'light',
  DARK = 'dark',
  DEFAULT = 'default',
}

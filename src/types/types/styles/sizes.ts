export enum SizeVariants {
  ADAPT = 'adapt',
  MINI = 'mini',
  SMALL = 'small',
  MEDIUM = 'medium',
  LARGE = 'large',
}

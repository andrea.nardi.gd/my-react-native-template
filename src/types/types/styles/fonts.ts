export type FontWeights =
  | '100'
  | '200'
  | '300'
  | '400'
  | '500'
  | '600'
  | '700'
  | '800'
  | '900'
  | 'normal'
  | 'bold';

export type FontStyles = 'normal' | 'italic' | 'default';

export type FontFamilies = 'default' | 'roboto';

enum RobotoFontFamily {
  robotoThin100 = 'robotoThin100',
  robotoThin100Italic = 'robotoThin100Italic',
  robotoExtraLight200 = 'robotoExtraLight200',
  robotoExtraLight200Italic = 'robotoExtraLight200Italic',
  robotoLight300 = 'robotoLight300',
  robotoLight300Italic = 'robotoLight300Italic',
  roboto = 'roboto',
  robotoItalic = 'robotoItalic',
  robotoMedium500 = 'robotoMedium500',
  robotoMedium500Italic = 'robotoMedium500Italic',
  robotoSemiBold600 = 'robotoSemiBold600',
  robotoSemiBold600Italic = 'robotoSemiBold600Italic',
  robotoBold700 = 'robotoBold700',
  robotoBold700Italic = 'robotoBold700Italic',
  robotoExtraBold800 = 'robotoExtraBold800',
  robotoExtraBold800Italic = 'robotoExtraBold800Italic',
  robotoBlack900 = 'robotoBlack900',
  robotoBlack900Italic = 'robotoBlack900Italic',
}

export const AppFonts = { ...RobotoFontFamily };

type AppFontSubDictionaryDef = Record<FontWeights, keyof typeof AppFonts>;

type AppFontSubDictionary = Record<FontStyles, AppFontSubDictionaryDef>;

export type AppFontDictionary = Record<FontFamilies, AppFontSubDictionary>;

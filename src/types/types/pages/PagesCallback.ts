import { StackNavigationHelpers } from '@react-navigation/stack/lib/typescript/src/types';
import { BackHandlerStatic } from 'react-native';
import { AppThunk } from '../action/Action';
import { Navigation } from '../config/navigation';
import { SequenceInfo } from '../navigation/app-page';

export type NavigationCallbackDescriptor = {
  method: PAGES_CALLBACK_PATHS;
  delay?: number;
};

export type ExecuteCallBackActionProps<P, T> = {
  params?: T;
  checkParams?: P;
  callbackInfo: NavigationCallbackDescriptor;
  navigation: StackNavigationHelpers;
};

export type ExecuteScrollCallBackActionProps<P, T> = {
  params?: T;
  checkParams?: P;
  callbackInfo: NavigationCallbackDescriptor;
  loaderStateSetter: (loading: boolean) => void;
};

export enum PAGES_CALLBACK_PATHS {
  DEFAULT = 'DEFAULT',
  TEST_PAGE_CALLBACK = 'TEST_PAGE_CALLBACK',
}

export type PagesCallbackResolverProps<P, T> = {
  navigation: StackNavigationHelpers;
  checkParams?: P;
  params?: T;
};

export type PageScrollCallbackResolverProps<P, T> = {
  loaderStateSetter: (loading: boolean) => void;
  checkParams?: P;
  params?: T;
};

export type PageScrollCallbackResolverFunction = <P, T>({
  ...props
}: PageScrollCallbackResolverProps<P, T>) => AppThunk;

export type PagesCallbackResolverFunction = <P, T>({
  ...props
}: PagesCallbackResolverProps<P, T>) => AppThunk;

export type PageBackButtonHandlerProps = {
  navigation: StackNavigationHelpers;
  navigationInfo: Navigation;
  sequenceInfo: SequenceInfo;
  backButtonCallback: () => void;
  backHandler: BackHandlerStatic;
};

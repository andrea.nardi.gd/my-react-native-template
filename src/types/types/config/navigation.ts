import { ScreenProps } from 'react-native-screens';
import { SECTIONS_LIST, TOP_TAB_PAGES } from '../../../config/navs/NavsConfig';
import { NavigationCallbackDescriptor } from '../pages/PagesCallback';
import { FlowAlteringConditionsKeys, PermissionConditionValues } from './FlowAlteringConditions';

export type Navigation = {
  name: string;
  title?: string;
  showTitle?: boolean;
  showLogo?: boolean;
  isModal?: boolean;
  onFocusCallBack?: NavigationCallbackDescriptor;
  onLeaveCallBack?: NavigationCallbackDescriptor;
  onScrollCallback?: NavigationCallbackDescriptor;
  blockGoBack?: boolean;
  showBackButton?: boolean;
  backButtonlabel?: string;
  backButtonDestination?: string;
  onlineNeeded?: boolean;
  externalLink?: string;
  askConfirmationOnBack?: boolean;
  askConfirmationOnChange?: boolean;
  hasHidingCondition?: boolean;
  section?: SECTIONS_LIST | undefined;
  topNavbarVisible?: boolean;
  tabBarHidden?: boolean;
  component?: string;
  icon?: string;
  enabled: boolean;
  statusBarBgColor?: string;
  pageBackgroundColor?: string;
  noMargin?: boolean;
  noScroll?: boolean;
  animation?: ScreenProps['stackAnimation'];
  isServiceNav?: boolean;
  drawerNavigator?: boolean;
  drawerNavigatorPosition?: 'left' | 'right';
  drawerNavigatorComponent?: string;
  nestedTopNavigatorName?: TOP_TAB_PAGES;
};

export type ShowingCondition = {
  name: FlowAlteringConditionsKeys;
  condition: PermissionConditionValues;
  type: 'number' | 'boolean' | 'string';
};

export type NavigationWithConditions = Navigation & {
  showingConditions?: ShowingCondition[];
};

export type NavigatorParamsList = {
  [key: string]: never;
};

export type NavigationModal = {
  name: string;
  title?: string;
  blockGoBack?: boolean;
  component?: string;
  enabled: boolean;
  isModal: true;
  modalTransition?: 'fade' | 'none' | 'slide';
};

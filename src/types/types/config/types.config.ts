import { AppXOR } from '../utils';

export type XorKVConfig<T> = {
  [key: string]: AppXOR<T, XorKVConfig<T>>;
};

export type AppKVConfig<T> = Record<string, T | Record<string, T>>;

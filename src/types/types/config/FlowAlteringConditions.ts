import { AppXOR } from '../utils';

export enum FlowAlteringConditionsKeys {
  AUTHORIZED = 'AUTHORIZED',
}

export type FunctionPermissionCheck = (val: never, val2: never) => boolean | Promise<boolean>;
export type FunctionPermissionCheckSync = (val: never, val2: never) => boolean;

export type NumberConditions = 'equal' | 'lower' | 'greater';

export type PermissionConditionsTypes = 'number' | 'boolean' | 'string' | 'function';

export type SimplePermissionValues = boolean | string | number;
export type ComplexPermissionValues = { target: number; condition: NumberConditions };

export type PermissionConditionValues = AppXOR<
  SimplePermissionValues,
  ComplexPermissionValues | FunctionPermissionCheck
>;

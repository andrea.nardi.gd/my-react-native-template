import { ImageSourcePropType } from 'react-native';

export type Slide = {
  image: string | ImageSourcePropType;
  title: string;
  description: string;
};

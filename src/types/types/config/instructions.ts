import UI_Icons from '../../../components/icons';

export type StepInstructionObj = {
  image?: keyof typeof UI_Icons.INSTRUCTIONS | string;
  description: string;
};

export type Instruction = {
  title: string;
  subtitle?: string;
  steps: StepInstructionObj[];
};

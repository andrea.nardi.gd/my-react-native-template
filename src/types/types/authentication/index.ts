export enum AppAuthTypes {
  PIN,
  FINGERPRINT,
  FACIAL_RECOGNITION,
  PASSWORD,
}

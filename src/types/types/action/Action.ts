import { Action, AnyAction, ThunkAction } from '@reduxjs/toolkit';
import { RootState } from '../../../reducers';
import { AppXOR } from '../../types/utils';

export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;

export type AppAction<T> = AppXOR<T, AnyAction>;

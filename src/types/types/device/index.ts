import { DeviceType, osName } from 'expo-device';

export type AppDeviceType = keyof typeof DeviceType;
export type AppDeviceOS = typeof osName;

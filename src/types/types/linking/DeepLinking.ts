import { StackNavigationHelpers } from '@react-navigation/stack/lib/typescript/src/types';
import { ParsedURL } from 'expo-linking';
import { AppThunk } from '../action/Action';

export type ExecuteDeepLinkActionProps<T> = {
  checkParams?: T;
  url: ParsedURL;
  navigation: StackNavigationHelpers;
};

export enum LINKING_PATHS {
  TEST_UNAUTH = 'test-unauth',
  TEST_AUTH = 'test-auth',
  TEST_MIDDLEWARE = 'test-middleware',
  DEFAULT = 'DEFAULT',
}

export type DeepLinkingResolverProps<P, T> = {
  navigation: StackNavigationHelpers;
  checkParams?: T;
  params: P | null;
};

export type DeepLinkingResolverFunction = <P, T>({
  ...props
}: DeepLinkingResolverProps<P, T>) => AppThunk;

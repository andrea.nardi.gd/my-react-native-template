type Without<T, U> = { [P in Exclude<keyof T, keyof U>]?: never };
export type AppXOR<T, U> = T | U extends object ? (Without<T, U> & U) | (Without<U, T> & T) : T | U;
export type ArrayElement<A> = A extends readonly (infer T)[] ? T : never;

export type VerticalPositioning = 'top' | 'bottom';
export type AppSizes = 'small' | 'medium' | 'large' | 'full';

enum AppPermissionStatus {
  IMPOSSIBLE = 'impossible',
}

export type ExtendedAppPermissionStatusT = AppPermissionStatus | PermissionStatus;

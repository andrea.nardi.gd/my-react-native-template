export type Ticket = {
  id: string;
  name: string;
  date: string;
  time: string;
  qrSrc: string;
  showQr: boolean;
  showPin: boolean;
  email: string;
  cancelTicketUrl: string | null;
  status: 'active' | 'pending' | string;
  numberOfTickets: number;
  details: [
    {
      number: number;
      type: string;
    },
  ];
  hidden: boolean;
};

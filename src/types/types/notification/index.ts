export enum PushNotificationTypes {
  TEST_PUSH_NOTIFICATION = 'testPushNotification',
}

type NotificationDataPayload = object; //TODO describe notification payload

export type AppNotificationDataPayload = NotificationDataPayload;

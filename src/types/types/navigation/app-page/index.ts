import { Navigation, NavigationModal } from '../../config/navigation';
import { SequenceStep } from '../sequence/sequence';

export enum PAGES_DICTIONARIES {
  UNAUTHORIZED_PAGES = 'UNAUTHORIZED_PAGES',
  AUTHORIZED_PAGES = 'AUTHORIZED_PAGES',
  SEQUENCE_PAGES = 'SEQUENCE_PAGES',
  MIDDLEWARE_PAGES = 'MIDDLEWARE_PAGES',
  SERVICE_PAGES = 'SERVICE_PAGES',
}

export interface SequenceInfo {
  currentStep: number;
  steps: SequenceStep[];
  config: SequenceStep;
}

export interface AppPageParamsProps {
  insetTop?: number;
  insetBottom?: number;
  navigationInfo?: Navigation;
  sequenceInfo?: SequenceInfo;
  pageDictionary: PAGES_DICTIONARIES;
}

export interface AppModalParamsProp {
  navigationInfo?: NavigationModal;
}

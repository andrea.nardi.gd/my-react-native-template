import { NavigationWithConditions } from '../../config/navigation';

export type SequenceStep = NavigationWithConditions & {
  orderInSequence: number;
  label: string;
  goBack?: {
    behavior: StepsBehavior;
    redirect?: string;
    cleanData?: boolean;
  };
  error?: {
    behavior: StepsBehavior;
    cleanData?: boolean;
    redirect?: string;
  };
  warning?: {
    behavior: StepsBehavior;
    cleanData?: boolean;
    redirect?: string;
  };
  goForward?: {
    behavior: StepsBehavior;
    redirect?: string;
  };
};

export enum StepsBehavior {
  STAY,
  REDIRECT,
  GO_BACK,
  GO_FORWARD,
  RESTART_SEQUENCE,
  GO_SEQUENCE_INDEX,
}

export type SequenceConfig = {
  sequenceName: string;
  config: {
    showProgress?: boolean;
    progressDotNavigation?: boolean;
  };
  steps: SequenceStep[];
};

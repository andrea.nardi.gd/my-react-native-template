import { RouteProp } from '@react-navigation/native';
import { AppModalParamsProp, AppPageParamsProps } from '../app-page';
import { ImagePageRouteProps } from './general-image-page';

export enum CustomParamsRouteKeys {
  APP_PAGE = 'APP_PAGE',
  APP_MODAL = 'APP_MODAL',
  GENERAL_IMAGE_PAGE = 'GENERAL_IMAGE_PAGE',
}

export type CustomParamsNavigatorList = {
  [CustomParamsRouteKeys.APP_PAGE]: AppPageParamsProps;
  [CustomParamsRouteKeys.APP_MODAL]: AppModalParamsProp;
  [CustomParamsRouteKeys.GENERAL_IMAGE_PAGE]: ImagePageRouteProps;
};

export type CustomParamNavigationProp<K extends CustomParamsRouteKeys> = RouteProp<
  CustomParamsNavigatorList,
  K
>;

import { Redirect } from '../../../../../config/navs/ImagePages';
import { IMAGE_PAGES } from '../../../../../config/navs/NavsConfig';
import { ColorVariants } from '../../../styles/themes';

export interface ImagePageRouteProps {
  image?: IMAGE_PAGES | null;
  background: ColorVariants;
  title: string;
  subtitles?: string;
  descriptions?: string[];
  redirects?: Redirect[];
  goForward?: string;
  goForwardParams?: never;
}

import { Navigation } from '../../config/navigation';

export type TopTabOptions = {
  titleBackgroundColor?: string;
  titleTextColor?: string;
  showIcon?: boolean;
  showText?: boolean;
  textSize?: number;
  iconSize?: number;
  activeIndicatorHeight?: number;
  activeTabUnderlineColor?: string;
  useLazy?: boolean;
  lazyPreloadDistance?: number;
  firstScreen?: number;
  clearStackOnLeave?: boolean;
};

export type TopTabRoute = {
  key: string;
  title: string;
  icon: string;
  component: string;
  navigationInfo?: Navigation;
};

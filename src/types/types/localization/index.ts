import defaultLocalization from '../../../config/locale/defaultTranslation.json';

export enum LocalizationLang {
  EN = 'en',
  IT = 'it',
}

export enum LocalizationCurr {
  EUR = 'EUR',
  USD = 'USD',
}

export type TranslationKeys = keyof typeof defaultLocalization;

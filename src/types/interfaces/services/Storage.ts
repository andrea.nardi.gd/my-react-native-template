export type OperationSafetyType = 'safe' | 'unsafe';

export interface BaseAppStorage {
  get: (key: string, safety?: OperationSafetyType) => Promise<string | null>;
  set: <T>(key: string, value: T, safety?: OperationSafetyType) => Promise<void>;
  delete: (key: string, safety?: OperationSafetyType) => Promise<void>;
  clear: (safety?: OperationSafetyType) => Promise<void>;
  clearWithExceptions: (preserveKeys?: string[], safety?: OperationSafetyType) => Promise<void>;
}

export interface AppStorage extends BaseAppStorage {
  enclaveStorage: BaseAppStorage;
  storage: BaseAppStorage;
}

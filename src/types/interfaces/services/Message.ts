import { MessageTextType, MessageTypes, SnackBarMessage } from '../../types/messages';

export interface EmitHandledMessage {
  message: MessageTextType;
  optionalTranslationParams?: Record<string, string>;
  type?: MessageTypes;
  overrideMessageOptions?: Partial<SnackBarMessage>;
  allowUnsupportedTranslation?: boolean;
}

export interface EmitUnhandledMessage {
  e: unknown;
  overrideMessage?: MessageTextType;
  overrideKeyErrorExtractor?: string;
  fallBackMessage?: MessageTextType;
  optionalTranslationParams?: Record<string, string>;
  overrideMessageOptions?: Partial<SnackBarMessage>;
  showUser: boolean;
  allowUnsupportedTranslation?: boolean;
}

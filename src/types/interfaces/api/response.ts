export type AppResponseError = {
  status: 'error';
  message: string;
};

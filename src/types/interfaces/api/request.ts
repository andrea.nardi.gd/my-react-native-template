import { AppXOR } from '../../types/utils';

export enum AppMethods {
  CONNECT = 'CONNECT',
  DELETE = 'DELETE',
  GET = 'GET',
  HEAD = 'HEAD',
  OPTIONS = 'OPTIONS',
  PATCH = 'PATCH',
  POST = 'POST',
  PUT = 'PUT',
  TRACE = 'TRACE',
}

export enum AppHosts {
  DEFAULT = 'APP_SERVER_HOST',
}

export type AppBaseRequest = {
  method: AppMethods;
  url: string;
  useAuthentication?: boolean;
};

interface HeadersType {
  Authorization: string;
  api?: boolean;
}

type AppRequestAuthorized<T> = AppBaseRequest & {
  data?: T | T[] | null;
  params?: T | T[] | null;
  withCredentials?: boolean;
  headers?: HeadersType;
};

type AppRequestUnAuthorized<T> = AppBaseRequest & {
  data?: T | T[] | null;
  params?: T | T[] | null;
};

export type AppRequest<T> = AppXOR<AppRequestAuthorized<T>, AppRequestUnAuthorized<T>>;

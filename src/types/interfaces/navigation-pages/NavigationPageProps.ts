import { NativeStackNavigationProp } from '@react-navigation/native-stack';
import { Navigation, NavigatorParamsList } from '../../../types/types/config/navigation';
import { SequenceStep } from '../../types/navigation/sequence/sequence';
import { ParamListBase, RouteProp } from '@react-navigation/native';
import {
  CustomParamNavigationProp,
  CustomParamsRouteKeys,
} from '../../types/navigation/routes/CustomParamsRoutesList';

export interface CustomRoutePageProps<K extends CustomParamsRouteKeys> {
  navigation?: NativeStackNavigationProp<NavigatorParamsList>;
  navProperties?: Navigation;
  externalLinkKey?: string;
  sequenceInfo?: SequenceStep[];
  sequenceStep?: SequenceStep;
  route?: CustomParamNavigationProp<K>;
}

export interface NavigationPageProps {
  navigation?: NativeStackNavigationProp<NavigatorParamsList>;
  navProperties?: Navigation;
  externalLinkKey?: string;
  sequenceInfo?: SequenceStep[];
  sequenceStep?: SequenceStep;
  route?: RouteProp<ParamListBase, string>;
}

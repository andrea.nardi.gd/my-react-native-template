import { StyleSheet, View } from 'react-native';
import React from 'react';
import AppText from '../../../layout/text/AppText';
import { useAppSelector } from '../../../../store/hooks';

interface SettingsButtonTextProps {
  title?: string;
  description?: string;
}

const SettingsButtonText = ({ title, description }: SettingsButtonTextProps): JSX.Element => {
  const AppColors = useAppSelector((store) => store.theme.AppColors);
  return (
    <View style={{ flexShrink: 1, marginRight: 10 }}>
      {title && (
        <AppText
          lineBreakMode="clip"
          fontWeight="bold"
          style={[styles.settingButtonTitle, { color: AppColors.primary500 }]}
          textBreakStrategy="highQuality"
        >
          {title}
        </AppText>
      )}
      {description && (
        <AppText
          lineBreakMode="clip"
          style={[styles.settingButtonDescription, { color: AppColors.primary500 }]}
          textBreakStrategy="highQuality"
          fontWeight="300"
        >
          {description}
        </AppText>
      )}
    </View>
  );
};

export default SettingsButtonText;

const styles = StyleSheet.create({
  settingButtonTitle: { fontSize: 14 },
  settingButtonDescription: { fontSize: 12 },
});

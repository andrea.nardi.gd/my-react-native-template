import { StyleSheet, View } from 'react-native';
import React, { useCallback } from 'react';
import ToggleField from '../../forms/ToggleFields';

type SettingsToggleButtonProps = {
  callback: (...params: unknown[]) => unknown;
  enabled?: boolean;
  toggleValue?: boolean;
};

export default function SettingsToggleButton({
  toggleValue,
  callback,
  enabled,
}: SettingsToggleButtonProps): JSX.Element {
  const toggleCallback = useCallback(() => {
    if (enabled) {
      callback();
    }
  }, [callback, enabled]);

  return (
    <View style={styles.settingsToggleButtonContainer}>
      <ToggleField toggleValue={toggleValue || false} editFunction={toggleCallback} />
    </View>
  );
}

const styles = StyleSheet.create({
  settingsToggleButtonContainer: {
    alignSelf: 'center',
    marginLeft: 'auto',
  },
});

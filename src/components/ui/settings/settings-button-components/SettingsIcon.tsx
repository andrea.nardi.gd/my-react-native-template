import { StyleSheet, View } from 'react-native';
import UI_Icons from '../../../icons';
import { useAppSelector } from '../../../../store/hooks';
import { globalStyles } from '../../../../config/styles/GlobalStyles';
import CircularIconWrapper from '../../../wrappers/CircularIconWrapper';
import React, { useMemo } from 'react';
import AppText from '../../../layout/text/AppText';

export default function SettingsIcon({
  iconDictionary = UI_Icons.SETTINGS,
  icon,
  iconSize = globalStyles.ICONS_SETTINGS.DEFAULT_SETTINGS_BUTTON_SIZE,
  title,
  showIconLabel = false,
}: {
  iconDictionary?: Record<string, (color: string, size?: number) => JSX.Element>;
  icon: keyof typeof iconDictionary | React.ReactNode;
  iconSize?: number;
  title?: string;
  showIconLabel: boolean;
}): JSX.Element {
  const AppColors = useAppSelector((store) => store.theme.AppColors);
  const iconComponent = useMemo(() => {
    console.log('Icon dictionary =>', iconDictionary, icon);
    if (typeof icon === 'string') {
      if (icon && iconDictionary[icon]) {
        return iconDictionary[icon](AppColors.primary500, iconSize);
      }
      return <AppText>?</AppText>;
    }
    return icon;
  }, [icon, iconSize, AppColors, iconDictionary]);
  return (
    <View style={[styles.settingIconContainerStyle]}>
      <CircularIconWrapper
        icon={iconComponent}
        size={iconSize}
        wrapperColor={AppColors.primary100}
        label={showIconLabel ? title : undefined}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  settingIconContainerStyle: {
    display: 'flex',
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
    marginLeft: 5,
    marginRight: 10,
  },
});

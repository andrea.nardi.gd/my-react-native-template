import React from 'react';
import { useTranslation } from 'react-i18next';
import { Pressable, StyleSheet } from 'react-native';
import AppText from '../../layout/text/AppText';
import SettingsButtonText from './settings-button-components/SettingsButtonText';
import SettingsIcon from './settings-button-components/SettingsIcon';
import SettingsToggleButton from './settings-button-components/SettingsToggleButton';

export enum CallbackActivator {
  BUTTON_PRESSION = 'BUTTON_PRESSION',
  TOGGLE_PRESSION = 'TOGGLE_PRESSION',
}

type SettingButtonProp<T> = {
  buttonActivator: CallbackActivator;
  callback: (...params: unknown[]) => unknown;
  iconSize?: number;
  title: string;
  description?: string;
  enabled?: boolean;
  additionalFieldValue?: T;
  toggleValue?: boolean;
  showIconLabel?: boolean;
};

const SettingButton = <T, K extends string>({
  buttonActivator,
  callback,
  icon,
  iconSize = 20,
  iconDictionary,
  title,
  description,
  additionalFieldValue,
  toggleValue,
  enabled = true,
  showIconLabel = false,
}: SettingButtonProp<T> & {
  iconDictionary?: Record<K, (color: string, size?: number) => JSX.Element>;
  icon?: keyof typeof iconDictionary | React.ReactNode;
}): JSX.Element => {
  const { t } = useTranslation();

  const onPressHandler = (): void => {
    if (enabled) {
      callback();
    }
  };

  return (
    <Pressable
      onPress={onPressHandler}
      style={[
        styles.SettingButtonContainer,
        {
          opacity: enabled ? 1 : 0.3,
        },
      ]}
    >
      {icon && (
        <SettingsIcon
          icon={icon}
          iconSize={iconSize}
          iconDictionary={iconDictionary}
          title={title}
          showIconLabel={showIconLabel}
        />
      )}
      <SettingsButtonText title={title} description={description} />
      {additionalFieldValue && buttonActivator !== CallbackActivator.TOGGLE_PRESSION && (
        <AppText
          style={{
            alignSelf: 'center',
            marginLeft: 'auto',
            fontSize: 16,
            lineHeight: 24,
            marginRight: 20,
          }}
        >
          {t(additionalFieldValue as unknown as string)}
        </AppText>
      )}
      {buttonActivator === CallbackActivator.TOGGLE_PRESSION && !additionalFieldValue && (
        <SettingsToggleButton callback={callback} enabled={enabled} toggleValue={toggleValue} />
      )}
    </Pressable>
  );
};

export default SettingButton;

const styles = StyleSheet.create({
  SettingButtonContainer: {
    flex: 1,
    padding: 10,
    flexDirection: 'row',
    alignItems: 'center',
  },
});

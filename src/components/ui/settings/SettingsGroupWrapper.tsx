import { isArray } from 'lodash';
import React, { useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import { StyleSheet, TextStyle, View, ViewStyle } from 'react-native';
import { useAppSelector } from '../../../store/hooks';
import AppText from '../../layout/text/AppText';

interface SettingsGroupWrapper {
  children: JSX.Element[] | JSX.Element;
  entriesWrapperStyle?: ViewStyle | ViewStyle[];
  labelStyle?: TextStyle | TextStyle[];
  isTransparent?: boolean;
  label?: string;
}

const SettingsGroupWrapper = ({
  children,
  label,
  entriesWrapperStyle,
  isTransparent,
  labelStyle,
}: SettingsGroupWrapper): JSX.Element => {
  const { t } = useTranslation();
  const AppColors = useAppSelector((store) => store.theme.AppColors);

  const wrapperStyle = useMemo(() => {
    let temp: ViewStyle[] = [];
    if (entriesWrapperStyle) {
      if (isArray(entriesWrapperStyle)) {
        temp = entriesWrapperStyle;
      } else {
        temp.push(entriesWrapperStyle);
      }
    } else {
      temp.push({ backgroundColor: AppColors.secondary100 });
      temp.push(styles.defaultOptionsWrapper);
    }

    if (isTransparent) {
      temp.push({ backgroundColor: 'transparent' });
    }

    return temp;
  }, [entriesWrapperStyle, isTransparent, AppColors]);

  const labelStyles = useMemo(() => {
    let temp: TextStyle[] = [];
    if (labelStyle) {
      if (isArray(labelStyle)) {
        temp = labelStyle;
      } else {
        temp.push(labelStyle);
      }
    } else {
      temp.push({ color: AppColors.primary500 });
    }
    return temp;
  }, [labelStyle, AppColors]);

  return (
    <View style={{ width: '100%' }}>
      {label && (
        <AppText fontWeight="bold" style={[...labelStyles, styles.defaultLabelStyle]}>
          {t(label) || ''}
        </AppText>
      )}
      <View style={wrapperStyle}>{children}</View>
    </View>
  );
};

export default SettingsGroupWrapper;

const styles = StyleSheet.create({
  defaultOptionsWrapper: {
    flex: 1,
    margin: 5,
    borderRadius: 5,
  },

  defaultLabelStyle: {
    fontSize: 16,
    //padding: 5,
    paddingBottom: 5,
    textTransform: 'capitalize',
  },
});

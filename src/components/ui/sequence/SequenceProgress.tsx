import React, { useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { LayoutRectangle, ScrollView, View } from 'react-native';
import { useAppSelector } from '../../../store/hooks';
import { SequenceStep } from '../../../types/types/navigation/sequence/sequence';
import StepPoint from '../../icons/sequence/StepPoint';
import AppText from '../../layout/text/AppText';

//TODO define styles elsewhere

const ICON_SIZE = 28;

export interface SequenceProgressProps {
  currentStep: number;
  stepLabels?: SequenceStep[];
  forceHideOnCreation?: boolean;
}

const StepLine = ({ done }: { done: boolean }): JSX.Element => {
  const AppColors = useAppSelector((store) => store.theme.AppColors);

  return (
    <View
      style={{
        width: ICON_SIZE * 4,
        height: 1,
        backgroundColor: done ? AppColors.primary500 : AppColors.ink500,
        alignSelf: 'center',
      }}
    />
  );
};

const getStepLabel = (stepLabels: SequenceStep[] | undefined, index: number): string => {
  if (stepLabels && stepLabels.length >= index - 1) {
    return stepLabels[index].label || '';
  }

  return '';
};

const SequenceProgress = ({
  currentStep,
  stepLabels,
  forceHideOnCreation = false, //We really never need this, set it to true only if you have a sequence with a lot (> 50) of steps
}: SequenceProgressProps): JSX.Element => {
  const AppColors = useAppSelector((store) => store.theme.AppColors);

  const [scrollPosition, setScrollPosition] = useState<number>(0);
  const [visible, setVisible] = useState<boolean>(!forceHideOnCreation); //NOTE maybe set this to false on same page transition change, but it shouldn't be necessary unless passing a list with a huge number of steps
  const [scrollRef, setScrollRef] = useState<ScrollView | null>(null);
  const totalSteps = stepLabels?.length || 0;
  const { t } = useTranslation();

  const incrementScrollPosition = (layout: LayoutRectangle) => {
    const { x } = layout;
    setScrollPosition(scrollPosition + x);
  };

  const scrollToPos = (layout: LayoutRectangle) => {
    if (scrollRef) {
      scrollRef.scrollTo({ x: scrollPosition - layout.width * 0.4, animated: false });
      setVisible(true); //NOTE only useful on same page transition change (extremely rare, needs sequences with more than 50 steps or so)
    }
  };

  const [StepLabels, StepElements] = useMemo(() => {
    const tempLabels = [];
    const tempElements = [];

    for (let i = 0; i < totalSteps; i++) {
      tempLabels.push(
        <AppText
          key={'stepLabel_' + i}
          fontWeight={i === currentStep ? '600' : 'normal'}
          style={{
            padding: ICON_SIZE / 8,
            width: ICON_SIZE * 4,
            textAlign: 'center',
            color: i > currentStep ? AppColors.ink500 : AppColors.primary500,
            marginRight: i === totalSteps - 1 ? ICON_SIZE / 4 : 'auto',
            marginLeft: i === 0 ? ICON_SIZE / 4 : 'auto',
          }}
        >
          {t(getStepLabel(stepLabels, i))}
        </AppText>,
      );

      if (i < currentStep) {
        tempElements.push(
          <View
            key={'step_' + i}
            style={{
              paddingLeft: i === 0 ? ICON_SIZE * 2 : 0,
              paddingRight: i === totalSteps - 1 ? ICON_SIZE * 2 : 0,
            }}
          >
            <StepPoint done={true} size={ICON_SIZE} number={i + 1} />
          </View>,
        );
        if (i !== totalSteps - 1)
          tempElements.push(<StepLine key={'step_divider_' + i} done={true} />);
        continue;
      }

      if (i > currentStep) {
        tempElements.push(
          <View
            key={'step_' + i}
            style={{
              paddingLeft: i === 0 ? ICON_SIZE * 2 : 0,
              paddingRight: i === totalSteps - 1 ? ICON_SIZE * 2 : 0,
            }}
          >
            <StepPoint size={ICON_SIZE} number={i + 1} />
          </View>,
        );
        if (i !== totalSteps - 1)
          tempElements.push(<StepLine key={'step_divider_' + i} done={false} />);
        continue;
      }

      tempElements.push(
        <View
          key={'step_' + i}
          onLayout={(e) => incrementScrollPosition(e.nativeEvent.layout)}
          style={{
            paddingLeft: i === 0 ? ICON_SIZE * 2 : 0,
            paddingRight: i === totalSteps - 1 ? ICON_SIZE * 2 : 0,
          }}
        >
          <StepPoint size={ICON_SIZE} active={true} number={i + 1} />
        </View>,
      );
      if (i !== totalSteps - 1)
        tempElements.push(<StepLine key={'step_divider_' + i} done={false} />);
    }

    return [tempLabels, tempElements];
  }, [currentStep, stepLabels]);

  return (
    <View
      style={{
        marginTop: ICON_SIZE / 2,
        display: 'flex',
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center',
      }}
    >
      {visible && (
        <ScrollView
          showsHorizontalScrollIndicator={false}
          onLayout={(e) => scrollToPos(e.nativeEvent.layout)}
          ref={(ref) => setScrollRef(ref)}
          horizontal
        >
          <View>
            <View
              style={{
                width: '100%',
                display: 'flex',
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center',
              }}
            >
              {StepElements.map((element) => element)}
            </View>
            <View
              style={{
                width: '100%',
                display: 'flex',
                flexDirection: 'row',
                marginTop: ICON_SIZE / 4,
                justifyContent: 'space-around',
              }}
            >
              {StepLabels.map((label) => label)}
            </View>
          </View>
        </ScrollView>
      )}
    </View>
  );
};

export default SequenceProgress;

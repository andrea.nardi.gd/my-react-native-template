import React, { useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Pressable } from 'react-native';
import { useAppSelector } from '../../../store/hooks';
import AppText from '../../layout/text/AppText';

interface SmallConfirmButtonProps {
  confirmFunction: () => void;
  title: string;
  enabled?: boolean;
}

const SmallConfirmButton = ({
  confirmFunction,
  title,
  enabled = true,
}: SmallConfirmButtonProps): JSX.Element => {
  const AppColors = useAppSelector((store) => store.theme.AppColors);

  const { t } = useTranslation();
  const [isEnabled, setIsEnabled] = useState<boolean>(true);
  const timeoutRef = useRef<NodeJS.Timeout>();

  useEffect(() => {
    setIsEnabled(enabled);
  }, [enabled]);

  useEffect(() => {
    return () => {
      setIsEnabled(true);
      clearInterval(timeoutRef.current);
    };
  }, []);

  const onConfirm = async () => {
    if (isEnabled) {
      setIsEnabled(false);
      confirmFunction();
      const timeoutId = setTimeout(() => {
        setIsEnabled(true);
      }, 2000);
      timeoutRef.current = timeoutId;
    }
  };

  return (
    <Pressable
      style={{
        opacity: isEnabled ? 1 : 0.2,
        borderRadius: 15,
        backgroundColor: AppColors.primary500,
        justifyContent: 'center',
        marginLeft: 15,
        marginRight: 15,
        padding: 8,
        //width: '15%',
      }}
      onPress={onConfirm}
    >
      <AppText
        fontWeight="600"
        style={{
          textAlign: 'center',
          color: AppColors.secondary100,
        }}
      >
        {t(title)}
      </AppText>
    </Pressable>
  );
};

export default SmallConfirmButton;

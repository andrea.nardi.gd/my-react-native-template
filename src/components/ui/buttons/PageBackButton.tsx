import { useNavigation } from '@react-navigation/native';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { Pressable, View } from 'react-native';
import { useAppSelector } from '../../../store/hooks';
import { ThemeVariants } from '../../../types/types/styles/themes';
import UI_Icons from '../../icons';
import AppText from '../../layout/text/AppText';

interface HeaderBackButtonProps {
  textKey?: string;
  destination?: string;
  variant?: ThemeVariants;
}

const PageBackButton = ({
  textKey = 'back',
  destination,
  variant = ThemeVariants.DEFAULT,
}: HeaderBackButtonProps): JSX.Element => {
  const AppColors = useAppSelector((store) => store.theme.AppColors);

  const navigation = useNavigation();
  const { t } = useTranslation();

  const onBack = () => {
    if (destination) {
      navigation?.navigate(destination as never);
    } else {
      navigation?.goBack();
    }
  };
  return (
    <Pressable
      onPress={onBack}
      style={{
        display: 'flex',
        flexDirection: 'row',
        padding: 10,
        paddingLeft: 0,
        width: 150,
      }}
    >
      <View style={{ marginLeft: 9, alignSelf: 'center' }}>
        {UI_Icons.HEADER.BACK_BUTTON(
          variant === ThemeVariants.DEFAULT ? AppColors.ink900 : AppColors.secondary100,
          16,
        )}
      </View>
      <AppText
        fontWeight="bold"
        style={{
          alignSelf: 'center',
          marginLeft: 9,
          color: variant === ThemeVariants.DEFAULT ? AppColors.ink900 : AppColors.secondary100,
        }}
      >
        {t(textKey)}
      </AppText>
    </Pressable>
  );
};

export default PageBackButton;

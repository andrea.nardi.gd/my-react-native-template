import React, { useState, useEffect, useMemo, useRef } from 'react';
import { useTranslation } from 'react-i18next';
import { Pressable } from 'react-native';
import { useAppSelector } from '../../../store/hooks';
import { SizeVariants } from '../../../types/types/styles/sizes';
import { ColorVariants } from '../../../types/types/styles/themes';
import AppText from '../../layout/text/AppText';

interface AppConfirmButtonProps {
  confirmFunction: () => void;
  title: string;
  enabled?: boolean;
  variant?: ColorVariants;
  size?: SizeVariants.SMALL | SizeVariants.MEDIUM | SizeVariants.LARGE;
  timeoutInSec?: number;
}

const AppConfirmButton = ({
  confirmFunction,
  title,
  enabled = true,
  size = SizeVariants.MEDIUM,
  variant = ColorVariants.DEFAULT,
  timeoutInSec = 2,
}: AppConfirmButtonProps): JSX.Element => {
  const AppColors = useAppSelector((store) => store.theme.AppColors);

  const { t } = useTranslation();
  const [isEnabled, setIsEnabled] = useState<boolean>(true);
  const timeoutRef = useRef<NodeJS.Timeout>();

  useEffect(() => {
    setIsEnabled(enabled);
  }, [enabled]);

  useEffect(() => {
    return () => {
      setIsEnabled(true);
      clearInterval(timeoutRef.current);
    };
  }, []);

  const onConfirm = async () => {
    if (isEnabled) {
      setIsEnabled(false);
      confirmFunction();
      const timeoutId = setTimeout(() => {
        setIsEnabled(true);
      }, timeoutInSec * 1000);
      timeoutRef.current = timeoutId;
    }
  };

  const buttonSize = useMemo(() => {
    switch (size) {
      case SizeVariants.MEDIUM:
      default:
        return '66%';
      case SizeVariants.LARGE:
        return '92%';
      case SizeVariants.SMALL:
        return '33%';
    }
  }, [size]);

  return (
    <Pressable
      style={{
        borderRadius: 15,
        backgroundColor:
          variant === ColorVariants.DEFAULT
            ? AppColors.ink900
            : variant === ColorVariants.SECONDARY
            ? AppColors.secondary100
            : AppColors.primary500,
        borderWidth: 0.1,
        width: buttonSize,
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center',
        opacity: isEnabled ? 1 : 0.3,
        margin: 15,
        marginTop: 30,
        marginBottom: 10,
        padding: 8,
      }}
      onPress={onConfirm}
    >
      <AppText
        fontWeight="600"
        style={{
          textAlign: 'center',
          color:
            variant === ColorVariants.SECONDARY ? AppColors.primary500 : AppColors.secondary100,
        }}
      >
        {t(title)}
      </AppText>
    </Pressable>
  );
};

export default AppConfirmButton;

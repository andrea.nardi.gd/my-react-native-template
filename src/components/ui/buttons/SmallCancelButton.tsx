import React, { useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Pressable } from 'react-native';
import { useAppSelector } from '../../../store/hooks';
import { SizeVariants } from '../../../types/types/styles/sizes';
import { ColorVariants } from '../../../types/types/styles/themes';
import AppText from '../../layout/text/AppText';

interface SmallCancelButtonProps {
  border?: boolean;
  cancelFunction: () => void;
  title: string;
  enabled?: boolean;
  variant?: ColorVariants;
  size?: SizeVariants;
}

const SmallCancelButton = ({
  cancelFunction,
  title,
  border = false,
  enabled = true,
  variant = ColorVariants.PRIMARY,
  size = SizeVariants.SMALL,
}: SmallCancelButtonProps): JSX.Element => {
  const AppColors = useAppSelector((store) => store.theme.AppColors);

  const { t } = useTranslation();

  const [isEnabled, setIsEnabled] = useState<boolean>(true);
  const timeoutRef = useRef<NodeJS.Timeout>();

  useEffect(() => {
    setIsEnabled(enabled);
  }, [enabled]);

  useEffect(() => {
    return () => {
      setIsEnabled(true);
      clearInterval(timeoutRef.current);
    };
  }, []);

  const onCancel = async () => {
    if (isEnabled) {
      setIsEnabled(false);
      cancelFunction();
      const timeoutId = setTimeout(() => {
        setIsEnabled(true);
      }, 2000);
      timeoutRef.current = timeoutId;
    }
  };

  const chooseSize = (): string => {
    switch (size) {
      case SizeVariants.MEDIUM:
      default:
        return '66%';
      case SizeVariants.LARGE:
        return '90%';
      case SizeVariants.SMALL:
        return '33%';
      case SizeVariants.MINI:
        return '15%';
    }
  };

  return (
    <Pressable
      style={{
        ...(size !== SizeVariants.ADAPT && { width: chooseSize() }),
        borderRadius: 15,
        backgroundColor: 'transparent',
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center',
        marginLeft: 15,
        marginRight: 15,
        opacity: isEnabled ? 1 : 0.3,
        padding: 8,
        borderWidth: border ? 1 : 0,
        borderColor:
          variant === ColorVariants.PRIMARY ? AppColors.primary500 : AppColors.secondary100,
      }}
      onPress={onCancel}
    >
      <AppText
        fontWeight="600"
        style={{
          textAlign: 'center',
          color: variant === ColorVariants.PRIMARY ? AppColors.primary500 : AppColors.secondary100,
        }}
      >
        {t(title)}
      </AppText>
    </Pressable>
  );
};

export default SmallCancelButton;

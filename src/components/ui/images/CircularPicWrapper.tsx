import React from 'react';
import { ImageStyle, StyleProp, View } from 'react-native';

import { useAppSelector } from '../../../store/hooks';
import UI_Icons from '../../icons';
import AppImage from '../../images/AppImage';

interface CircularPicWrapperProps {
  src?: string | null;
  containerStyle?: StyleProp<ImageStyle>;
  height: number;
  width: number;
  defaultToText?: boolean;
}

const CircularPicWrapper = ({
  src,
  containerStyle,
  height,
  width,
}: CircularPicWrapperProps): JSX.Element => {
  const AppColors = useAppSelector((store) => store.theme.AppColors);

  if (src) {
    return (
      <View style={containerStyle || {}}>
        <AppImage
          source={{
            uri: src,
          }}
          style={{ height: height, width: width }}
          height={height}
          width={width}
        />
      </View>
    );
  } else {
    return (
      <View
        style={{
          borderRadius: height,
          height: height,
          width: width,
          backgroundColor: AppColors.ink500,
          alignContent: 'center',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        {UI_Icons.SETTINGS.EMPTY_CIRCULAR_IMAGE_PLACEHOLDER(AppColors.secondary100, height / 2)}
      </View>
    );
  }
};

export default CircularPicWrapper;

import React from 'react';
import { useTranslation } from 'react-i18next';
import { View } from 'react-native';
import { useAppSelector } from '../../../store/hooks';
import { MessageTypes } from '../../../types/types/messages';
import UI_Icons from '../../icons';
import AppText from '../../layout/text/AppText';
import { SnackMessageStyle } from './styles';

interface AppAlertProps {
  type: MessageTypes;
  message: string;
  closeFunction?: (() => void) | null;
}

const IconSelector = ({ type }: { type: MessageTypes }): JSX.Element => {
  const AppColors = useAppSelector((store) => store.theme.AppColors);

  switch (type) {
    case 'error':
    default:
      return UI_Icons.MESSAGES.ERROR(AppColors.secondary900, 24);
    case 'info':
      return UI_Icons.MESSAGES.INFO(AppColors.secondary900, 24);
    case 'success':
      return UI_Icons.MESSAGES.SUCCESS(AppColors.secondary900, 24);
    case 'warning':
      return UI_Icons.MESSAGES.WARNING(AppColors.secondary900, 24);
  }
};

const AppAlert = ({ type = 'error', message }: AppAlertProps): JSX.Element => {
  const { t } = useTranslation();
  return (
    <View style={SnackMessageStyle.container}>
      <View style={SnackMessageStyle.iconContainer}>
        <IconSelector type={type} />
      </View>
      <AppText fontWeight="600" style={SnackMessageStyle.alertText}>
        {t(message)}
      </AppText>
    </View>
  );
};

export default AppAlert;

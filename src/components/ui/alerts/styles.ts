import { Platform, StyleSheet, ViewStyle } from 'react-native';
import { EdgeInsets } from 'react-native-safe-area-context';
import config from '../../../config';
import { AppColors, globalStyles } from '../../../config/styles/GlobalStyles';
import { SnackBarMessage } from '../../../types/types/messages';
import { VerticalPositioning } from '../../../types/types/utils';

const SnackBarStyles: Record<string, ViewStyle> = {
  common: {
    //height: 60,
    position: 'absolute',
    padding: 10,
    zIndex: 100000,
    borderRadius: 5,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
  },
  top: {
    top: '1.2%',
  },

  top_unsafe: {
    top: '5%',
  },
  bottom: {
    bottom: '1.2%',
  },
  small: {
    width: '34%',
    left: '33%',
  },
  bottom_unsafe: {
    bottom: '5%',
  },
  medium: {
    width: '50%',
    left: '25%',
  },
  large: {
    width: '76%',
    left: '12%',
  },
  full: {
    width: '96%',
    left: '2%',
  },
  warning: {
    backgroundColor: '#F68A1C',
  },
  success: {
    backgroundColor: '#4E9A51',
  },
  quit: {
    backgroundColor: '#1E95D6',
  },
  info: {
    backgroundColor: '#1E95D6',
  },
  error: {
    backgroundColor: '#D84646',
  },
};

export const SnackMessageStyle = StyleSheet.create({
  container: {
    height: '100%',
    display: 'flex',
    flexDirection: 'row',
    alignContent: 'center',
  },
  iconContainer: {
    alignContent: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
    paddingLeft: 10,
    paddingRight: 10,
  },
  alertText: {
    flex: 1,
    color: AppColors.secondary900,
    alignSelf: 'center',
    alignContent: 'center',
    justifyContent: 'center',
    paddingBottom: 2,
    paddingTop: 2,
  },
});

const chooseIosPosition = (
  position: VerticalPositioning,
  inSafeView: boolean,
  insets?: EdgeInsets,
): ViewStyle => {
  const iosPositions = {
    bottom: {
      bottom:
        /*insets?.bottom ? insets.bottom * 6 : globalStyles.IOS_MEASURES.statusBar + 50*/ insets?.bottom &&
        insets.bottom > 20
          ? insets.bottom * 3
          : globalStyles.IOS_MEASURES.statusBar * 4,
    },
    bottom_unsafe: { bottom: insets?.bottom || globalStyles.IOS_MEASURES.statusBar },
    top: {
      top: insets?.top
        ? insets.top / 8 + globalStyles.APP_HEADER_BAR.height
        : globalStyles.IOS_MEASURES.statusBar + globalStyles.APP_HEADER_BAR.height,
    },
    top_unsafe: {
      top: insets?.top || globalStyles.IOS_MEASURES.statusBar,
    },
  };
  switch (position) {
    case 'bottom':
    default:
      if (inSafeView) {
        return iosPositions.bottom;
      }
      return iosPositions.bottom_unsafe;
    case 'top':
      if (inSafeView) {
        return iosPositions.top;
      }
      return iosPositions.top_unsafe;
  }
};

const choosePosition = (position: VerticalPositioning, inSafeView: boolean) => {
  switch (position) {
    case 'bottom':
    default:
      if (inSafeView) {
        return SnackBarStyles.bottom;
      }
      return SnackBarStyles.bottom_unsafe;
    case 'top':
      if (inSafeView) {
        return SnackBarStyles.top;
      }
      return SnackBarStyles.top_unsafe;
  }
};

const generateStyle = (
  message: SnackBarMessage,
  inSafeView: boolean,
  insets?: EdgeInsets,
): object => {
  const size = message.size ? SnackBarStyles[message.size] : SnackBarStyles.full;
  const color = SnackBarStyles[message.type];
  const position =
    Platform.OS === 'android' || !config.LAYOUT.USE_IOS_BAR_HACK
      ? choosePosition(message.position || 'bottom', inSafeView)
      : chooseIosPosition(message.position || 'bottom', inSafeView, insets);
  return { ...SnackBarStyles.common, ...size, ...color, ...position };
};

export default generateStyle;

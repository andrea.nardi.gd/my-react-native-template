import React, { useEffect, useState, useRef } from 'react';
import { Animated, Pressable } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { useDispatch } from 'react-redux';
import MessageServices from '../../../services/local/MessageServices';
//import MessageServices from '../../services/local/MessageServices';
import { useAppSelector } from '../../../store/hooks';
import AppAlert from './SnackbarAlert';
import generateStyle from './styles';

//const timer = new AppTimer();
const AnimatedTouchable = Animated.createAnimatedComponent(Pressable); //TODO use React-native-reanimated instead

interface AppSnackBarProps {
  inSafeView?: boolean;
  isQuitSnackbar?: boolean;
}

const AppSnackBar = ({
  inSafeView = true,
  isQuitSnackbar = false,
}: AppSnackBarProps): JSX.Element => {
  //const dispatch = useDispatch();
  const insets = useSafeAreaInsets();
  const activeSnackBar = isQuitSnackbar
    ? useAppSelector((state) => state.messageContext.activeQuitMessage)
    : useAppSelector((state) => state.messageContext.activeSnackBar);
  //const activeSnackBar = useAppSelector((state) => state.messageContext.activeSnackBar);
  const [opacityRef, setOpacityRef] = useState(useRef(new Animated.Value(0)).current);
  const [alreadyVisible, setAlreadyVisible] = useState<boolean>(false);
  const [visible, setVisible] = useState<boolean>(false);
  const dispatch = useDispatch();
  const isAnimated = false;

  const hideSnackBar = (): void => {
    setOpacityRef(new Animated.Value(0));
    dispatch(MessageServices.hideSnackBarMessage());
    setVisible(false);
    setAlreadyVisible(false);
  };

  const showSnackBar = (): void => {
    setAlreadyVisible(false);
    setOpacityRef(new Animated.Value(0));
    setVisible(true);
  };

  useEffect(() => {
    if (activeSnackBar) {
      setAlreadyVisible(true);
    }
  }, []);

  useEffect(() => {
    if (opacityRef) {
      Animated.timing(opacityRef, {
        toValue: 1,
        duration: alreadyVisible ? 0 : 150,
        useNativeDriver: true,
      }).start();
    }
  }, [opacityRef]);

  useEffect(() => {
    if (activeSnackBar) {
      setVisible(false);
      showSnackBar();
    } else {
      hideSnackBar();
    }
  }, [activeSnackBar]);

  const snackBarPress = () => {
    if (activeSnackBar?.type !== 'quit') {
      hideSnackBar();
    }
  };

  if (activeSnackBar && visible) {
    return (
      <AnimatedTouchable
        onPress={snackBarPress}
        style={{
          opacity: isAnimated ? opacityRef : 1,
          ...generateStyle(activeSnackBar, inSafeView, insets),
        }}
      >
        <AppAlert
          closeFunction={hideSnackBar}
          type={activeSnackBar.type}
          message={activeSnackBar.message}
        />
      </AnimatedTouchable>
    );
  }

  return <></>;
};

export default AppSnackBar;

import React from 'react';
import { StyleProp, View, ViewStyle } from 'react-native';
import { CheckBox } from 'react-native-elements';

interface AppFormFieldProps {
  containerStyle?: StyleProp<ViewStyle>;
  title?: string;
  field: string;
  checked: boolean;
  editFunction: (field: string, value: boolean) => void;
}

/*const AppFormField = <T>({ type, key, value, state }: AppFormField<T>): JSX.Element => {
  return <></>;
};*/

const AppCheckBox = ({
  title = '',
  field,
  checked,
  editFunction,
  containerStyle,
}: AppFormFieldProps): JSX.Element => {
  const onChecked = () => {
    editFunction(field, !checked);
  };

  return (
    <View style={containerStyle ? containerStyle : {}}>
      <CheckBox title={title} checked={checked} onPress={onChecked} />
    </View>
  );
};

export default AppCheckBox;

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { StyleProp, View, ViewStyle } from 'react-native';
import { TextInput } from 'react-native';
import { AppFontsDictionary } from '../../../config/assets/fonts';
import { InfosDict, InfosEntryKeys } from '../../../config/modals/infos/InfosDict';
import InfoModal from '../../modals/InfoModal';
import { ThemeVariants } from '../../../types/types/styles/themes';
import AppText from '../../layout/text/AppText';
import { useAppSelector } from '../../../store/hooks';

interface AppFormFieldProps {
  containerStyle?: StyleProp<ViewStyle>;
  type: 'text' | 'number';
  keyName?: string;
  value: string;
  label?: string;
  hasDescription?: boolean;
  labelStyle?: ThemeVariants;
  isEditable?: boolean;
  editFunction?: (value: string, keyName?: string) => void;
}

function AppFormField({
  containerStyle,
  keyName,
  value,
  label,
  labelStyle = ThemeVariants.DEFAULT,
  type = 'text',
  hasDescription = false,
  isEditable = true,
  editFunction,
}: AppFormFieldProps): JSX.Element {
  const AppColors = useAppSelector((store) => store.theme.AppColors);

  const [isDescriptionModalVisible, setIsDescriptionModalVisible] = useState(false);
  const { t } = useTranslation();

  const descriptionModalControlFunction = () => {
    setIsDescriptionModalVisible(!isDescriptionModalVisible);
  };

  const onEdit = (value: string, keyName?: string): void => {
    if (isEditable && editFunction) {
      editFunction(value, keyName);
    }
  };

  return (
    <View style={containerStyle ? containerStyle : { width: '100%' }}>
      <View style={{ flexDirection: 'row', width: '100%' }}>
        <AppText
          fontWeight="500"
          style={{
            color:
              labelStyle === ThemeVariants.DEFAULT
                ? AppColors.ink800
                : labelStyle === ThemeVariants.DARK
                ? AppColors.ink900
                : AppColors.secondary100,
            marginLeft: 15,
            fontSize: 18,
            lineHeight: 20,
            flex: 1,
          }}
        >
          {label}
        </AppText>
        {hasDescription && (
          <AppText
            onPress={descriptionModalControlFunction}
            style={{
              textAlign: 'right',
              color:
                labelStyle === ThemeVariants.DEFAULT
                  ? AppColors.primary600
                  : labelStyle === ThemeVariants.DARK
                  ? AppColors.primary700
                  : AppColors.secondary100,
              marginLeft: 15,
              fontSize: 12,
              lineHeight: 20,
              marginRight: 20,
              textDecorationLine: 'underline',
            }}
          >
            {t('labelDescriptionKey')}
          </AppText>
        )}
      </View>

      <View
        style={{
          opacity: isEditable ? 1 : 0.5,
          borderWidth: 1,
          borderColor:
            labelStyle === ThemeVariants.DEFAULT || label === ThemeVariants.DARK
              ? AppColors.ink500
              : AppColors.secondary900,
          borderRadius: 15,
          backgroundColor: AppColors.secondary900,

          display: 'flex',
          flexDirection: 'row',
          margin: 15,
          marginTop: 10,
          marginBottom: 10,
        }}
      >
        <TextInput
          style={{
            fontFamily: AppFontsDictionary.default,
            marginLeft: 10,
            flex: 1,
            height: 20,
          }}
          editable={isEditable}
          keyboardType={type === 'number' ? 'number-pad' : 'default'}
          value={value}
          onChangeText={(text) => onEdit(text, keyName)}
        />
      </View>
      {hasDescription && (
        <InfoModal
          toggleModal={descriptionModalControlFunction}
          isVisible={isDescriptionModalVisible}
          infoContent={
            InfosDict[keyName as unknown as InfosEntryKeys]
              ? InfosDict[keyName as unknown as InfosEntryKeys]
              : InfosDict.default
          }
        />
      )}
    </View>
  );
}

export default AppFormField;

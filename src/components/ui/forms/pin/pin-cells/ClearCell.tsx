import React from 'react';
import { LayoutChangeEvent, View } from 'react-native';
import { Cursor } from 'react-native-confirmation-code-field';
import { useAppSelector } from '../../../../../store/hooks';
import AppText from '../../../../layout/text/AppText';
import styles from '../styles';

interface ClearCellProps {
  index: number;
  isFocused: boolean;
  getCellOnLayoutHandler: (index: number) => (event: LayoutChangeEvent) => void;
  cellCount: number;
  halfSplit?: boolean;
  symbol: string;
}

const ClearCell = ({
  index,
  isFocused,
  getCellOnLayoutHandler,
  cellCount,
  halfSplit = false,
  symbol,
}: ClearCellProps): JSX.Element => {
  const AppColors = useAppSelector((store) => store.theme.AppColors);

  return (
    <>
      <View
        key={index}
        style={[styles.cell, isFocused && styles.focusCell]}
        onLayout={getCellOnLayoutHandler(index)}
      >
        <AppText
          style={{
            fontSize: isFocused ? 16 : 24,
            lineHeight: isFocused ? 18 : 28,
            color: AppColors.ink900,
          }}
        >
          {(symbol ? <AppText>{symbol}</AppText> : null) || (isFocused ? <Cursor /> : null)}
        </AppText>
      </View>
      {halfSplit && index === cellCount / 2 - 1 ? (
        <View
          style={{
            margin: 10,
            width: 30,
            height: 0.5,
            backgroundColor: AppColors.ink500,
            alignSelf: 'center',
          }}
          key={'separator-' + index}
        ></View>
      ) : (
        <View key={'separator-' + index} />
      )}
    </>
  );
};

export default ClearCell;

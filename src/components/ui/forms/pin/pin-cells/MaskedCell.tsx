import React from 'react';
import { LayoutChangeEvent, View } from 'react-native';
import { Cursor, MaskSymbol } from 'react-native-confirmation-code-field';
import { useAppSelector } from '../../../../../store/hooks';
import AppText from '../../../../layout/text/AppText';
import styles from '../styles';

interface MaskedCellProps {
  index: number;
  isFocused: boolean;
  getCellOnLayoutHandler: (index: number) => (event: LayoutChangeEvent) => void;
  isLastFilledCell: boolean;
  symbol: string;
  cellCount: number;
  halfSplit?: boolean;
}

const MaskedCell = ({
  index,
  isFocused,
  getCellOnLayoutHandler,
  isLastFilledCell,
  symbol,
  cellCount,
  halfSplit = false,
}: MaskedCellProps): JSX.Element => {
  const AppColors = useAppSelector((store) => store.theme.AppColors);

  return (
    <>
      <View
        key={index}
        style={[styles.cell, isFocused && styles.focusCell]}
        onLayout={getCellOnLayoutHandler(index)}
      >
        <AppText
          style={{
            fontSize: isFocused ? 16 : 24,
            lineHeight: isFocused ? 18 : 28,
            color: AppColors.ink900,
          }}
        >
          {(symbol ? (
            <MaskSymbol maskSymbol="·" isLastFilledCell={isLastFilledCell}>
              {symbol}
            </MaskSymbol>
          ) : null) || (isFocused ? <Cursor /> : null)}
        </AppText>
      </View>
      {halfSplit && index === cellCount / 2 - 1 ? (
        <View
          style={{
            margin: 10,
            width: 30,
            height: 0.5,
            backgroundColor: AppColors.ink500,
            alignSelf: 'center',
          }}
          key={'separator-' + index}
        ></View>
      ) : (
        <View key={'separator-' + index} />
      )}
    </>
  );
};

export default MaskedCell;

import { LayoutChangeEvent } from 'react-native';
import React from 'react';
import ClearCell from './ClearCell';
import MaskedCell from './MaskedCell';

interface GenericPinCellProps {
  index: number;
  isFocused: boolean;
  getCellOnLayoutHandler: (index: number) => (event: LayoutChangeEvent) => void;
  cellCount: number;
  halfSplit?: boolean;
  symbol: string;
  isLastFilledCell: boolean;
  isPassword: boolean;
}

const PinCell = ({
  index,
  isFocused,
  getCellOnLayoutHandler,
  isLastFilledCell,
  symbol,
  cellCount,
  halfSplit = false,
  isPassword = false,
}: GenericPinCellProps): JSX.Element => {
  if (isPassword) {
    return (
      <MaskedCell
        key={index + '_masked_pin'}
        isLastFilledCell={isLastFilledCell}
        getCellOnLayoutHandler={getCellOnLayoutHandler}
        index={index}
        isFocused={isFocused}
        symbol={symbol}
        halfSplit={halfSplit}
        cellCount={cellCount}
      />
    );
  }
  return (
    <ClearCell
      getCellOnLayoutHandler={getCellOnLayoutHandler}
      index={index}
      isFocused={isFocused}
      symbol={symbol}
      halfSplit={halfSplit}
      cellCount={cellCount}
    />
  );
};

export default PinCell;

import { StyleSheet } from 'react-native';
import { AppColors } from '../../../../config/styles/GlobalStyles';

export default StyleSheet.create({
  root: { padding: 20, minHeight: 300 },
  title: { textAlign: 'center', fontSize: 30, lineHeight: 34 },
  codeFiledRoot: { justifyContent: 'space-around' },
  cell: {
    width: 40,
    height: 60,
    borderRadius: 20,
    lineHeight: 38,
    fontSize: 24,
    borderWidth: 2,
    backgroundColor: AppColors.secondary100,
    margin: 20,
    borderColor: '#00000030',
    textAlign: 'center',
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    textAlignVertical: 'center',
  },
  focusCell: {
    borderColor: '#000',
    textAlign: 'center',
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    textAlignVertical: 'center',
  },
});

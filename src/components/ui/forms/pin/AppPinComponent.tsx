import React, { useEffect, useState } from 'react';
import {
  CodeField,
  useBlurOnFulfill,
  useClearByFocusCell,
  isLastFilledCell,
} from 'react-native-confirmation-code-field';
import config from '../../../../config';
import Logger from '../../../../services/local/Logger';
import PinCell from './pin-cells/PinCell';
import styles from './styles';

interface PinComponentProps {
  password?: boolean;
  editFunction: (pin: string) => void;
  cellCount: number;
  halfSplit?: boolean;
}

const AppPinComponent = ({
  editFunction,
  password,
  halfSplit = false,
  cellCount = config.SECURITY_SETTINGS.PIN_LENGTH || 4,
}: PinComponentProps): JSX.Element => {
  const [value, setValue] = useState('');
  const ref = useBlurOnFulfill({ value, cellCount: cellCount });
  const [props, getCellOnLayoutHandler] = useClearByFocusCell({
    value,
    setValue,
  });

  useEffect(() => {
    Logger.LogDebug({ args: ['Pin length on change ', value.length], prefixes: 'PIN' });
    if (value.length >= cellCount) onFullfill();
  }, [value]);

  const onFullfill = () => {
    editFunction(value);
  };

  return (
    <CodeField
      ref={ref}
      {...props}
      caretHidden={false}
      value={value}
      onChangeText={setValue}
      cellCount={cellCount}
      rootStyle={styles.codeFiledRoot}
      keyboardType="phone-pad"
      textContentType="oneTimeCode"
      renderCell={({ index, symbol, isFocused }) => (
        <PinCell
          key={`${!password ? 'un' : ''}masked_pin_${index}`}
          isLastFilledCell={isLastFilledCell({ index, value })}
          getCellOnLayoutHandler={getCellOnLayoutHandler}
          index={index}
          isFocused={isFocused}
          symbol={symbol}
          isPassword={password || false}
          halfSplit={halfSplit}
          cellCount={cellCount}
        />
      )}
    />
  );
};

export default AppPinComponent;

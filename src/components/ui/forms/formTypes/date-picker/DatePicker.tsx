import React, { useEffect, useState } from 'react';

import { Pressable, StyleSheet } from 'react-native';

import AppText from '../../../../layout/text/AppText';
import DatePickerModal from './DatePickerModal';
import { format } from 'date-fns';
import { it, enUS } from 'date-fns/locale';
import { useTranslation } from 'react-i18next';
import dateTimeFormats from '../../../../../config/constants/dateTimeFormats';
import { useAppSelector } from '../../../../../store/hooks';

interface AppDatePickerProps {
  value?: string;
  keyName?: string;
  editFunction?: (keyName: string, value: string) => void;

  isEditable?: boolean;
}

const DatePicker = ({
  value,
  keyName,
  editFunction,
  isEditable = true,
}: AppDatePickerProps): JSX.Element => {
  const AppColors = useAppSelector((store) => store.theme.AppColors);

  const [date, setDate] = useState('');
  const [pickerVisible, setPickerVisible] = useState(false);
  const { i18n } = useTranslation();

  useEffect(() => {
    if (value && value !== date) {
      setDate(value);
    }
  }, [value]);

  const editDateFunction = (date: Date) => {
    if (date && isEditable && editFunction) {
      const parsedDate = format(date, dateTimeFormats.input.standardDate, {
        locale: i18n.language === 'it' ? it : enUS,
      });
      //const parsedDate = date.toLocaleString()
      setDate(parsedDate); //HACK to speedup date change
      editFunction(keyName as string, parsedDate);
      hidePickerVisible();
    }
  };

  const hidePickerVisible = () => {
    setPickerVisible(false);
  };

  const showDatePicker = () => {
    setPickerVisible(true);
  };

  return (
    <>
      <Pressable
        style={[styles.datePickerStyle, { backgroundColor: AppColors.secondary900 }]}
        onPress={showDatePicker}
      >
        <AppText style={{ marginLeft: 15 }}>{date || ''}</AppText>
      </Pressable>
      <DatePickerModal
        isVisible={pickerVisible}
        modalControlFunction={hidePickerVisible}
        editDateFunction={editDateFunction}
      />
    </>
  );
};

export default DatePicker;

const styles = StyleSheet.create({
  container: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  datePickerStyle: {
    padding: 0,
    borderWidth: 0,
    height: 32,
    display: 'flex',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'flex-start',
    borderRadius: 20,
  },
});

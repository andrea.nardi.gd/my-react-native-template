import React, { useState } from 'react';
import DateTimePickerModal from 'react-native-modal-datetime-picker';

interface DatePickerModalProps {
  isVisible: boolean;
  modalControlFunction: () => void;
  editDateFunction: (value: Date) => void;
}

const DatePickerModal = ({
  isVisible,
  modalControlFunction,
  editDateFunction,
}: DatePickerModalProps): JSX.Element => {
  const [defaultDate, setDefaultDate] = useState(new Date());

  const handleConfirm = (date: Date) => {
    editDateFunction(date);
    setDefaultDate(date);
  };

  return (
    <DateTimePickerModal
      isVisible={isVisible}
      mode="date"
      date={defaultDate}
      onConfirm={handleConfirm}
      onCancel={modalControlFunction}
    />
  );
};

export default DatePickerModal;

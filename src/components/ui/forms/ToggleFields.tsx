import React from 'react';
import { Switch } from 'react-native';
import { useAppSelector } from '../../../store/hooks';

interface AppFormFieldProps {
  toggleValue: boolean;
  editFunction: () => void;
}

const ToggleField = ({ toggleValue, editFunction }: AppFormFieldProps): JSX.Element => {
  const AppColors = useAppSelector((store) => store.theme.AppColors);
  return (
    <Switch
      value={toggleValue}
      onValueChange={editFunction}
      trackColor={{ false: AppColors.ink400, true: AppColors.primary700 }}
      thumbColor={toggleValue ? AppColors.primary300 : AppColors.ink200}
      ios_backgroundColor="transparent"
    />
  );
};

export default ToggleField;

import AppStateMonitor from '../../utils/monitors/AppStateMonitor';
import DeviceInfoMonitor from '../../utils/monitors/DeviceInfoMonitor';
import LocalizationMonitor from '../../utils/monitors/LocalizationMonitor';
import NetListener from '../../utils/monitors/NetListener';
import PopupMessageMonitor from '../../utils/monitors/PopupMessageMonitor';

const StartSystemsMonitor = (): void => {
  PopupMessageMonitor();
  AppStateMonitor();
  NetListener();
  DeviceInfoMonitor();
  LocalizationMonitor();
};

export default StartSystemsMonitor;

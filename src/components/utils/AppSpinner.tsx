import React, { useMemo } from 'react';
import { ActivityIndicator, Platform, StyleProp, ViewStyle } from 'react-native';
import { useAppSelector } from '../../store/hooks';
import { SizeVariants } from '../../types/types/styles/sizes';
import { ColorVariants } from '../../types/types/styles/themes';

interface AppLoaderProps {
  size: SizeVariants.LARGE | SizeVariants.SMALL;
  color: ColorVariants;
  style?: StyleProp<ViewStyle>;
}

const AppSpinner = ({ size, color, style }: AppLoaderProps): JSX.Element => {
  const AppColors = useAppSelector((store) => store.theme.AppColors);

  const platformSize = useMemo(() => {
    if (Platform.OS === 'ios') {
      return size;
    }
    return size === SizeVariants.LARGE ? 160 : 48;
  }, [size]);

  return (
    <ActivityIndicator
      style={style || {}}
      size={platformSize}
      color={color === ColorVariants.SECONDARY ? AppColors.secondary100 : AppColors.primary500}
    />
  );
};

export default AppSpinner;

import React from 'react';
import { useTranslation } from 'react-i18next';
import { Dimensions, Modal, Platform, ScrollView, View } from 'react-native';
import UI_Icons from '../icons';
import AppText from '../layout/text/AppText';
import VerticalSeparator from '../layout/utilities/VerticalSeparator';
import AppConfirmButton from '../ui/buttons/AppConfirmButton';
import { InfoEntry } from '../../config/modals/infos/InfosDict';
import { globalStyles } from '../../config/styles/GlobalStyles';
import { SizeVariants } from '../../types/types/styles/sizes';
import { ColorVariants } from '../../types/types/styles/themes';
import { useAppSelector } from '../../store/hooks';

interface InfoModalContent {
  isVisible: boolean;
  toggleModal: () => void;
  infoContent: InfoEntry;
  optionalTranslationParameters?: object;
}

const InfoModal = ({
  isVisible,
  toggleModal,
  infoContent,
  optionalTranslationParameters,
}: InfoModalContent): JSX.Element => {
  const AppColors = useAppSelector((store) => store.theme.AppColors);
  const { t } = useTranslation();

  const confirmNavigation = () => {
    toggleModal();
  };

  const changePasswordOnChange = () => {
    confirmNavigation();
  };

  return (
    <Modal transparent={true} visible={isVisible}>
      <View
        style={{
          backgroundColor: globalStyles.MODALS.backgroundColor,
          flex: 1,
          justifyContent: 'center',
          alignContent: 'center',
          alignItems: 'center',
          width: '100%',
        }}
      >
        <View
          style={{
            backgroundColor: AppColors.secondary900,
            height: Dimensions.get('screen').height > 700 ? '55%' : '75%',
            width: '85%',
            borderRadius: 10,
          }}
        >
          <View
            style={{
              borderTopLeftRadius: 10,
              borderTopEndRadius: 10,
              padding: 10,
              backgroundColor: AppColors.primary500,
              alignContent: 'center',
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            {UI_Icons.MESSAGES.INFO(AppColors.secondary900, 48)}
          </View>
          <View style={{ flex: 1 }}>
            <ScrollView style={{ padding: 20, paddingBottom: 10 }}>
              <AppText
                fontWeight="bold"
                style={{ fontSize: 24, lineHeight: 30, textAlign: 'center' }}
              >
                {t(
                  infoContent?.textTitle || '',
                  optionalTranslationParameters ? { ...optionalTranslationParameters } : {},
                )}
              </AppText>
              <VerticalSeparator distance={SizeVariants.MEDIUM} />
              <AppText style={{ fontSize: 16, lineHeight: 30, textAlign: 'center' }}>
                {t(
                  infoContent?.textKey || '',
                  optionalTranslationParameters ? { ...optionalTranslationParameters } : {},
                )}
              </AppText>
              {Platform.OS === 'ios' && <VerticalSeparator distance={SizeVariants.LARGE} />}
            </ScrollView>
          </View>

          <View style={{ padding: 20, paddingBottom: 10, paddingTop: 0, justifyContent: 'center' }}>
            <AppConfirmButton
              variant={ColorVariants.PRIMARY}
              size={SizeVariants.LARGE}
              confirmFunction={changePasswordOnChange}
              title={t('close')}
            />
            <View style={{ height: 10 }} />
          </View>
        </View>
      </View>
    </Modal>
  );
};

export default InfoModal;

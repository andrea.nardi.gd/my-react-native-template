import { useNavigation } from '@react-navigation/native';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { Dimensions, Modal, Platform, ScrollView, View } from 'react-native';
import UI_Icons from '../icons';
import AppText from '../layout/text/AppText';
import AppConfirmButton from '../ui/buttons/AppConfirmButton';
import { DefaultStackPages } from '../../config/navs/NavsConfig';
import { globalStyles } from '../../config/styles/GlobalStyles';
import { WarningEntry } from '../../config/modals/warnings/WarningsDict';
import VerticalSeparator from '../layout/utilities/VerticalSeparator';
import AppCancelButton from '../ui/buttons/AppCancelButton';
import { ColorVariants } from '../../types/types/styles/themes';
import { SizeVariants } from '../../types/types/styles/sizes';
import { useAppSelector } from '../../store/hooks';

interface AlertModalContent {
  isVisible: boolean;
  toggleModal: () => void;
  alertContent: WarningEntry;
  overrideRedirect?: string;
  optionalTranslationParameters?: object;
  optionalRedirectParameters?: unknown;
  isGoBackAlert?: boolean;
  goBackOverride?: string | null;
  optionalGoBackParams?: never;
  _isQuitModal?: boolean;
  overrideFunction?: () => void;
}

const AlertModal = ({
  isVisible,
  toggleModal,
  alertContent,
  overrideRedirect,
  optionalTranslationParameters,
  optionalRedirectParameters,
  overrideFunction,
  goBackOverride,
  optionalGoBackParams,
  isGoBackAlert = false,
  _isQuitModal = false,
}: AlertModalContent): JSX.Element => {
  const AppColors = useAppSelector((store) => store.theme.AppColors);

  const navigation = useNavigation();
  const isAuthorized = useAppSelector((store) => store.localAuth.isUserAuthorized);
  const { t } = useTranslation();

  const confirmNavigation = () => {
    if (isGoBackAlert) {
      goBackOverride
        ? navigation?.navigate(goBackOverride as never, optionalGoBackParams || ({} as never))
        : navigation?.goBack();
    } else {
      if (alertContent?.confirmRedirect) {
        navigation?.navigate(
          (overrideRedirect
            ? (overrideRedirect as never)
            : (alertContent?.confirmRedirect as never)) ||
            (isAuthorized
              ? DefaultStackPages.AUTHENTICATED_STACK
              : (DefaultStackPages.UNAUTHENTICATED_STACK as never)),
          (optionalRedirectParameters as never) || ({} as never),
        );
      }
    }
    toggleModal();
  };

  const changePasswordOnChange = () => {
    if (overrideFunction) {
      overrideFunction();
    } else {
      confirmNavigation();
    }
  };

  return (
    <Modal transparent={true} visible={isVisible}>
      <View
        style={{
          backgroundColor: globalStyles.MODALS.backgroundColor,
          flex: 1,
          justifyContent: 'center',
          alignContent: 'center',
          alignItems: 'center',
          width: '100%',
        }}
      >
        <View
          style={{
            backgroundColor: AppColors.secondary900,
            height: Dimensions.get('screen').height > 700 ? '55%' : '75%',
            width: '85%',
            borderRadius: 10,
          }}
        >
          <View
            style={{
              borderTopLeftRadius: 10,
              borderTopEndRadius: 10,
              padding: 10,
              backgroundColor: AppColors.warning400,
              alignContent: 'center',
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            {UI_Icons.MESSAGES.WARNING(AppColors.secondary900, 48)}
          </View>
          <View style={{ flex: 1 }}>
            <ScrollView style={{ padding: 20, paddingBottom: 5 }}>
              <AppText
                fontWeight="bold"
                style={{ fontSize: 24, lineHeight: 30, textAlign: 'center' }}
              >
                {t(
                  alertContent?.textTitle || '',
                  optionalTranslationParameters ? { ...optionalTranslationParameters } : {},
                )}
              </AppText>
              {alertContent?.textTitle && <VerticalSeparator distance={SizeVariants.MEDIUM} />}
              <AppText style={{ fontSize: 16, lineHeight: 30, textAlign: 'center' }}>
                {t(
                  alertContent?.textKey || '',
                  optionalTranslationParameters ? { ...optionalTranslationParameters } : {},
                )}
              </AppText>
              {Platform.OS === 'ios' && <VerticalSeparator distance={SizeVariants.LARGE} />}
            </ScrollView>
          </View>

          <View
            style={{
              padding: 20,
              paddingBottom: 10,
              paddingTop: 0,
              justifyContent: 'center',
              alignContent: 'center',
              alignItems: 'center',
            }}
          >
            <AppConfirmButton
              variant={ColorVariants.PRIMARY}
              size={SizeVariants.LARGE}
              confirmFunction={changePasswordOnChange}
              title={t('confirm')}
            />

            <AppCancelButton
              variant={ColorVariants.PRIMARY}
              size={SizeVariants.LARGE}
              cancelFunction={toggleModal}
              title={t('cancel')}
              border={true}
            />
          </View>
        </View>
      </View>
    </Modal>
  );
};

export default AlertModal;

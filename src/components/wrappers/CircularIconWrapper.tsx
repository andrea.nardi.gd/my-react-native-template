import React, { useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import { FlexStyle, Pressable, StyleProp, TextStyle, View, ViewStyle } from 'react-native';
import { useAppSelector } from '../../store/hooks';
import AppText from '../layout/text/AppText';

interface CircularIconWrapperProps {
  icon: React.ReactNode;
  size: number;
  wrapperColor: string;
  onPress?: () => void;
  label?: string;
  containerStyle?: StyleProp<ViewStyle>;
  labelPosition?: 'top' | 'bottom' | 'left' | 'right';
  labelStyle?: TextStyle;
}

const CircularIconWrapper = ({
  icon,
  size,
  wrapperColor,
  onPress,
  label,
  labelPosition = 'bottom',
  labelStyle,
  containerStyle,
}: CircularIconWrapperProps): JSX.Element => {
  const AppColors = useAppSelector((store) => store.theme.AppColors);

  const { t } = useTranslation();

  const flexDirection: FlexStyle['flexDirection'] = useMemo(() => {
    switch (labelPosition) {
      default:
      case 'bottom':
        return 'column';
      case 'top':
        return 'column-reverse';
      case 'left':
        return 'row';
      case 'right':
        return 'row-reverse';
    }
  }, [labelPosition]);

  return (
    <View
      style={[
        containerStyle ? containerStyle : {},
        {
          flexDirection: flexDirection,
          alignContent: 'center',
          alignItems: 'center',
          justifyContent: 'center',
        },
      ]}
    >
      <Pressable
        onPress={onPress}
        style={{
          width: size * 2,
          height: size * 2,
          borderRadius: size * 2,
          alignContent: 'center',
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: wrapperColor,
        }}
      >
        {icon}
      </Pressable>
      {label && (
        <AppText
          fontWeight="300"
          style={
            labelStyle || {
              fontSize: 12,
              lineHeight: 14,
              color: AppColors.ink800,
              margin: 5,
              textAlign: 'center',
            }
          }
        >
          {t(label)}
        </AppText>
      )}
    </View>
  );
};

export default CircularIconWrapper;

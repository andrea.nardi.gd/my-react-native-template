import React from 'react';
import { View } from 'react-native';
import { AntDesign } from '@expo/vector-icons';
import generateStepPointContainerStyles, { StepPointTextStyle } from './styles';
import AppText from '../../layout/text/AppText';
import { useAppSelector } from '../../../store/hooks';

interface NumberLabelProps {
  active?: boolean;
  done?: boolean;
  size?: number;
  label?: string;
  number: number;
}

const StepPoint = ({
  active = false,
  done = false,
  size = 12,
  number,
}: NumberLabelProps): JSX.Element => {
  const AppColors = useAppSelector((store) => store.theme.AppColors);
  if (done) {
    return (
      <AntDesign
        style={generateStepPointContainerStyles({
          done,
          active,
          size,
        })}
        name="checkcircle"
        size={size}
        color={AppColors.primary500}
      />
    );
  } else {
    return (
      <View
        style={generateStepPointContainerStyles({
          done,
          active,
          size,
        })}
      >
        <AppText style={StepPointTextStyle.text}>{number}</AppText>
      </View>
    );
  }
};

export default StepPoint;

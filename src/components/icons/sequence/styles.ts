import { StyleSheet, ViewStyle } from 'react-native';
import { AppColors } from '../../../config/styles/GlobalStyles';

interface StepStyleProps {
  done: boolean;
  active: boolean;
  size: number;
}

export const StepPointTextStyle = StyleSheet.create({
  text: {
    color: AppColors.secondary900,
    alignSelf: 'center',
    //fontSize: 10,
  },
});

const StepPointContainerStyles = ({
  done = false,
  active = false,
  size = 12,
}: StepStyleProps): ViewStyle => {
  if (done) {
    return {};
  }
  const stepStyleValues: ViewStyle = {
    width: size,
    height: size,
    borderRadius: size,
    backgroundColor: active ? AppColors.primary500 : AppColors.ink500,
    display: 'flex',
    justifyContent: 'center',
    alignContent: 'center',
  };

  if (active) {
    stepStyleValues.borderColor = AppColors.primary200;
    stepStyleValues.borderWidth = size / 12;
  }

  return stepStyleValues;
};

const generateStepPointContainerStyles = ({
  done = false,
  active = false,
  size = 12,
}: StepStyleProps): ViewStyle => {
  return { ...StepPointContainerStyles({ done, active, size }) };
};

export default generateStepPointContainerStyles;

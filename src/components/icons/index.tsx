import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faMugSaucer } from '@fortawesome/free-solid-svg-icons/faMugSaucer';
import React from 'react';

const UI_Icons = {
  INSTRUCTIONS: {},
  TOP_BAR: {
    TEST: (color: string, size = 36): JSX.Element => (
      <FontAwesomeIcon size={size} color={color} icon={faMugSaucer} />
    ),
  },
  HEADER: {
    BACK_BUTTON: (color: string, size = 36): JSX.Element => (
      <FontAwesomeIcon size={size} color={color} icon={faMugSaucer} />
    ),
  },
  MESSAGES: {
    WARNING: (color: string, size = 36): JSX.Element => (
      <FontAwesomeIcon size={size} color={color} icon={faMugSaucer} />
    ),
    INFO: (color: string, size = 36): JSX.Element => (
      <FontAwesomeIcon size={size} color={color} icon={faMugSaucer} />
    ),
    SUCCESS: (color: string, size = 36): JSX.Element => (
      <FontAwesomeIcon size={size} color={color} icon={faMugSaucer} />
    ),
    ERROR: (color: string, size = 36): JSX.Element => (
      <FontAwesomeIcon size={size} color={color} icon={faMugSaucer} />
    ),
  },
  SETTINGS: {
    EMPTY_CIRCULAR_IMAGE_PLACEHOLDER: (color: string, size = 36): JSX.Element => (
      <FontAwesomeIcon size={size} color={color} icon={faMugSaucer} />
    ),
    /*FONTAWESOME_EXAMPLE: (color: string, size = 36): JSX.Element => (
      <FontAwesomeIcon size={size} color={color} icon={ICON_NAME} />
    ),
    SVG_WRAPPER_EXAMPLE: (color: string, size = 36): JSX.Element => (
      <SvgWrapper
        svgComponent={ComponentFunctionName}
        containerStyle={{ width: size, height: size, alignSelf: 'center' }}
        svgProps={{ color: color }}
      />
    ),*/
  },
};

export default UI_Icons;

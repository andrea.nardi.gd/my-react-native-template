import { useNavigation } from '@react-navigation/native';
import React from 'react';
import { View } from 'react-native';
import { useAppSelector } from '../../../store/hooks';
import { Navigation } from '../../../types/types/config/navigation';

const AppHeader = (): JSX.Element => {
  //TODO add builder based on ActivePage Info
  const _navProperties = useAppSelector(
    (store) => store.navigationContext.activeScreen,
  ) as Navigation;
  const _navigation = useNavigation();
  return <View style={{ height: 50, width: '100%', backgroundColor: 'purple' }}></View>;
};

export default AppHeader;

import React, { useCallback, useEffect, useState } from 'react';
import { Modal, Platform, View } from 'react-native';
import config from '../../../config';
import LocalAuthServices from '../../../services/local/LocalAuthServices';
import { StorageAdapter } from '../../../services/local/StorageAdapter';
import { useAppSelector } from '../../../store/hooks';
import { AppXOR } from '../../../types/types/utils';
import SvgWrapper from '../../images/SvgWrapper';
import AppCancelButton from '../../ui/buttons/AppCancelButton';
import AppConfirmButton from '../../ui/buttons/AppConfirmButton';
import AppPinComponent from '../../ui/forms/pin/AppPinComponent';
import MockComponent from '../utilities/mock-components/MockComponent';

interface AuthorizationModalContent {
  isVisible: boolean;
  toggleModal: () => void;
  authorizedCallback: AppXOR<() => unknown, null>;
}

const AuthorizationModal = ({
  isVisible,
  toggleModal,
  authorizedCallback,
}: AuthorizationModalContent): JSX.Element => {
  const AppColors = useAppSelector((store) => store.theme.AppColors);
  const { biometricAuthEnabled, isPinSet } = useAppSelector((store) => store.localAuth);
  const [errors, setErrors] = useState<number>(0);
  const [validPin, setValidPin] = useState<boolean>(false);
  const [insertedPin, setInsertedPin] = useState<string>('');

  const resetFields = () => {
    setErrors(0);
    setValidPin(false);
    setInsertedPin('');
  };

  const closeModal = () => {
    toggleModal();
    resetFields();
  };

  const grantAuth = async () => {
    if (authorizedCallback) {
      authorizedCallback();
    }
    resetFields();
  };

  useEffect(() => {
    if (errors > config.SECURITY_SETTINGS.MAX_SECURITY_ERRORS) {
      closeModal();
    }
  }, [errors]);

  const checkPin = useCallback(async () => {
    const storage = new StorageAdapter();
    const pin = await storage.get(config.STORAGE_KEYS.pin);

    const pinValid = LocalAuthServices.checkPasswordEquality(insertedPin, pin);
    if (pin && pinValid) {
      setValidPin(true);
    } else {
      setErrors(errors + 1);
      setInsertedPin('');
    }
  }, [insertedPin]);

  useEffect(() => {
    if (validPin) {
      grantAuth();
    }
  }, [validPin]);

  useEffect(() => {
    if (validPin) {
      grantAuth();
    }
  }, [validPin]);

  const authWithBio = useCallback(async () => {
    const successfullBioAuth = await LocalAuthServices.unlockWithBiometrics();
    if (successfullBioAuth) {
      grantAuth();
    }
  }, [biometricAuthEnabled, isPinSet, grantAuth]);

  useEffect(() => {
    if (biometricAuthEnabled && isPinSet && isVisible) {
      authWithBio();
    }
  }, [biometricAuthEnabled, isPinSet, isVisible]);

  useEffect(() => {
    if (isPinSet && insertedPin.length === config.SECURITY_SETTINGS.PIN_LENGTH) {
      checkPin();
    }
  }, [insertedPin]);

  return (
    <Modal transparent={true} visible={isVisible} animationType="slide" onRequestClose={closeModal}>
      <View
        style={{
          backgroundColor: AppColors.primary500,
          flex: 1,
          justifyContent: 'flex-end',
          alignContent: 'center',
          alignItems: 'center',
          width: '100%',
        }}
      >
        <View
          style={{
            flex: 1,

            justifyContent: 'center',
            alignItems: 'center',
            marginTop: 100,
          }}
        >
          <SvgWrapper
            svgComponent={MockComponent} //TODO add image for
            containerStyle={{
              alignSelf: 'center',
              height: 230,
              width: 230,
            }}
          />
          <AppPinComponent
            cellCount={config.SECURITY_SETTINGS.PIN_LENGTH}
            editFunction={setInsertedPin}
            password={true}
          />
          {Platform.OS === 'web' && (
            <AppConfirmButton title={'Debug access grant'} confirmFunction={grantAuth} />
          )}
          <AppCancelButton title="cancel" cancelFunction={closeModal} />
        </View>
      </View>
    </Modal>
  );
};

export default AuthorizationModal;

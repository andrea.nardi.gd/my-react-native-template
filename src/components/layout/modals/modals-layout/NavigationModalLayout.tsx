import { useNavigation, useRoute } from '@react-navigation/native';
import React from 'react';
import { NavigationModal } from '../../../../types/types/config/navigation';
import MockComponent from '../../utilities/mock-components/MockComponent';
import FlexAlignView from '../../utilities/FlexAlignView';
import { globalStyles } from '../../../../config/styles/GlobalStyles';

interface ModalPageProps {
  page: React.ReactNode | null;
  navigationInfo?: NavigationModal;
}

const NavigationModalLayout = ({ navigationInfo, page }: ModalPageProps): JSX.Element => {
  if (page) {
    const PageComp = page as React.ElementType;
    const navigation = useNavigation();
    const route = useRoute();

    return (
      <FlexAlignView
        style={{ flex: 1, backgroundColor: globalStyles.MODALS.backgroundColorDarker }}
      >
        <PageComp navigation={navigation} navProperties={navigationInfo} route={route} />
      </FlexAlignView>
    );
  } else {
    return <MockComponent />;
  }
};

export default NavigationModalLayout;

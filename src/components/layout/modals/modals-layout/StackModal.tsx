import React, { useCallback, useMemo } from 'react';
import { Modal } from 'react-native';
import ModalsNavsComponents from '../../../../config/navs/navs-component/ModalsNavsComponents';
import ServiceNavsComponents from '../../../../config/navs/navs-component/ServiceNavsComponents';
import { MODALS_LIST } from '../../../../config/navs/NavsConfig';
import { CustomRoutePageProps } from '../../../../types/interfaces/navigation-pages/NavigationPageProps';
import { CustomParamsRouteKeys } from '../../../../types/types/navigation/routes/CustomParamsRoutesList';
import AppSnackBar from '../../../ui/alerts/AppSnackBar';
import NavigationModalLayout from './NavigationModalLayout';

const StackModal = ({
  navigation,
  route,
}: CustomRoutePageProps<CustomParamsRouteKeys.APP_MODAL>): JSX.Element => {
  const FullPageComponent = useMemo(() => {
    const compKey = route?.params?.navigationInfo?.component as MODALS_LIST;

    const component = ModalsNavsComponents[compKey]
      ? ModalsNavsComponents[compKey]
      : ServiceNavsComponents['default'];

    return (
      <NavigationModalLayout page={component} navigationInfo={route?.params?.navigationInfo} />
    );
  }, [route]);

  const closeModal = useCallback(() => {
    navigation?.goBack();
  }, [navigation, route]);

  return (
    <Modal
      animationType={route?.params?.navigationInfo?.modalTransition || 'fade'}
      transparent
      onRequestClose={closeModal}
    >
      {FullPageComponent}
      <AppSnackBar inSafeView={false} />
    </Modal>
  );
};

export default StackModal;

import { isArray } from 'lodash';
import React from 'react';
import { StyleProp, View, ViewStyle } from 'react-native';
import FlexAlignView from '../utilities/FlexAlignView';

interface ChunkContainerProps {
  rowStyle?: StyleProp<ViewStyle>;
  columnStyle?: StyleProp<ViewStyle>;
  children?: JSX.Element | JSX.Element[];
  containerSeparator?: JSX.Element;
  itemSeparator?: JSX.Element;
  chunkDirection: 'row' | 'column';
}

const ChunkContainer = ({
  rowStyle = {},
  children,
  containerSeparator,
  itemSeparator,
  columnStyle = {},
  chunkDirection,
}: ChunkContainerProps): JSX.Element => {
  const childrenCount = isArray(children) ? children.length : 1;
  const firstLevelStyle = chunkDirection === 'row' ? rowStyle : columnStyle;
  const secondLevelStyle = chunkDirection === 'column' ? columnStyle : rowStyle;

  return (
    <>
      <View style={[firstLevelStyle, { flexDirection: chunkDirection }]}>
        {React.Children.map(children, (child, index) => {
          return (
            <>
              <View style={[secondLevelStyle]}>{child}</View>
              {index !== childrenCount - 1 ? itemSeparator : <></>}
            </>
          );
        })}
      </View>
      {containerSeparator && <FlexAlignView>{containerSeparator}</FlexAlignView>}
    </>
  );
};

export default ChunkContainer;

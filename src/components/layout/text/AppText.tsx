import React, { useMemo } from 'react';
import { Text, TextProps, TextStyle } from 'react-native';
import { AppFontsDictionary } from '../../../config/assets/fonts';
import StyleHelpers from '../../../services/utils/TextHelpers';
import { useAppSelector } from '../../../store/hooks';
import { FontFamilies, FontStyles, FontWeights } from '../../../types/types/styles/fonts';
import { SizeVariants } from '../../../types/types/styles/sizes';

interface AppTextProps extends TextProps {
  children?: string | React.ReactNode | React.ReactNode[];
  fontFamily?: FontFamilies;
  fontWeight?: FontWeights;
  fontStyle?: FontStyles;
  style?: TextStyle | TextStyle[];
  lineHeightSpacing?: 'required' | SizeVariants.SMALL | SizeVariants.MEDIUM | SizeVariants.LARGE;
}

//NOTE AppText can wrap other AppText instances and apply common styles to its children,
//but composite text styles are applied this way only on Android devices due to the underlying native implementation
//of the Text renderer. To avoid unwanted behaviours on iOS, do not apply fontFamily, fontWeight and fontStyle properties on
//a wrapper component, but apply it to all of it's children instead.

const AppText = ({
  children,
  fontFamily,
  fontWeight,
  fontStyle,
  style,
  lineHeightSpacing = 'required',
  ellipsizeMode = undefined,
  numberOfLines = undefined,
  textBreakStrategy = undefined,
  onPress = undefined,
}: AppTextProps): JSX.Element => {
  const fontSizeMultiplier = useAppSelector((store) => store.theme.fontSizeMultiplier);

  const renderedText = useMemo(() => {
    return children;
  }, [children]);

  const combinedStyles = useMemo(() => {
    if (children) {
      const startStyle: TextStyle = style ? (style as TextStyle) : {};
      const lineHeightStyle: TextStyle = {};
      const fontFamilyStyle: TextStyle = {};

      if (startStyle?.fontSize) {
        startStyle.fontSize *= fontSizeMultiplier;
      }

      if (startStyle.lineHeight) {
        startStyle.lineHeight *= fontSizeMultiplier;
      }

      if (startStyle && startStyle.fontSize && !startStyle.lineHeight) {
        lineHeightStyle.lineHeight = StyleHelpers.generateCompatibleLineHeight(
          startStyle.fontSize,
          lineHeightSpacing,
        );
      }

      fontFamilyStyle.fontFamily = StyleHelpers.generateFontFamily({
        fontFamily: fontFamily as keyof typeof AppFontsDictionary,
        fontWeight: fontWeight as keyof typeof AppFontsDictionary[keyof typeof AppFontsDictionary],
        fontStyle: fontStyle as keyof typeof AppFontsDictionary[keyof typeof AppFontsDictionary],
      });

      return [startStyle, lineHeightStyle, fontFamilyStyle];
    }
    return {};
  }, [fontFamily, fontWeight, fontStyle, style, children, fontSizeMultiplier]);

  if (children) {
    return (
      <Text
        onPress={onPress}
        textBreakStrategy={textBreakStrategy}
        numberOfLines={numberOfLines}
        ellipsizeMode={ellipsizeMode}
        style={combinedStyles}
      >
        {renderedText}
      </Text>
    );
  }
  return <></>;
};

export default AppText;

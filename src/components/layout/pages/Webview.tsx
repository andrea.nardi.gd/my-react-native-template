import React from 'react';
import { Platform } from 'react-native';
import WebSafeWebView from './webview/WsWebView';
import NativeWebView from './webview/NativeWebView';

const WebViewPage = ({ uri }: { uri: string }): JSX.Element => {
  if (Platform.OS === 'web') {
    return <WebSafeWebView uri={uri} />;
  }

  return <NativeWebView uri={uri} />;
};

export default WebViewPage;

/* eslint-disable unused-imports/no-unused-vars */
/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { LegacyRef, useEffect, useLayoutEffect, useRef, useState } from 'react';
import { View } from 'react-native';
import { AppXOR } from '../../../../types/types/utils';
import { PostMessageResponse } from '../../../../types/types/api/postMessage';
import { useNavigation } from '@react-navigation/native';
import { useDispatch } from 'react-redux';

const WebSafeWebView = ({ uri }: { uri: string }): JSX.Element => {
  const navigation = useNavigation();
  const [frameLoaded, setFrameLoaded] = useState<boolean>(false);
  const frameRef = useRef<HTMLIFrameElement>();
  const dispatch = useDispatch();

  const sendPostMessage = async () => {
    if (frameRef) {
      frameRef?.current?.contentWindow?.postMessage(uri);
    }
  };

  const onMessageHandler = async (data: AppXOR<PostMessageResponse, Record<string, never>>) => {
    if (data.method) {
      switch (data.method) {
        default:
          break;
      }
    }
  };

  useLayoutEffect(() => {
    if (frameRef && frameLoaded) {
      sendPostMessage();
    }
  }, [frameRef, frameLoaded]);

  useEffect(() => {
    window.addEventListener('message', ({ data, ...e }) => onMessageHandler(data), false);

    return () => {
      window.removeEventListener('message', () => true);
    };
  }, []);

  return (
    <View style={{ flex: 1 }}>
      <iframe
        ref={frameRef as LegacyRef<HTMLIFrameElement>}
        style={{ flex: 1, width: '100%', borderStyle: 'none' }}
        src={uri}
        onLoad={() => setFrameLoaded(true)}
      ></iframe>
    </View>
  );
};

export default WebSafeWebView;

import React, { createRef, useMemo } from 'react';
import { WebView } from 'react-native-webview';
import { AppXOR } from '../../../../types/types/utils';
import { PostMessageResponse } from '../../../../types/types/api/postMessage';

const NativeWebView = ({ uri }: { uri: string }): JSX.Element => {
  const webViewRef = createRef<WebView>();

  const currentUri = useMemo(() => {
    if (uri) {
      return uri;
    }
    return null;
  }, [uri]);

  const injectMessageListener = useMemo(() => {
    return `
      window.addEventListener("message", function(message){
        window.ReactNativeWebView.postMessage(JSON.stringify(message.data));
      }, false);
      true;
    `;
  }, []);

  const onMessageHandler = async (data: AppXOR<PostMessageResponse, Record<string, never>>) => {
    if (data.method) {
      switch (data.method) {
        default:
          break;
      }
    }
  };

  if (!currentUri) return <></>;

  return (
    <WebView
      ref={webViewRef}
      originWhitelist={['*']}
      injectedJavaScriptBeforeContentLoaded={injectMessageListener}
      javaScriptEnabled
      mediaPlaybackRequiresUserAction={true}
      scalesPageToFit={true}
      bounces={false}
      domStorageEnabled={true}
      onMessage={(e) => {
        onMessageHandler(JSON.parse(e.nativeEvent.data));
      }}
      style={{ flex: 1, width: '100%', height: '100%' }}
      cacheEnabled={true}
      source={{
        uri: currentUri,
      }}
    />
  );
};

export default NativeWebView;

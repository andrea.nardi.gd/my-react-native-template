import React, { useMemo } from 'react';
import FullPage from '../page-layout/FullPage';
import AppSnackBar from '../../../ui/alerts/AppSnackBar';
import UnauthorizedNavComponents from '../../../../config/navs/navs-component/UnauthorizedNavsComponent';
import { CustomRoutePageProps } from '../../../../types/interfaces/navigation-pages/NavigationPageProps';
import { CustomParamsRouteKeys } from '../../../../types/types/navigation/routes/CustomParamsRoutesList';
import { PAGES_DICTIONARIES } from '../../../../types/types/navigation/app-page';
import AuthorizedNavComponents from '../../../../config/navs/navs-component/AuthorizedNavsComponents';
import MiddlewareNavsComponents from '../../../../config/navs/navs-component/MiddlewareNavsComponents';
import SequencesNavComponents from '../../../../config/navs/navs-component/SequencesNavsComponents';
import ServiceNavsComponents from '../../../../config/navs/navs-component/ServiceNavsComponents';
import { PAGES_LIST } from '../../../../config/navs/NavsConfig';
import { useAppSelector } from '../../../../store/hooks';

const StackScreen = ({
  route,
}: CustomRoutePageProps<CustomParamsRouteKeys.APP_PAGE>): JSX.Element => {
  const modalOverlayActive = useAppSelector((store) => store.navigationContext.activeModal);
  const SnackBarComponent = useMemo(() => {
    if (modalOverlayActive) {
      return <></>;
    }

    return (
      <AppSnackBar
        inSafeView={
          !route?.params?.navigationInfo?.noMargin &&
          (route?.params?.navigationInfo?.tabBarHidden ||
            route?.params?.navigationInfo?.topNavbarVisible)
        }
      />
    );
  }, [route, modalOverlayActive]);

  const FullPageComponent = useMemo(() => {
    const choosePage = (): React.ReactNode => {
      const compKey = route?.params?.navigationInfo?.component as PAGES_LIST;
      switch (route?.params?.pageDictionary) {
        case PAGES_DICTIONARIES.AUTHORIZED_PAGES:
          return AuthorizedNavComponents[compKey] || ServiceNavsComponents['default'];
        case PAGES_DICTIONARIES.UNAUTHORIZED_PAGES:
          return UnauthorizedNavComponents[compKey] || ServiceNavsComponents['default'];
        case PAGES_DICTIONARIES.MIDDLEWARE_PAGES:
          return MiddlewareNavsComponents[compKey] || ServiceNavsComponents['default'];
        case PAGES_DICTIONARIES.SEQUENCE_PAGES:
          return SequencesNavComponents[compKey] || ServiceNavsComponents['default'];
        case PAGES_DICTIONARIES.SERVICE_PAGES:
          return ServiceNavsComponents[compKey] || ServiceNavsComponents['default'];
        default:
          return ServiceNavsComponents['default'];
      }
    };

    const pageComponent = choosePage();

    return (
      <FullPage
        navigationInfo={route?.params?.navigationInfo}
        externalLink={route?.params?.navigationInfo?.externalLink || undefined}
        page={pageComponent}
        sequenceInfo={route?.params?.sequenceInfo}
      />
    );
  }, [route]);

  return (
    <>
      {FullPageComponent}
      {SnackBarComponent}
    </>
  );
};

export default StackScreen;

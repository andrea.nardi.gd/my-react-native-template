import React from 'react';
import { CustomRoutePageProps } from '../../../../types/interfaces/navigation-pages/NavigationPageProps';
import { CustomParamsRouteKeys } from '../../../../types/types/navigation/routes/CustomParamsRoutesList';
import { Platform } from 'react-native';
import StackScreen from './StackScreen';
import SlideInHorizontal from '../../container-animations/SlideInHorizontal';

const TabAnimationWrapper = ({ children }: { children: React.ReactNode }): JSX.Element => {
  if (Platform.OS === 'android') {
    return <>{children}</>;
  }
  //TODO defaults to slide, add selector based on animation in navigation info
  return <SlideInHorizontal>{children}</SlideInHorizontal>;
};

const TabScreen = ({
  route,
}: CustomRoutePageProps<CustomParamsRouteKeys.APP_PAGE>): JSX.Element => {
  return (
    <TabAnimationWrapper>
      <StackScreen route={route} />
    </TabAnimationWrapper>
  );
};

export default TabScreen;

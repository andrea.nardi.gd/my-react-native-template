import { useFocusEffect, useNavigation, useRoute } from '@react-navigation/native';
import React, { useCallback, useState } from 'react';
import { BackHandler, InteractionManager } from 'react-native';
import { useAppSelector } from '../../../../store/hooks';
import { Navigation } from '../../../../types/types/config/navigation';
import MockComponent from '../../utilities/mock-components/MockComponent';
import FullPageWrapper from './sections/FullPageWrapper';
import PageSequenceHeader from './sections/PageSequenceHeader';
import { useDispatch } from 'react-redux';
import PagesCallbackServices from '../../../../services/local/PagesCallbackServices';
import { StackNavigationHelpers } from '@react-navigation/stack/lib/typescript/src/types';
import { SequenceInfo } from '../../../../types/types/navigation/app-page';

interface FullPageProps {
  externalLink?: string;
  page: React.ReactNode | null;
  sequenceInfo?: SequenceInfo;
  navigationInfo?: Navigation;
}

const FullPage = ({
  navigationInfo,
  page,
  sequenceInfo,
  externalLink,
}: FullPageProps): JSX.Element => {
  if (page) {
    const [showConfirmModal, setShowConfirmModal] = useState<boolean>(false);
    const PageComp = page as React.ElementType;
    const navigation = useNavigation();
    const dispatch = useDispatch();
    const route = useRoute();
    const appThreadLoading = useAppSelector((store) => store.appStatus.appThreadLoading);

    const toggleModal = () => {
      setShowConfirmModal(!showConfirmModal);
    };

    useFocusEffect(
      useCallback(() => {
        const onBackPress = () => {
          return PagesCallbackServices.PageBackButtonHandler({
            navigation: navigation as unknown as StackNavigationHelpers,
            navigationInfo: navigationInfo as Navigation,
            sequenceInfo: sequenceInfo as SequenceInfo,
            backButtonCallback: toggleModal,
            backHandler: BackHandler,
          });
        };

        BackHandler.addEventListener('hardwareBackPress', onBackPress);

        return () => BackHandler.removeEventListener('hardwareBackPress', onBackPress);
      }, [navigationInfo, navigation, sequenceInfo]),
    );

    useFocusEffect(
      useCallback(() => {
        const onFocusCallBack = navigationInfo?.onFocusCallBack;
        const onLeaveCallBack = navigationInfo?.onLeaveCallBack;

        if (onFocusCallBack) {
          InteractionManager.runAfterInteractions(async () => {
            dispatch(
              PagesCallbackServices.ExecutePagesCallback({
                callbackInfo: onFocusCallBack,
                navigation: navigation as unknown as StackNavigationHelpers,
              }),
            );
          });
        }

        return () => {
          if (onLeaveCallBack) {
            InteractionManager.runAfterInteractions(async () => {
              dispatch(
                PagesCallbackServices.ExecutePagesCallback({
                  callbackInfo: onLeaveCallBack,
                  navigation: navigation as unknown as StackNavigationHelpers,
                }),
              );
            });
          }
        };
      }, [navigationInfo, navigation]),
    );

    return (
      <FullPageWrapper
        sequenceInfo={sequenceInfo}
        externalLink={externalLink}
        navigationInfo={navigationInfo}
        appThreadLoading={appThreadLoading}
        showConfirmModal={showConfirmModal}
        toggleModal={toggleModal}
      >
        <PageSequenceHeader
          sequenceInfo={sequenceInfo}
          navigationInfo={navigationInfo as Navigation}
        />
        <PageComp
          navigation={navigation}
          navProperties={navigationInfo}
          externalLinkKey={externalLink}
          sequenceInfo={sequenceInfo}
          sequenceStep={sequenceInfo?.steps[sequenceInfo.currentStep]}
          route={route}
        />
      </FullPageWrapper>
    );
  } else {
    return <MockComponent />;
  }
};

export default FullPage;

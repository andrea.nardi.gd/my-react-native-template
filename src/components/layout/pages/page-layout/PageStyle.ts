import { StyleSheet } from 'react-native';
import { AppColors } from '../../../../config/styles/GlobalStyles';

export const CommonPageStyles = StyleSheet.create({
  pageTitle: {
    fontSize: 32,
    lineHeight: 34,
    //fontWeight: '700',
    //marginTop: 32,
    marginBottom: 32,
    color: AppColors.primary500,
    marginLeft: 10,
    marginRight: 20,
    marginTop: 20,
  },
  pageTitleLeft: {
    fontSize: 32,
    lineHeight: 34,
    //fontWeight: '700',
    //marginTop: 32,
    textAlign: 'left',
    marginBottom: 10,
    color: AppColors.primary500,
    marginLeft: 10,
    marginRight: 10,
    marginTop: 20,
  },

  pageTitleBlack: {
    fontSize: 24,
    lineHeight: 28,
    marginBottom: 0,
    color: AppColors.ink900,
    marginLeft: 20,
    marginRight: 20,
    marginTop: 20,
  },

  pageSubtitle: {
    fontSize: 20,
    lineHeight: 24,
    marginBottom: 32,
  },

  pageSubtitleH3: {
    fontSize: 16,
    lineHeight: 18,
    //fontWeight: '400',
    marginLeft: 20,
    marginBottom: 32,
  },
  iconContainer: {
    alignContent: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
    paddingLeft: 10,
    paddingRight: 10,
  },
  alertText: {
    flex: 1,
    color: AppColors.secondary900,
    alignSelf: 'center',
    alignContent: 'center',
    justifyContent: 'center',
    paddingBottom: 2,
    paddingTop: 2,
    //fontWeight: 'bold',
  },
});

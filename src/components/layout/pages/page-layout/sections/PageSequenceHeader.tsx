import React from 'react';
import { useTranslation } from 'react-i18next';
import { Navigation } from '../../../../../types/types/config/navigation';
import { SequenceInfo } from '../../../../../types/types/navigation/app-page';
import SequenceProgress from '../../../../ui/sequence/SequenceProgress';
import AppText from '../../../text/AppText';

const PageSequenceHeader = ({
  sequenceInfo,
  navigationInfo,
}: {
  sequenceInfo?: SequenceInfo;
  navigationInfo: Navigation;
}): JSX.Element => {
  const { t } = useTranslation();
  if (sequenceInfo) {
    return (
      <>
        <AppText fontWeight="bold" style={{ fontSize: 24, lineHeight: 26, margin: 20 }}>
          {navigationInfo?.title && navigationInfo.showTitle ? t(navigationInfo.title) : ''}
        </AppText>
        <SequenceProgress currentStep={sequenceInfo.currentStep} stepLabels={sequenceInfo.steps} />
      </>
    );
  }
  return <></>;
};

export default PageSequenceHeader;

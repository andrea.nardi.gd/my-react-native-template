import { BigNumber } from 'bignumber.js';
import React from 'react';
import { Modal, View } from 'react-native';
import { globalStyles } from '../../../../../config/styles/GlobalStyles';
import { useAppSelector } from '../../../../../store/hooks';
import { SizeVariants } from '../../../../../types/types/styles/sizes';
import { ColorVariants } from '../../../../../types/types/styles/themes';
import AppSpinner from '../../../../utils/AppSpinner';
import AppText from '../../../text/AppText';

const AppThreadLoadingModal = ({ visible }: { visible: boolean }): JSX.Element => {
  const AppColors = useAppSelector((store) => store.theme.AppColors);
  const progress = useAppSelector((store) => store.appStatus.threadLoadingProgress);
  return (
    <Modal transparent={true} visible={visible}>
      <View
        style={{
          backgroundColor: globalStyles.MODALS.backgroundColor,
          flex: 1,
          justifyContent: 'center',
          alignContent: 'center',
          alignItems: 'center',
          width: '100%',
        }}
      >
        <View
          style={{
            height: '100%',
            width: '100%',
            borderRadius: 10,
            justifyContent: 'center',
            alignContent: 'center',
            alignItems: 'center',
          }}
        >
          <AppSpinner size={SizeVariants.SMALL} color={ColorVariants.SECONDARY} />
          {visible && progress ? (
            <AppText style={{ color: AppColors.secondary900 }}>
              {new BigNumber(progress).times(100).toFixed(0).toString()} / 100
            </AppText>
          ) : (
            <></>
          )}
        </View>
      </View>
    </Modal>
  );
};

export default AppThreadLoadingModal;

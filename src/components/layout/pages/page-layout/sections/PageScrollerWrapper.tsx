import React, { useCallback, useState } from 'react';
import { RefreshControl, View } from 'react-native';
import { Navigation } from '../../../../../types/types/config/navigation';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { useDispatch } from 'react-redux';
import PagesCallbackServices from '../../../../../services/local/PagesCallbackServices';
import { useAppSelector } from '../../../../../store/hooks';

interface PageScrollerWrapperProps {
  navigationInfo?: Navigation;
  children: React.ReactNode[] | React.ReactNode;
}

const PageScrollerWrapper = ({
  navigationInfo,
  children,
}: PageScrollerWrapperProps): JSX.Element => {
  const AppColors = useAppSelector((store) => store.theme.AppColors);

  const [refreshing, setRefreshing] = useState(false);
  const dispatch = useDispatch();

  const setRefreshStatus = useCallback((status: boolean) => {
    setRefreshing(status);
  }, []);

  const pageRefreshCallback = useCallback(() => {
    if (navigationInfo?.onScrollCallback) {
      const onScrollCallback = navigationInfo?.onScrollCallback;
      dispatch(
        PagesCallbackServices.ExecutePageScrollCallback({
          callbackInfo: onScrollCallback,
          loaderStateSetter: setRefreshStatus,
        }),
      );
    }
  }, [navigationInfo, setRefreshStatus]);

  return (
    <KeyboardAwareScrollView
      style={{
        backgroundColor: navigationInfo?.pageBackgroundColor || AppColors.secondary100,
      }}
      refreshControl={
        navigationInfo?.onScrollCallback ? (
          <RefreshControl refreshing={refreshing} onRefresh={pageRefreshCallback} />
        ) : (
          <></>
        )
      }
    >
      <View
        style={{
          flex: 1,
          margin: navigationInfo?.noMargin ? 0 : 10,
          backgroundColor: AppColors.secondary100,
        }}
      >
        {children}
      </View>
    </KeyboardAwareScrollView>
  );
};

export default PageScrollerWrapper;

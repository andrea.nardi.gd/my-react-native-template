import React from 'react';
import { View } from 'react-native';
import { WarningDict, WarningEntry } from '../../../../../config/modals/warnings/WarningsDict';
import { globalStyles } from '../../../../../config/styles/GlobalStyles';
import { useAppSelector } from '../../../../../store/hooks';
import { Navigation } from '../../../../../types/types/config/navigation';
import { SequenceInfo } from '../../../../../types/types/navigation/app-page';
import AlertModal from '../../../../modals/AlertModal';
import AppThreadLoadingModal from './AppThreadLoadingModal';
import PageScrollerWrapper from './PageScrollerWrapper';

interface FullPageProps {
  externalLink?: string;
  sequenceInfo?: SequenceInfo;
  navigationInfo?: Navigation;
  children: JSX.Element[] | JSX.Element;
  appThreadLoading: boolean;
  showConfirmModal: boolean;
  toggleModal: () => void;
}

const SafeAreaWrapper = ({
  children,
  sequenceInfo,
  navigationInfo,
}: {
  navigationInfo?: Navigation;
  sequenceInfo?: SequenceInfo;
  children: JSX.Element[] | JSX.Element;
}): JSX.Element => {
  const AppColors = useAppSelector((store) => store.theme.AppColors);
  if (sequenceInfo || navigationInfo?.noMargin) {
    return (
      <>
        <View
          style={{
            flexGrow: 100,
            flexShrink: 0,
            flexBasis: '100%',
            flex: 100,
            backgroundColor: navigationInfo?.pageBackgroundColor || AppColors.secondary100,
          }}
        >
          {children}
        </View>
      </>
    );
  }
  return <View style={{ flex: 1 }}>{children}</View>;
};

const FullPageViewWrapper = ({
  navigationInfo,
  children,
  externalLink,
}: {
  sequenceInfo?: SequenceInfo;
  navigationInfo?: Navigation;
  children: JSX.Element[] | JSX.Element;
  externalLink?: string;
}): JSX.Element => {
  const AppColors = useAppSelector((store) => store.theme.AppColors);

  if (externalLink || navigationInfo?.noScroll) {
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: navigationInfo?.pageBackgroundColor || AppColors.secondary100,
        }}
      >
        {children}
      </View>
    );
  }
  return <PageScrollerWrapper navigationInfo={navigationInfo}>{children}</PageScrollerWrapper>;
};

const FullPageWrapper = ({
  children,
  sequenceInfo,
  navigationInfo,
  externalLink,
  appThreadLoading,
  showConfirmModal,
  toggleModal,
}: FullPageProps): JSX.Element => {
  return (
    <SafeAreaWrapper sequenceInfo={sequenceInfo} navigationInfo={navigationInfo}>
      <View style={{ flex: 1, backgroundColor: globalStyles.UNSAFE_AREA_TOP_COLOR }}>
        <FullPageViewWrapper externalLink={externalLink} navigationInfo={navigationInfo}>
          {children}
        </FullPageViewWrapper>
      </View>
      <AppThreadLoadingModal visible={appThreadLoading} />
      <AlertModal
        isVisible={showConfirmModal}
        toggleModal={toggleModal}
        alertContent={WarningDict.TEST_WARNING_ENTRY_KEY as WarningEntry}
        isGoBackAlert={true}
        goBackOverride={navigationInfo?.backButtonDestination}
      />
    </SafeAreaWrapper>
  );
};

export default FullPageWrapper;

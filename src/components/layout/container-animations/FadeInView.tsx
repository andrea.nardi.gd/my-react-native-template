import React from 'react';
import Animated, { FadeIn } from 'react-native-reanimated';

const FadeInView = ({
  children,
  fadeInDuration = 400,
  damping = 10,
}: {
  fadeInDuration?: number;
  fadeOutDuration?: number;
  damping?: number;
  children: React.ReactNode | React.ReactNode[];
}): JSX.Element => {
  return (
    <Animated.View
      entering={FadeIn.duration(fadeInDuration).damping(damping)}
      style={{
        flex: 1,
      }}
    >
      {children}
    </Animated.View>
  );
};

export default FadeInView;

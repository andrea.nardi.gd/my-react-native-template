import React from 'react';
import Animated, { SlideInRight } from 'react-native-reanimated';

const SlideInHorizontal = ({
  children,
  fadeInDuration,
}: {
  fadeInDuration?: number;
  fadeOutDuration?: number;
  children: React.ReactNode | React.ReactNode[];
}): JSX.Element => {
  return (
    <Animated.View
      entering={SlideInRight.duration(fadeInDuration || 300)}
      style={{
        flex: 1,
      }}
    >
      {children}
    </Animated.View>
  );
};

export default SlideInHorizontal;

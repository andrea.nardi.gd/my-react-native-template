import React from 'react';
import Animated, { SlideInUp, SlideOutDown } from 'react-native-reanimated';

const SlideInVertical = ({
  children,
}: {
  fadeInDuration?: number;
  fadeOutDuration?: number;
  children: React.ReactNode | React.ReactNode[];
}): JSX.Element => {
  return (
    <Animated.View entering={SlideInUp} exiting={SlideOutDown}>
      {children}
    </Animated.View>
  );
};

export default SlideInVertical;

import React, { useEffect, useMemo, useState } from 'react';
import { Platform, View } from 'react-native';
import {
  authenticatedBottomTabNavs,
  unAuthenticatedBottomTabNavs,
} from '../../config/navs/BottomTabNavs';
import { globalStyles } from '../../config/styles/GlobalStyles';
import { Navigation } from '../../types/types/config/navigation';
import TabBarIcon from './tab/TabBarIcon';
import { Keyboard } from 'react-native';
import { PAGES_LIST } from '../../config/navs/NavsConfig';
import SlideInVertical from './container-animations/SlideInVertical';
import { useAppSelector } from '../../store/hooks';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import AlertModal from '../modals/AlertModal';
import { WarningDict, WarningEntry } from '../../config/modals/warnings/WarningsDict';
import { BottomTabNavigationHelpers } from '@react-navigation/bottom-tabs/lib/typescript/src/types';

const AppTabBar = ({ navigation }: { navigation: BottomTabNavigationHelpers }): JSX.Element => {
  const AppColors = useAppSelector((store) => store.theme.AppColors);

  const activeScreen = useAppSelector(
    (store) => store.navigationContext.activeScreen,
  ) as Navigation;
  const hidingConditionSatisfied = false; //TODO add a hook here to hide some tab bar icons
  const bottomInset = useSafeAreaInsets().bottom;

  const [isKeyboardVisible, setKeyboardVisible] = useState(false);
  const isAuthorized = useAppSelector((store) => store.localAuth.isUserAuthorized);

  const [showConfirmModal, setShowConfirmModal] = useState(false);
  const [pendingDestination, setPendingDestination] = useState<string | undefined>(undefined);
  const [pendingGoBack, setPendingGoBack] = useState(false);

  const links = useMemo(() => {
    //TODO could extend for middlewares pages too
    if (isAuthorized) {
      return Object.keys(authenticatedBottomTabNavs) as PAGES_LIST[];
    }
    return Object.keys(unAuthenticatedBottomTabNavs) as PAGES_LIST[];
  }, []);

  const toggleModal = () => {
    setShowConfirmModal(!showConfirmModal);
    setPendingDestination(undefined);
  };

  const setConfirmModalDestination = (destination: string, isGoBack = false) => {
    if (!showConfirmModal) {
      setPendingDestination(destination);
      if (isGoBack) {
        setPendingGoBack(true);
      }
      setShowConfirmModal(true);
    }
  };

  useEffect(() => {
    const keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', () => {
      setKeyboardVisible(true);
    });
    const keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', () => {
      setKeyboardVisible(false);
    });

    return () => {
      keyboardDidHideListener.remove();
      keyboardDidShowListener.remove();
    };
  }, []);

  const deferredNavigation = (navLink: Navigation) => {
    if (activeScreen?.askConfirmationOnChange) {
      setConfirmModalDestination(navLink.name);
    } else {
      navigation?.navigate(navLink.name as never);
    }
  };

  const icons = useMemo(() => {
    const tempIcons: JSX.Element[] = [];
    links.forEach((link) => {
      const properties = authenticatedBottomTabNavs[link] as Navigation;

      tempIcons.push(
        <TabBarIcon
          key={link + '_icon'}
          deferredNavigation={deferredNavigation}
          activeScreen={activeScreen}
          properties={properties}
          hidingConditionSatisfied={hidingConditionSatisfied}
        />,
      );
    });
    return [...tempIcons];
  }, [activeScreen, links]);

  if (icons.length === 0 || (isKeyboardVisible && Platform.OS === 'android')) {
    return <></>;
  }

  return (
    <>
      <AlertModal
        isVisible={showConfirmModal}
        toggleModal={toggleModal}
        overrideRedirect={pendingDestination}
        alertContent={WarningDict.TEST_WARNING_ENTRY_KEY as WarningEntry}
        isGoBackAlert={pendingGoBack}
      />
      <SlideInVertical fadeInDuration={300}>
        <View
          style={{
            paddingTop: globalStyles.APP_TAB_BAR.icons.size / 2,
            paddingBottom:
              globalStyles.APP_TAB_BAR.icons.size / 2 +
              (bottomInset && bottomInset > globalStyles.IOS_MEASURES.statusBar
                ? bottomInset - globalStyles.IOS_MEASURES.statusBar
                : 0),
            backgroundColor: AppColors.secondary900,
            width: '100%',
            marginTop: 'auto',
            display: activeScreen?.tabBarHidden ? 'none' : 'flex',
            flexDirection: 'row',
            justifyContent: 'space-evenly',
            zIndex: 100,
            alignContent: 'center',
            alignItems: 'center',
            ...globalStyles.SHADOWS.top.high,
          }}
        >
          {icons.map((icon) => icon)}
        </View>
      </SlideInVertical>
    </>
  );
};

export default AppTabBar;

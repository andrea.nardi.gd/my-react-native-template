import React from 'react';
import { View } from 'react-native';
import { SizeVariants } from '../../../types/types/styles/sizes';

interface VerticalSeparatorProps {
  distance?: SizeVariants | 'safe-view-link-ios';
  backgroundColor?: string;
  negative?: boolean;
}

const VerticalSeparator = ({
  distance = SizeVariants.MEDIUM,
  backgroundColor,
}: VerticalSeparatorProps): JSX.Element => {
  const chooseHeight = () => {
    switch (distance) {
      case SizeVariants.MINI:
        return 5;
      case SizeVariants.SMALL:
        return 10;
      case SizeVariants.LARGE:
        return 50;
      case 'safe-view-link-ios':
        return 20;
      case SizeVariants.MEDIUM:
      default:
        return 25;
    }
  };

  return (
    <View
      style={{
        width: '100%',
        height: chooseHeight(),
        ...(backgroundColor && { backgroundColor: backgroundColor }),
      }}
    />
  );
};

export default VerticalSeparator;

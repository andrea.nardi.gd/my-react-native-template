import React from 'react';
import { KeyboardAvoidingView, Platform, ViewStyle } from 'react-native';

interface PlatformAwareKeyboardAvoidingViewProps {
  children: React.ReactNode | React.ReactNode[];
  style?: ViewStyle;
  inModal?: boolean;
}

const PlatformAwareKeyboardAvoidingView = ({
  children,
  style,
  inModal = false,
}: PlatformAwareKeyboardAvoidingViewProps): JSX.Element => {
  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? (inModal ? 'position' : 'padding') : 'height'}
      contentContainerStyle={Platform.OS === 'ios' && inModal ? style : {}}
      style={Platform.OS === 'android' || !inModal ? style : {}}
    >
      {children}
    </KeyboardAvoidingView>
  );
};

export default PlatformAwareKeyboardAvoidingView;

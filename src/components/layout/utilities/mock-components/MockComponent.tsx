import React from 'react';
import { SafeAreaView } from 'react-native-safe-area-context';

const MockComponent = (): JSX.Element => {
  return <SafeAreaView style={{ flex: 1, backgroundColor: 'transparent' }} />;
};

export default MockComponent;

import React from 'react';
import { StyleProp, View, ViewStyle } from 'react-native';

type FlexSimplifiedPositions = 'flex-start' | 'center' | 'flex-end';
type FlexDirections = 'row' | 'row-reverse' | 'column' | 'column-reverse';

interface FlexAlignViewProps {
  children: React.ReactNode | React.ReactNode[];
  direction?: FlexDirections;
  horizontal?: FlexSimplifiedPositions;
  vertical?: FlexSimplifiedPositions;
  alignSelf?: FlexSimplifiedPositions;
  style?: StyleProp<ViewStyle>;
}

const FlexAlignView = ({
  children,
  direction = 'row',
  horizontal = 'center',
  vertical = 'center',
  alignSelf = undefined,
  style,
}: FlexAlignViewProps): JSX.Element => {
  return (
    <View
      style={[
        {
          width: '100%',
          flexDirection: direction,
          justifyContent: horizontal,
          alignContent: horizontal,
          alignItems: vertical,
          ...(alignSelf && { alignSelf: alignSelf }),
        },
        style || {},
      ]}
    >
      {children}
    </View>
  );
};

export default FlexAlignView;

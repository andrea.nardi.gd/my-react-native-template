import React from 'react';
import { View } from 'react-native';
import { useAppSelector } from '../../../store/hooks';

const BarcodeScannerArea = (): JSX.Element => {
  const AppColors = useAppSelector((store) => store.theme.AppColors);

  return (
    <View
      style={{
        backgroundColor: 'rgba(255, 255, 255, 0.05)',
        width: 220,
        height: 220,
        display: 'flex',
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center',
      }}
    >
      <View
        style={{
          width: '90%',
          height: 1,
          backgroundColor: AppColors.primary400,
        }}
      />
      <View
        style={{
          position: 'absolute',
          top: -3,
          left: -3,
          borderLeftWidth: 3,
          borderTopWidth: 3,
          height: 30,
          width: 30,
          borderColor: AppColors.secondary100,
        }}
      ></View>
      <View
        style={{
          position: 'absolute',
          top: -3,
          right: -3,
          borderRightWidth: 3,
          borderTopWidth: 3,
          height: 30,
          width: 30,
          borderColor: AppColors.secondary100,
        }}
      ></View>
      <View
        style={{
          position: 'absolute',
          bottom: -3,
          left: -3,
          borderLeftWidth: 3,
          borderBottomWidth: 3,
          height: 30,
          width: 30,
          borderColor: AppColors.secondary100,
        }}
      ></View>
      <View
        style={{
          position: 'absolute',
          bottom: -3,
          right: -3,
          borderRightWidth: 3,
          borderBottomWidth: 3,
          height: 30,
          width: 30,
          borderColor: AppColors.secondary100,
        }}
      ></View>
    </View>
  );
};

export default BarcodeScannerArea;

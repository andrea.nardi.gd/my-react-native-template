import React from 'react';
import { useTranslation } from 'react-i18next';
import { TopTabOptions, TopTabRoute } from '../../../types/types/navigation/navigators/topTab';
import UI_Icons from '../../icons';
import AppText from '../text/AppText';
import FlexAlignView from '../utilities/FlexAlignView';

interface TopTabBarTitleProps {
  options: TopTabOptions;
  activeTab: TopTabRoute;
}

interface TopTabIconProps {
  iconName?: string;
  iconColor: string;
  iconSize?: number;
}

const DEFAULT_ICON_SIZE = 24;

const TopTabIcon = ({ iconName, iconColor, iconSize }: TopTabIconProps): JSX.Element => {
  return UI_Icons.TOP_BAR[iconName as keyof typeof UI_Icons.TOP_BAR] ? (
    UI_Icons.TOP_BAR[iconName as keyof typeof UI_Icons.TOP_BAR](iconColor, iconSize)
  ) : (
    <></>
  );
};

const TopTabBarTitle = ({ options, activeTab }: TopTabBarTitleProps): JSX.Element => {
  const { t } = useTranslation();
  return (
    <FlexAlignView alignSelf="center">
      {options.showIcon && (
        <TopTabIcon
          iconName={activeTab.icon}
          iconColor={options.titleTextColor as string}
          iconSize={options.iconSize || DEFAULT_ICON_SIZE}
        />
      )}
      {options.showText && (
        <AppText style={{ color: options.titleTextColor }}>{t(activeTab.title)}</AppText>
      )}
    </FlexAlignView>
  );
};

export default TopTabBarTitle;

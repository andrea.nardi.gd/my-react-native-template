import React from 'react';
import { Pressable } from 'react-native';
import { globalStyles } from '../../../config/styles/GlobalStyles';
import { Navigation } from '../../../types/types/config/navigation';
import authConfig from '../../../pages/authorized-pages/config';

interface TabBarIconProps {
  //link: string;
  properties: Navigation;
  deferredNavigation: (navLink: Navigation) => void;
  activeScreen: Navigation;
  hidingConditionSatisfied: boolean;
}

interface TabBarIconRendererProps {
  //link: string;
  properties: Navigation;
  activeScreen: Navigation;
}

const IconRenderer = ({ properties, activeScreen }: TabBarIconRendererProps): JSX.Element => {
  if (properties.icon) {
    return authConfig.ICONS[properties.icon as keyof typeof authConfig.ICONS](
      activeScreen?.section === properties.section
        ? globalStyles.APP_TAB_BAR.icons.activeColor
        : globalStyles.APP_TAB_BAR.icons.inactiveColor,
      globalStyles.APP_TAB_BAR.icons.size,
    );
  }
  return <></>;
};

const TabBarIcon = ({
  //link,
  properties,
  deferredNavigation,
  activeScreen,
  hidingConditionSatisfied,
}: TabBarIconProps): JSX.Element => {
  const navOnPress = () => {
    deferredNavigation(properties);
  };

  if (!(properties.hasHidingCondition && hidingConditionSatisfied)) {
    return (
      <Pressable
        style={{
          flex: 1,
          display: 'flex',
          justifyContent: 'center',
          alignContent: 'center',
          alignItems: 'center',
        }}
        onPress={navOnPress}
      >
        <IconRenderer properties={properties} activeScreen={activeScreen} />
      </Pressable>
    );
  }
  return <></>;
};

export default TabBarIcon;

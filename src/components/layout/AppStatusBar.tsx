import React, { useMemo } from 'react';
import { StatusBar } from 'expo-status-bar';
import { globalStyles } from '../../config/styles/GlobalStyles';
import { Navigation } from '../../types/types/config/navigation';
import { ThemeVariants } from '../../types/types/styles/themes';
import { Platform, View } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { useAppSelector } from '../../store/hooks';

interface AppStatusBarProp {
  activePage?: Navigation;
  isHiddenNavbar?: boolean;
  isPrimaryNavbar?: boolean;
  isDarkNavbar?: boolean;
  isTransparent?: boolean;
}

const AppStatusBar = ({
  activePage,
  isHiddenNavbar = false,
  isPrimaryNavbar = false,
  isDarkNavbar = false,
  isTransparent = false,
}: AppStatusBarProp): JSX.Element => {
  const AppColors = useAppSelector((store) => store.theme.AppColors);
  const useInset = useSafeAreaInsets();
  const backgroundColor = useMemo(() => {
    if (activePage?.statusBarBgColor) {
      return activePage?.statusBarBgColor;
    }

    if (activePage?.noMargin) {
      return activePage.pageBackgroundColor
        ? activePage.pageBackgroundColor
        : AppColors.secondary100;
    }
    return AppColors.secondary100;
  }, [activePage]);

  const style = useMemo(() => {
    if (activePage?.noMargin) {
      return activePage.pageBackgroundColor &&
        activePage.pageBackgroundColor !== AppColors.secondary100
        ? ThemeVariants.LIGHT
        : ThemeVariants.DARK;
    }
    return ThemeVariants.DARK;
  }, [activePage, AppColors]);

  const StatusBarComponent = useMemo(() => {
    if (isPrimaryNavbar) {
      return <StatusBar style={ThemeVariants.LIGHT} backgroundColor={AppColors.primary500} />;
    }

    if (isHiddenNavbar) {
      return <StatusBar style={ThemeVariants.LIGHT} backgroundColor={AppColors.ink500} hidden />;
    }

    if (isDarkNavbar) {
      return <StatusBar style={ThemeVariants.LIGHT} backgroundColor={AppColors.ink900} />;
    }

    if (isTransparent) {
      return <StatusBar style={ThemeVariants.LIGHT} backgroundColor={'transparent'} />;
    }

    return <StatusBar style={style} backgroundColor={backgroundColor} />;
  }, [
    backgroundColor,
    style,
    isPrimaryNavbar,
    isHiddenNavbar,
    isDarkNavbar,
    AppColors,
    isTransparent,
  ]);

  return (
    <>
      {Platform.OS === 'ios' && activePage?.noMargin && (
        <View
          style={{
            position: 'absolute',
            width: '100%',
            height: useInset.top || globalStyles.IOS_MEASURES.statusBar,
            top: 0,
            backgroundColor: activePage?.statusBarBgColor
              ? activePage.statusBarBgColor
              : activePage?.pageBackgroundColor,
            zIndex: 1000,
          }}
        />
      )}
      {StatusBarComponent}
    </>
  );
};

export default AppStatusBar;

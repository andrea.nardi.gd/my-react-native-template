import React from 'react';
import AppServices from '../../services/local/AppServices';
import SafeImageWrapper from './app-image/SafeImageWrapper';
import NativeImageWrapper from './app-image/NativeImageWrapper';
import { FastImageProps } from 'react-native-fast-image';
import { ImageProps as RNElementsImageProps } from 'react-native-elements';
import { ImageProps } from 'react-native';

interface ExtendedImageProps extends RNElementsImageProps, ImageProps {
  width?: number;
  height?: number;
  useAltImage?: boolean;
  forceSafeImage?: boolean;
}

const purgeProps = ({ ...props }: ExtendedImageProps): FastImageProps => {
  delete props.width;
  delete props.height;
  return { ...props } as FastImageProps;
};

const AppImage = ({ ...props }: ExtendedImageProps): JSX.Element => {
  if (AppServices.IsRunningOnExpoGo() || props.forceSafeImage) {
    return <SafeImageWrapper {...props} />;
  }

  return <NativeImageWrapper {...purgeProps({ ...props })} />;
};

export default AppImage;

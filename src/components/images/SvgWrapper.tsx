import React from 'react';
import { SvgProps } from 'react-native-svg';
import { View, ViewStyle } from 'react-native';

interface SvgWrapperProps {
  svgComponent: React.ReactNode;
  containerStyle: ViewStyle;
  svgProps?: SvgProps & ViewStyle & { crop?: boolean; color?: string; secondaryColor?: string };
}

const SvgWrapper = ({ svgComponent, svgProps, containerStyle }: SvgWrapperProps): JSX.Element => {
  const SvgComp = svgComponent as React.ElementType;
  return (
    <View style={[containerStyle]}>
      <SvgComp {...svgProps} />
    </View>
  );
};

export default SvgWrapper;

import React from 'react';
import { Image, ImageProps } from 'react-native';
import { Image as AltImage, ImageProps as RNElementsImageProps } from 'react-native-elements';

interface ExtendedImageProps extends RNElementsImageProps, ImageProps {
  width?: number;
  height?: number;
  useAltImage?: boolean;
}

const SafeImageWrapper = ({ ...props }: ExtendedImageProps): JSX.Element => {
  if (props.useAltImage) {
    //@ts-ignore
    return <AltImage {...props} />;
  }
  return <Image {...props} />;
};

export default SafeImageWrapper;

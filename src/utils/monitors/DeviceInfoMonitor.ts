import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import DeviceInfoServices from '../../services/local/DeviceInfoServices';
import { AppHooksReturn } from '../../types/types/hooks/AppHooks';

const DeviceInfoMonitor = (): AppHooksReturn => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(DeviceInfoServices.getDeviceInfo());
  }, []);
};

export default DeviceInfoMonitor;

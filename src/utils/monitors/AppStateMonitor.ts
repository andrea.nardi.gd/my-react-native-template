import { useRef, useEffect } from 'react';
import { AppState, AppStateStatus } from 'react-native';
import { useDispatch } from 'react-redux';
import { setActivityState } from '../../actions/app-status';
import Logger from '../../services/local/Logger';
import { useAppSelector } from '../../store/hooks';
import { AppHooksReturn } from '../../types/types/hooks/AppHooks';

const AppStateMonitor = (): AppHooksReturn => {
  const appState = useRef(AppState.currentState);
  const activityState = useAppSelector((store) => store.appStatus.activityState);
  const dispatch = useDispatch();

  useEffect(() => {
    const subscription = AppState.addEventListener('change', _handleAppStateChange);
    return () => {
      subscription.remove();
    };
  }, []);

  const _handleAppStateChange = (nextAppState: AppStateStatus) => {
    if (appState.current.match(/inactive|background/) && nextAppState === 'active') {
      Logger.LogInfo({ args: 'App has come to the foreground', prefixes: 'STATE MONITOR' });
    }

    appState.current = nextAppState;
    Logger.LogInfo({
      args: ['Current app state => ', appState.current],
      prefixes: 'STATE MONITOR',
    });

    if (activityState !== appState.current) {
      dispatch(setActivityState(appState.current));
    }
  };
};

export default AppStateMonitor;

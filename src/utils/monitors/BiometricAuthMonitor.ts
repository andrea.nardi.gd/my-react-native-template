import { useCallback, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import LocalAuthServices from '../../services/local/LocalAuthServices';
import { StorageAdapter } from '../../services/local/StorageAdapter';
import { useAppSelector } from '../../store/hooks';
import { AppHooksReturn } from '../../types/types/hooks/AppHooks';

const BiometricAuthMonitor = (): AppHooksReturn => {
  const localAuth = useAppSelector((store) => store.localAuth);
  const dispatch = useDispatch();

  const CheckEnabledStatus = useCallback(async () => {
    const _storage = new StorageAdapter();
    const isEnabled = false; //TODO maybe read this from storage in the future if it's going to be implemented

    if (isEnabled) {
      dispatch(LocalAuthServices.setLocalAuthEnabled(true));
    } else {
      dispatch(LocalAuthServices.setLocalAuthEnabled(false));
    }
  }, [dispatch]);

  useEffect(() => {
    CheckEnabledStatus();
  }, []);

  useEffect(() => {
    if (localAuth.biometricAuthPossible) {
      dispatch(LocalAuthServices.setLocalAuthEnrolled());
    } else {
      dispatch(LocalAuthServices.setLocalAuthPossibility());
    }
  }, [localAuth.biometricAuthPossible]);
};

export default BiometricAuthMonitor;

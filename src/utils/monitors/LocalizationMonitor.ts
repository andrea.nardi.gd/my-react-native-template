import { useEffect } from 'react';
import { useAppSelector } from '../../store/hooks';
import { AppHooksReturn } from '../../types/types/hooks/AppHooks';
import i18next from 'i18next';

const LocalizationMonitor = (): AppHooksReturn => {
  const appCurrentLanguage = useAppSelector((store) => store.localization.lang);

  useEffect(() => {
    if (appCurrentLanguage !== i18next.language) {
      i18next.changeLanguage(appCurrentLanguage);
    }
  }, [appCurrentLanguage]);
};

export default LocalizationMonitor;

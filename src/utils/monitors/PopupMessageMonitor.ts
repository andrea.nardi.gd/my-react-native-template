import { useEffect, useRef } from 'react';
import { useAppSelector } from '../../store/hooks';
import { AppHooksReturn } from '../../types/types/hooks/AppHooks';
import SnackBarConstants from '../../config/constants/messages';
import { SnackBarMessage } from '../../types/types/messages';
import { useDispatch } from 'react-redux';
import MessageServices from '../../services/local/MessageServices';

const getSnackbarDurationInSeconds = (activeSnackbar: SnackBarMessage): number => {
  if (activeSnackbar?.durationInSeconds) {
    return activeSnackbar.durationInSeconds;
  } else {
    return SnackBarConstants.durationInSeconds;
  }
};

const SnackBarHook = (): AppHooksReturn => {
  const activeSnackBar = useAppSelector((state) => state.messageContext.activeSnackBar);
  const timeoutRef = useRef<NodeJS.Timeout>();
  const dispatch = useDispatch();

  useEffect(() => {
    if (activeSnackBar) {
      clearTimeout(timeoutRef.current);
      const timeoutId = setTimeout(() => {
        dispatch(MessageServices.hideSnackBarMessage());
      }, getSnackbarDurationInSeconds(activeSnackBar) * 1000);
      timeoutRef.current = timeoutId;
    }
  }, [activeSnackBar]);
};

export default SnackBarHook;

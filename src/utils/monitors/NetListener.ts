import { useEffect } from 'react';
import NetInfo from '@react-native-community/netinfo';
import { AppHooksReturn } from '../../types/types/hooks/AppHooks';
import ConnectionInfoService from '../../services/local/ConnectionInfoServices';
import { useDispatch } from 'react-redux';

const NetListener = (): AppHooksReturn => {
  const dispatch = useDispatch();
  useEffect(() => {
    const unsubscribeNetListener = NetInfo.addEventListener((state) => {
      dispatch(ConnectionInfoService.setNetworkInformation(state));
    });
    return () => unsubscribeNetListener();
  }, []);
};

export default NetListener;

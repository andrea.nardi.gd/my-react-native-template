import { compact, slice } from 'lodash';

export const chunkWithBreakCondition = <T>(
  data: readonly T[],
  size = 1,
  callback: (item: T) => boolean,
): T[][] => {
  const length = data == null ? 0 : data.length;
  if (!length || size < 1) {
    return [];
  }
  let index = 0;
  let resIndex = 0;
  const result = new Array(length);

  while (index < length) {
    const shouldBreakInAdvance = callback(data[index]);
    result[resIndex++] = slice(data, index, (index += shouldBreakInAdvance ? 1 : size));
  }
  return compact(result);
};

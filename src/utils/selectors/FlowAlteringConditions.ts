import { useAppSelector } from '../../store/hooks';
import {
  FlowAlteringConditionsKeys,
  SimplePermissionValues,
} from '../../types/types/config/FlowAlteringConditions';

const FlowAlteringConditions = (): Partial<
  Record<FlowAlteringConditionsKeys, SimplePermissionValues>
> => {
  const isAuthorized = useAppSelector((store) => store.localAuth.isUserAuthorized);
  return {
    [FlowAlteringConditionsKeys.AUTHORIZED]: isAuthorized,
  };
};
export default FlowAlteringConditions;

import { useCallback, useEffect, useState } from 'react';
import { StorageAdapter } from '../../services/local/StorageAdapter';

const isPinSetHook = (): boolean | null => {
  const [isPinSet, setIsPinSet] = useState<boolean | null>(null);
  const checkPin = useCallback(async () => {
    const _storage = new StorageAdapter();
    //const pin = await storage.get(config.STORAGE_KEYS.pin);
    const pin = false; //TODO add storage retrieval for pin if implemented (almost certainly not)

    if (pin) {
      setIsPinSet(true);
    }
  }, []);
  useEffect(() => {
    checkPin();
  }, []);

  return isPinSet;
};

export default isPinSetHook;

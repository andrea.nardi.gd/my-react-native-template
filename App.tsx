import React from 'react';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { Provider } from 'react-redux';
import Index from './src/pages/Index';
import store from './src/store/store';
import * as SplashScreen from 'expo-splash-screen';
import AuthorizedCallBackProvider from './src/contexts/authorized-callback/AuthorizedCallbackContext';

// Instruct SplashScreen not to hide yet, we want to do this manually
SplashScreen.preventAutoHideAsync().catch(() => {
  /* reloading the app might trigger some race conditions, ignore them */
});

export default function App(): JSX.Element {
  return (
    <SafeAreaProvider
      initialMetrics={{
        frame: { x: 0, y: 0, width: 0, height: 0 },
        insets: { top: 0, left: 0, right: 0, bottom: 0 },
      }}
    >
      <Provider store={store}>
        <AuthorizedCallBackProvider>
          <Index />
        </AuthorizedCallBackProvider>
      </Provider>
    </SafeAreaProvider>
  );
}

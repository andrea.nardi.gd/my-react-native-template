# Change Log
All notable changes to this project will be documented in this file.
 
The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).
 
## [Unreleased]
 
Here we write upgrading notes for brands. It's a team effort to make them as
straightforward as possible.
 
### Added
 
### Changed
 
### Fixed

### Security
 
## [0.1.0] - 2020-01-01

Project is born.  
 
### Added

- Some boilerplate's files.
 
### Changed
  
### Fixed
 
### Security

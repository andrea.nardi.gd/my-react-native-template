# My React Native Template App

This is a modular template that I use for most of my React Native projects.

### Installation Instructions

Provided that you React Native environment is already set up correctly, you should be able to run the project by simply running:

```
yarn;
```

to install the dependencies. 

### How to run the project

You have 4 options to run the dev client based on your preference:

```
yarn start; (if you use expo and want to run the app on expo go or using the dev client)

yarn start-rn; (if you don't use expo and simply want to serve the app using only react-native cli)

yarn android; (if you want to serve the dev client to an android phone using expo cli)

yarn android-rn;  (if you want to serve the dev client to an android phone using react-native cli)

```

You should be able to add the equivalent commands for iOS too, but since I don't have a mac to try them they are not included and thus not guaranteed to work without any extra configurations. App has been tested on iOS devices using both expo go or EAS cloud build though.

### How to build the project

If you want to use EAS cli build, there are different 8 commands for building the app.

```
build:dev:ios:local => (to build the ios client using DEV environment on the local machine);

build:dev:android:local => (to build the android client using DEV environment on the local machine);

build:dev:ios: => (to build the ios client using DEV environment on EAS cloud);

build:dev:android => (to build the android client using DEV environment on EAS cloud);

build:prod:ios => (to build the ios client using PROD environment on EAS cloud);

build:prod:ios:local => (to build the ios client using PROD environment on the local machine);

build:prod:android => (to build the android client using PROD environment on EAS cloud);

build:prod:android:local => (to build the android client using PROD environment on the local machine);

```

You can also build the project on xcode or android studio. If you need to generate the project files for ios or android just run: 

```
expo prebuild;
```

In that case, in order to use the correct environment you will need to manually specify the current release channel on *AndroidManifest.xml* for android and on *info.plist* for ios.

### Documentation

If you want to enable logs in the app, you should create an .env file in the root directory and add this line

```
REACT_APP_ENV_TYPE=dev
```

Coming Soon
